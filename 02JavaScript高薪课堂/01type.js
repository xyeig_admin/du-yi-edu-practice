// console.log(typeof a);
// // undefined

// console.log(typeof undefined);
// // string => undefined

// console.log(typeof NaN);
// // number

// console.log(typeof null);
// // object

// let a = "123abc";
// console.log(typeof +a);
// // number
// console.log(typeof !!a);
// // boolean
// console.log(typeof a + "");
// // string

// console.log(1 == "1");
// // true

// console.log(NaN == NaN);
// // false

// console.log(NaN == undefined);
// // false

// console.log("11" + 11);
// // 1111

// console.log(1 === "1");
// // false

// console.log(parseInt("123abc"));
// // 123

// let num = 123123.345789;
// console.log(num.toFixed(3));
// // 123123.346

console.log(typeof typeof a);
// string
