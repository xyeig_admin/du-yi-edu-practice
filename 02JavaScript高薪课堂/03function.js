// 告知所选定的小动物的叫声
// function animalVoice(animal, voice) {
//   console.log("You choose " + animal + ", its voice is like " + voice + "~");
// }
// animalVoice("cat", "Mew");
// animalVoice("dog", "WooWong");
function scream(animal) {
  switch (animal) {
    case "cat":
      return "Mew~";
    case "dog":
      return "WooWong~";
    case "fish":
      return "o~o~o~";
    default:
      return "I don't know";
  }
}
console.log(scream("cat"));
console.log(scream("dog"));
console.log(scream("fish"));
console.log(scream("bird"));

// 加法计数器
function sum() {
  let sum = 0;
  for (var i = 0; i < arguments.length; i++) {
    sum += arguments[i];
  }
  return sum;
}
console.log(sum(1, 2, 3, 4, 5, 6, 7));

// 输入数字，逆转并输出汉字形式
function reverseTransform(num) {
  let res = "";
  let str = num + "";
  for (let i = 0; i < str.length; i++) {
    res += transformChinese(num % 10);
    num = parseInt(num / 10);
  }
  return res;
}
function transformChinese(n) {
  let CN = ["零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"];
  return CN[n];
}
console.log(reverseTransform(12));
console.log(reverseTransform(345));
console.log(reverseTransform(6789));

// 实现n的阶乘
function multiply(n) {
  if (n == 0 || n == 1) return 1;
  return n * multiply(n - 1);
}
console.log(multiply(0));
console.log(multiply(3));
console.log(multiply(6));

// 实现斐波那契数列
function feibonacci(n) {
  if (n == 0 || n == 1) return 1;
  return feibonacci(n - 2) + feibonacci(n - 1);
}
console.log(feibonacci(4));
console.log(feibonacci(6));

// 输入一个低于10位的数字，输出它的中文大写读音【千位有零要读】
// eg：10000 => 壹万，1001010 => 壹百万壹千零壹拾，1010001001 => 壹拾亿壹千万壹千零壹
function numberToChinese(number) {
  let CN = ["零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"];
  let UNIT = ["", "拾", "百", "千", "万", "拾", "百", "千", "亿", "拾"];
  let pos = 0,
    res = "";
  let lastNonZero = false; // 标记是否遇到过非零数字
  while (number > 0) {
    let cur = number % 10;
    if (cur > 0) {
      // 位数高的中文拼接到位数低的中文前面，再赋值给结果字符串
      res = CN[cur] + UNIT[pos] + res;
      lastNonZero = true;
    } else if (pos === 4 || pos === 8) {
      // 万和亿位为零时只拼接单位
      res = UNIT[pos] + res;
      lastNonZero = false;
    } else if (lastNonZero) {
      // 低位已在上一个循环拼接了零，不应该重复拼接多个零
      res = CN[cur] + res;
      lastNonZero = false;
    }
    number = Math.floor(number / 10);
    pos++;
  }
  if (res[0] === "零") {
    // 如果最高位是零，去掉前导零
    res = res.slice(1);
  }
  return res;
}
console.log(numberToChinese(10000));
console.log(numberToChinese(1001010));
console.log(numberToChinese(1010001001));
