/**
 * 写一个方法，求一个字符串的字节长度
 * 提示：字符串有一个方法 charCodeAt() ；一个中文占两个字节，一个英文占一个字节
 * 定义和用法：charCodeAt() 方法可返回指定位置的字符的Unicode编码，这个返回值是0-65535之间的整数（返回值<=255时为英文，>255时为中文）
 * 语法：stringObject.charCodeAt(index)
 * eg：var str = "Hello World!";  console.log(str.charCodeAt(1)); // 输出101
 */
function getStringCodeLength(str) {
  let res = (n = str.length);
  for (let i = 0; i < n; i++) {
    if (str.charCodeAt(i) > 255) res++;
  }
  return res;
}
console.log(getStringCodeLength("Web前端开发工程师"));
