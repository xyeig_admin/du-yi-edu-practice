/**
 * 封装type函数
 * [] => array
 * {} => object
 * function() {} => object
 * new Number() => number Object
 * 123 => number
 */
// function myType(param) {
//   if (param === null) return null;
//   switch (typeof param) {
//     case "number":
//       return "number";
//     case "string":
//       return "string";
//     case "boolean":
//       return "boolean";
//     case "function":
//       return "function";
//   }
//   if (Object.prototype.toString.call(param) === "[object Number]") {
//     return "[number Object]";
//   } else if (param instanceof Array) {
//     return "array";
//   } else {
//     return "object";
//   }
// }

// 姬成
/**
 * 1.分两类（原始值、引用值）
 * 2.区分引用值
 */
function myType(target) {
  var ret = typeof target;
  var targetArr = {
    "[object Array]": "array",
    "[object Object]": "object",
    "[object Number]": "number-object",
    "[object Boolean]": "boolean-object",
    "[object String]": "string-object",
  };
  if (target == null) return null;
  // if (typeof target == "function") return "function"; // 可以归在else判断原始值里面
  else if (ret == "object") {
    var str = Object.prototype.toString.call(target);
    return targetArr[str];
  } else {
    return ret;
  }
}

console.log(myType(null));
console.log(myType([]));
console.log(myType({}));
console.log(myType(123));
console.log(myType("123"));
console.log(myType(true));
console.log(myType(new Number("123")));
console.log(myType(function test() {}));
