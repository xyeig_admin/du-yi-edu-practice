/**
 * 实现深克隆
 * 引用值只考虑数组和对象
 */
var obj = {
  name: "John",
  age: 20,
  sex: "male",
  master: ["a", "b"],
  friends: {
    a: "sss",
    b: "ddd",
    c: ["e", "f"],
  },
};

/**
 * 方法一
 */
// function deepClone(origin, target) {
//   if (typeof origin !== "object" || origin === null) return origin;
//   var target = target || (origin instanceof Array ? [] : {});
//   for (var prop in origin) {
//     if (Object.prototype.hasOwnProperty.call(origin, prop))
//       target[prop] = deepClone(origin[prop]);
//   }
//   return target;
// }
// var obj1 = deepClone(obj, obj1);
// console.log(obj, obj1);

// obj.name = "Lucy";
// console.log(obj.name, obj1.name); // Lucy John

// obj.master.push("c");
// console.log(obj.master, obj1.master); // [ 'a', 'b', 'c' ] [ 'a', 'b' ]

/**
 * 方法二：姬成
 * 遍历对象 for (var prop in origin)
 * 1.判断是不是原始值 typeof() "object"
 * 2.判断是数组还是对象 instanceof toString constructor
 * 3.建立相应的数组或对象
 * 递归
 */
function deepClone(origin, target) {
  var target = target || {},
    toStr = Object.prototype.toString,
    arrStr = "[object Array]";
  for (var prop in origin) {
    // 只判断当前对象的属性值，不判断其原型链上的属性
    if (origin.hasOwnProperty(prop)) {
      if (origin[prop] !== "null" && typeof origin[prop] === "object") {
        // 是引用值
        // if (toStr.call(origin[prop]) === arrStr) {
        //   // 是数组
        //   target[prop] = [];
        // } else {
        //   // 是对象
        //   target[prop] = {};
        // }
        target[prop] = toStr.call(origin[prop]) === arrStr ? [] : {};
        // 递归
        deepClone(origin[prop], target[prop]);
      } else {
        // 是原始值
        target[prop] = origin[prop];
      }
    }
  }
  return target;
}
var obj1 = {};
deepClone(obj, obj1);
console.log(obj, obj1);

obj.name = "Lucy";
console.log(obj.name, obj1.name); // Lucy John

obj.master.push("c");
console.log(obj.master, obj1.master); // [ 'a', 'b', 'c' ] [ 'a', 'b' ]
