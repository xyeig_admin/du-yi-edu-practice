/**
 * 给定一串纯字母的字符串，输出第一个只出现一次的字母
 */
function findFirstChar(string) {
  var res = "",
    arr = string.split(""),
    obj = {};
  for (var i = 0; i < arr.length; i++) {
    if (obj[arr[i]]) {
      obj[arr[i]]++;
    } else {
      obj[arr[i]] = 1;
    }
  }
  for (var item in obj) {
    if (obj[item] === 1) {
      res = item;
      break;
    }
  }
  return res;
}
console.log(
  findFirstChar("sniwbchdfwahuiwjkabkasdbsbquerefjdvnmcvzxmcnzveuifiuqfdsb")
);
console.log(
  findFirstChar("qwereiutyuetewuyewuitdwyuwiqqweworvccbmxzbvcnxvbmxznzmzvzxc")
);
