/**
 * 在原型链上实现数组去重方法
 * [1,1,2,2,'a','b','a'] => [1,2,'a','b']
 */
// Array.prototype.uniqueArray = function () {
//   var mp = {},
//     res = [];
//   for (var i = 0; i < this.length; i++) {
//     if (!mp[this[i]]) {
//       res.push(this[i]);
//       mp[this[i]] = true;
//     }
//   }
//   return res;
// };

// 姬成
/**
 * 将数组的属性值当作对象的属性名（键）
 * var obj = {
 *  1: "abc",
 *  2: "abc"
 * }
 * obj[1] => undefined
 * obj[1] => "abc"
 * obj[2] => undefined
 */
Array.prototype.uniqueArray = function () {
  var temp = {},
    arr = [],
    len = this.length;
  for (var i = 0; i < len; i++) {
    if (!temp[this[i]]) {
      temp[this[i]] = "abc";
      arr.push(this[i]);
    }
  }
  return arr;
};

var arr = [1, 1, 2, 2, "a", "b", "a"];
console.log(arr.uniqueArray());
