/**
 * 封装函数，打印当前是何年何月何日何时几分几秒
 */
function printNowTime() {
  let now = new Date();
  return `${now.getFullYear()}年${
    now.getMonth() + 1
  }月${now.getDay()}日 ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
}
console.log(printNowTime());
