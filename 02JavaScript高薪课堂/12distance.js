/**
 * 封装兼容性方法，求滚动轮滚动距离getScrollOffset()
 */
function getScrollOffset() {
  // if (window.pageXOffset) {
  if (window.scrollX) {
    return {
      // x: window.pageXOffset,
      // y: window.pageYOffset,
      x: window.scrollX,
      y: window.scrollY,
    };
  } else {
    return {
      x: document.body.scrollLeft + document.documentElement.scrollLeft,
      y: document.body.scrollTop + document.documentElement.scrollTop,
    };
  }
}

/**
 * 封装兼容性方法，返回浏览器的视口尺寸getViewportOffset()
 */
function getViewportOffset() {
  if (window.innerWidth) {
    return {
      width: window.innerWidth,
      height: window.innerHeight,
    };
  } else {
    return {
      width:
        document.compatMode === "BackCompat"
          ? document.body.clientWidth
          : document.documentElement.clientWidth,
      height:
        document.compatMode === "BackCompat"
          ? document.body.clientHeight
          : document.documentElement.clientHeight,
    };
  }
}

/**
 * 求元素相对于文档的坐标getElementPosition
 */
function getElementPosition(ele) {
  if (ele.offsetParent === document.body) {
    return {
      x: ele.offsetLeft,
      y: ele.offsetTop,
    };
  } else {
    return {
      x: ele.offsetLeft + getElementPosition(ele.parentElement),
      y: ele.offsetTop + getElementPosition(ele.parentElement),
    };
  }
}

/**
 * 封装兼容性方法查询样式getStyle(elem, prop)
 */
function getStyle(elem, prop) {
  if (window.getComputedStyle) {
    return window.getComputedStyle(elem, null)[prop];
  } else {
    return elem.currentStyle[prop];
  }
}

/**
 * 封装兼容性的绑定事件方法addEvent(elem, type, handle)
 */
function addEvent(elem, type, handle) {
  if (elem.addEventListener) {
    elem.addEventListener(type, handle, false);
  } else if (elem.attachEvent) {
    elem.attachEvent("on" + type, function () {
      handle.call(elem);
    });
  } else {
    elem["on" + type] = handle;
  }
}

/**
 * 封装兼容性的移除事件方法removeEvent(elem, type, handle)
 */
function removeEvent(elem, type, handle) {
  if (elem.removeEventListener) {
    elem.removeEventListener(type, handle, false);
  } else if (elem.detachEvent) {
    elem.detachEvent("on" + type, function () {
      handle.call(elem);
    });
  } else {
    elem["on" + type] = null;
  }
}

/**
 * 封装取消冒泡的函数stopBubble(event)
 */
function stopBubble(event) {
  if (event.stopPropagation) {
    event.stopPropagation();
  } else {
    event.cancelBubble = true;
  }
}

/**
 * 封装阻止默认事件的函数cancelHandler(event)
 */
function cancelHandler(event) {
  if (event.preventDefault) {
    event.preventDefault();
  } else {
    event.returnValue = false;
  }
}

/**
 * 拖拽方块跟随鼠标移动
 */
function dragElement(elem) {
  var disX, disY;
  addEvent(elem, "mousedown", function (e1) {
    var event1 = e1 || window.event;
    disX = event1.pageX - parseInt(elem.style.left);
    disY = event1.pageY - parseInt(elem.style.top);
    function documentMouseMove(e2) {
      var event2 = e2 || window.event;
      elem.style.left = event2.pageX - disX + "px";
      elem.style.top = event2.pageY - disY + "px";
    }
    addEvent(document, "mousemove", documentMouseMove);
    addEvent(document, "mouseup", function () {
      removeEvent(document, "mousemove", documentMouseMove);
    });
  });
}
// 姬成
function drag(elem) {
  var disX, disY;
  addEvent(elem, "mousedown", function (e) {
    var event = e || window.event;
    disX = event.clientX - parseInt(getStyle(elem, "left"));
    disY = event.clientY - parseInt(getStyle(elem, "top"));
    addEvent(document, "mousemove", mouseMove);
    addEvent(document, "mouseup", mouseUp);
    stopBubble(event);
    cancelHandler(event);
  });
  function mouseMove(e) {
    var event = e || window.event;
    elem.style.left = event.clientX - disX + "px";
    elem.style.top = event.clientY - disY + "px";
  }
  function mouseUp(e) {
    var event = e || window.event;
    removeEvent(document, "mousemove", mouseMove);
    removeEvent(document, "mouseup", mouseUp);
  }
}

/**
 * 封装函数异步加载JS脚本
 */
function loadScriptAsync(url, callback) {
  var script = document.createElement("script");
  script.type = "text/javascript";
  if (script.readyState) {
    script.onreadystatechange = function () {
      if (script.readyState === "complete" || script.readyState === "loaded") {
        // callback();
        // eval(callback);
        tools[callback]();
      }
    };
  } else {
    script.onload = function () {
      // callback();
      // eval(callback);
      tools[callback]();
    };
  }
  script.src = url; // 绑定事件后再下载资源，防止资源下载速度过快而事件还没绑定上
  document.head.appendChild(script);
}
