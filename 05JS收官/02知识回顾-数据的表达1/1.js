// 下面的变量命名哪些是合法的

/* 


$ 
_
1$ 
list-style
list_style
list style
$$ 
$emit
var


*/

// 作答
/*
_
list-style
list_style
$emit
*/

// 答案
/*
$
_
list_style
$emit
$$
*/
