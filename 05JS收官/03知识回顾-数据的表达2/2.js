/* 
于谦，男，职业捧哏，手机号13812341234，江湖人称谦哥，家住北京八大胡同。
其父亲王老爷子是一位古汉语专家，爱好洗澡

用字面量描述上面的数据
*/
var obj = {
  name: "于谦",
  sex: "男",
  profession: "捧哏",
  phone: "13812341234",
  information: "江湖人称谦哥，家住北京八大胡同",
  father: {
    name: "王老爷子",
    profession: "古汉语专家",
    hobby: "洗澡",
  },
};
console.log(obj);

var yuqian = {
  name: "于谦",
  age: 72,
  isMale: true,
  job: "捧哏",
  mobile: "13812341234", // 读法是一个一个读就存字符串，数字读法则存整形
  nickname: "谦哥",
  address: {
    city: "北京",
    road: "八大胡同",
  },
  parent: {
    name: "王老爷子",
    job: "古汉语专家",
    love: "洗澡",
  },
};
console.log(yuqian);
console.log(yuqian.address.road);
