/* 
邓哥，男，身高185
有邓嫂二位：
其一名为翠花，东北人
其二名为丧彪，铜锣湾人

用字面量描述上面的信息
*/
var obj = {
  name: "邓哥",
  sex: "男",
  height: 185,
  wife: [
    {
      name: "翠花",
      location: "东北",
    },
    {
      name: "丧彪",
      location: "铜锣湾",
    },
  ],
};
console.log(obj);
console.log(obj.wife.length);
