/* 
编写一个完美的求和函数：
1. 若两个数据都是普通数字，求和即可
2. NaN的数据需要变为0
3. 其他类型的数据需要转换为数字
*/
// function sum(a, b) {
//   // code here
//   let typeA = typeof a,
//     typeB = typeof b;
//   if (typeA === "number") {
//     a = a + "" === "NaN" ? 0 : a;
//   } else {
//     a = +a + "" === "NaN" ? 0 : +a;
//   }
//   if (typeB === "number") {
//     b = b + "" === "NaN" ? 0 : b;
//   } else {
//     b = +b + "" === "NaN" ? 0 : +b;
//   }
//   return a + b;
// }
function sum(a, b) {
  a = +a || 0;
  b = +b || 0;
  return a + b;
}
console.log(sum(-1, 5));
console.log(sum(NaN, 2));
console.log(sum(NaN, NaN));
console.log(sum("NaN", NaN));
console.log(sum(NaN, "NaN"));
console.log(sum("NaN", "NaN"));
console.log(sum(1, {}));
console.log(sum({}, {}));
console.log(sum([], 2));
console.log(sum([], []));
console.log(sum([], {}));
console.log(sum([], false));
console.log(sum(true, {}));
