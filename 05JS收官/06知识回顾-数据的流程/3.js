/* 
1. 1~100求和
*/
var sum = 0;
for (var i = 1; i <= 100; i++) {
  sum += i;
}
console.log(sum);

/* 
2. 求某个数的阶乘
*/
// function calculateFactorial(n) {
//   if (n === 0 || n === 1) return 1;
//   return n * calculateFactorial(n - 1);
// }
// console.log(calculateFactorial(0));
// console.log(calculateFactorial(1));
// console.log(calculateFactorial(3));
// console.log(calculateFactorial(5));
var res = 1;
var n = 5;
for (var i = 2; i <= n; i++) {
  res *= i;
}
console.log(res);

/* 
3. 数组求和
*/
var arr = [1, 2, 3, 4, 5];
var sum2 = 0;
for (var i = 0; i < arr.length; i++) {
  sum2 += arr[i];
}
console.log(sum2);

/* 
4. 求数组中的奇数的个数
*/
var count = 0;
for (var i = 0; i < arr.length; i++) {
  if (arr[i] % 2 !== 0) count++;
}
console.log(count);

/* 
5. 求数组中的奇数和
*/
var sum3 = 0;
for (var i = 0; i < arr.length; i++) {
  if (arr[i] % 2 !== 0) sum3 += arr[i];
}
console.log(sum3);
