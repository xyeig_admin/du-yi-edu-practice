/* 
1. 数组中是否存在某个数，输出 是 或 否
*/
var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
function getNumberFromArray(num) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] === num) return "是";
  }
  return "否";
}
console.log(getNumberFromArray(100));
console.log(getNumberFromArray(5));

/* 
2. 数组中是否存在某个数，如果存在，则输出它所在的下标，如果不存在，则输出-1
*/
function getNumberIndexFromArray(num) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] === num) return i;
  }
  return -1;
}
console.log(getNumberIndexFromArray(100));
console.log(getNumberIndexFromArray(5));

/* 
3. 找到数组中第一个奇数和最后一个奇数，将它们求和
*/
var sum = 0;
var first = 0;
var last = 0;
for (var i = 0; i < arr.length / 2; i++) {
  if (arr[i] % 2 !== 0) {
    first = i;
    break;
  }
}
for (var j = arr.length - 1; j > arr.length / 2; j--) {
  if (arr[j] % 2 !== 0 && j !== first) {
    last = j;
    break;
  }
}
sum = arr[first] + arr[last];
console.log(arr[first], arr[last], sum);

/* 
4. 有两个数组，看两个数组中是否都存在奇数，输出 是 或 否
*/
var arr2 = [2, 4, 6, 8, 10];
var arr3 = [1, 3, 5, 7, 9];
function isBothOdd(arr1, arr2) {
  var flag = 0;
  for (var i = 0; i < arr1.length; i++) {
    if (arr1[i] % 2 !== 0) {
      flag++;
      break;
    }
  }
  for (var i = 0; i < arr2.length; i++) {
    if (arr2[i] % 2 !== 0) {
      flag++;
      break;
    }
  }
  return flag === 2 ? "是" : "否";
}
console.log(isBothOdd(arr, arr2));
console.log(isBothOdd(arr, arr3));
console.log(isBothOdd(arr2, arr3));
