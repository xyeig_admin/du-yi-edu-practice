/**
 * 交换两个变量的值
 * @param {*} a 变量1
 * @param {*} b 变量2
 */
function swap(a, b) {
  var temp = a;
  a = b;
  b = temp;
}
var a = 1,
  b = 2;
swap(a, b);
console.log(a, b);
// 无解
