/**
 * 修改对象，仅保留需要的属性
 * @param {Object} obj 要修改的对象
 * @param {Array<string>} keys 需要保留的属性名数组
 */
function pick(obj, keys) {
  for (var key in obj) {
    if (!keys.includes(key)) {
      delete obj[key];
    }
  }
}
var obj = {
  name: "Lucy",
  age: 20,
  sex: "female",
  hobby: ["1", "2"],
};
var keys = ["name", "age"];
pick(obj, keys);
console.log(obj);
