// 下面代码输出什么？
var foo = {
  n: 0,
  k: {
    n: 0,
  },
};
var bar = foo.k; // bar { n: 0 }
bar.n++; // bar { n: 1 }, foo { n: 0, k: { n: 1 } }
bar = {
  n: 10,
}; // bar { n: 10 }
bar = foo; // bar { n: 0, k: { n: 0 } }
bar.n++; // foo/bar { n: 1, k: { n: 0 } }
bar = foo.n; // bar: 1
bar++; // bar: 2
console.log(foo.n, foo.k.n);
// 1 1
