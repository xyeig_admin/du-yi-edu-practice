// 下面的代码输出什么（京东）？
var foo = {
  n: 1,
};

var arr = [foo]; // arr [{ n: 1 }]

function method1(arr) {
  var bar = arr[0]; // bar { n: 1 }
  arr.push(bar); // arr [{ n: 1 }, { n: 1 }]
  bar.n++; // bar { n: 2 }, arr [{ n: 2 }, { n: 2 }]
  arr = [bar]; // arr [{ n: 2 }]
  arr.push(bar); // arr [{ n: 2 }, { n: 2 }]
  arr[1].n++; // arr [{ n: 3 }, { n: 3 }]
}
function method2(foo) {
  foo.n++;
}
function method3(n) {
  n++;
}
method1(arr);
method2(foo); // foo { n: 4 }, arr [{ n: 4 }, { n: 4 }]
method3(foo.n);

console.log(foo.n, arr.length);
// 4 2
