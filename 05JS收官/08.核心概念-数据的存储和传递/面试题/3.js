// 下面的代码输出什么（字节）？
var foo = { bar: 1 };
var arr1 = [1, 2, foo]; // arr1 [1, 2, { bar: 1 }]
var arr2 = arr1.slice(1); // arr2 [2, { bar: 1 }]
arr2[0]++; // arr2 [3, { bar: 1 }]
arr2[1].bar++; // arr2 [3, { bar: 2 }], foo { bar: 2 }, arr1 [1, 2, { bar: 2 }]
foo.bar++; // arr2 [3, { bar: 3 }], foo { bar: 3 }, arr1 [1, 2, { bar: 3 }]
arr1[2].bar++; // arr2 [3, { bar: 4 }], foo { bar: 4 }, arr1 [1, 2, { bar: 4 }]
console.log(arr1[1] === arr2[0]); // 2 === 3
console.log(arr1[2] === arr2[1]); // { bar: 4 } === { bar: 4 } => foo
console.log(foo.bar);
// false true 4
