/* 
利用构造函数创建一副扑克牌
*/
// function PokeCard(number, color) {
//   this.number = number;
//   this.color = color;
//   this.cardName = color + number;
//   this.showCard = function () {
//     console.log("打出一张：" + this.cardName);
//   };
// }
// var card1 = new PokeCard("K", "红桃");
// card1.showCard();
// var card2 = new PokeCard("3", "方块");
// card2.showCard();

// 袁进
/**
 * Deck：一副扑克牌
 * Poker：一张扑克牌
 */

/**
 *
 * @param {number} number 1-1, ..., 11-J, 12-Q, 13-K, 14-小王, 15-大王
 * @param {number} color 1-黑桃, 2-红桃, 3-梅花, 4-方块
 */
function Poker(number, color) {
  this.number = number;
  this.color = color;
  this.print = function () {
    if (this.number === 14) {
      console.log("joker");
      return;
    }
    if (this.number === 15) {
      console.log("JOKER");
      return;
    }
    // 其他情况
    // 得到花色
    var colors = ["♠", "♥", "♣", "♦"];
    var color = colors[this.color - 1];
    // 得到点数
    var numbers = [
      "A",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "10",
      "J",
      "Q",
      "K",
    ];
    var number = numbers[this.number - 1];
    console.log(color + number);
  };
}
var p1 = new Poker(1, 1);
p1.print();
var p2 = new Poker(2, 3);
p2.print();

/**
 * 一碟扑克牌
 */
function Deck() {
  this.pokers = [];
  for (var i = 1; i <= 13; i++) {
    for (var j = 1; j <= 4; j++) {
      this.pokers.push(new Poker(i, j));
    }
  }
  this.pokers.push(new Poker(14, 0));
  this.pokers.push(new Poker(15, 0));
  this.print = function () {
    for (var i = 0; i < this.pokers.length; i++) {
      this.pokers[i].print();
    }
  };
}
var deck = new Deck();
deck.print();
