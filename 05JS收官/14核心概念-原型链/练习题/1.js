// toString方法属于Object.prototype，它会把对象转换为字符串的形式 [object Object]
// 这种格式并非每个对象想要的
// 1. 解释数组的toString为什么能得到不同的格式
// 2. 如果自己的构造函数希望改变toString，如何改变

/**
 * 1.
 * 因为Array.prototype重写了toString方法，使其能够得到不同格式
 * 数组调用toString方法时会沿着原型链找到Array.prototype
 */
console.log(Array.prototype.toString === Object.prototype.toString); // false

/**
 * 2.
 * 可以在构造函数的原型prototype上重写toString方法
 */
function User() {}
var user = new User();
User.prototype.toString = function () {
  console.log(111);
};
user.toString(); // 111
