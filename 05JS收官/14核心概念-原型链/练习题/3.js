// 创建一个没有隐式原型的用户对象，随意添加一些属性
var user = {
  name: "Lucy",
  age: 20,
};
console.log(user.__proto__);

// 去除隐式原型
Object.setPrototypeOf(user, null);
console.log(user.__proto__);
