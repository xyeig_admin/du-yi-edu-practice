// 将下面的字符串分割成一个单词数组，同时去掉数组中每一项的,和.
var str =
  "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci impedit voluptatem cupiditate, est corporis, quis sunt quod tempore officiis hic voluptates eaque commodi. Repudiandae provident animi quia qui harum quasi.";

var strArr = str.split(" ");
for (var i = 0; i < strArr.length; i++) {
  strArr[i] = strArr[i].replace(",", "").replace(".", "");
}
console.log(strArr);

// 最好使用replaceAll，但是run code插件不支持，会报错，要在浏览器console运行
