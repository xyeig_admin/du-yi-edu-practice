// 将下面的rgb格式转换成为HEX格式
var rgb = "rgb(253, 183, 25)";

var nums = rgb.substring(rgb.indexOf("(") + 1, rgb.lastIndexOf(")"));
var numsArr = nums.replaceAll(" ", "").split(",");
var color = "#";
for (var i = 0; i < numsArr.length; i++) {
  color += parseInt(numsArr[i]).toString(16);
}
console.log(color);
// var hex = '#fdb719';

// 袁进
var parts = rgb.replace("rgb", "").replace("(", "").replace(")", "").split(",");
var r = parseInt(parts[0]).toString(16);
var g = parseInt(parts[1]).toString(16);
var b = parseInt(parts[2]).toString(16);
var res = "#" + r + g + b;
console.log(res);
