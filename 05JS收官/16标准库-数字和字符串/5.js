// name转换成驼峰命名
// var name = "has own property"; // --> hasOwnProperty
var name = "what is your name";

var nameArr = name.split(" ");
for (var i = 1; i < nameArr.length; i++) {
  nameArr[i] =
    nameArr[i].substring(0, 1).toUpperCase() + nameArr[i].substring(1);
}
var res = nameArr.join("");
console.log(res);

// 袁进
var res = "";
var parts = name.split(" ");
for (var i = 0; i < parts.length; i++) {
  var s = parts[i];
  if (i > 0) {
    s = s[0].toUpperCase() + s.substring(1);
  }
  res += s;
}
console.log(res);
