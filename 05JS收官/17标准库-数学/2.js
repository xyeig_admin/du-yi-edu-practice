/**
 * 得到一个指定长度的随机字符串
 * 字符串包含：数字、字母
 * @param {number} length 字符串的长度
 * @return {number} 随机字符串
 */
function getRandomString(length) {
  var res = "";
  for (var i = 0; i < length; i++) {
    res +=
      Math.random() - 0.5 > 0
        ? String.fromCharCode(Math.floor(Math.random() * (58 - 48) + 48))
        : String.fromCharCode(Math.floor(Math.random() * (123 - 65) + 65));
  }
  return res;
}
console.log(getRandomString(5));

/**
 * 方法一
 */
// function getRandomString(length) {
//   var str = "0123456789abcdefghijklmnopqrstuvwxyz";
//   var result = "";
//   for (var i = 0; i < length; i++) {
//     var index = getRandom(0, str.length);
//     result += str[index];
//   }
//   return result;
// }

/**
 * 方法二
 */
function getRandomString2(length) {
  return Math.random()
    .toString(36)
    .substring(2, 2 + length);
}
console.log(getRandomString2(5));
