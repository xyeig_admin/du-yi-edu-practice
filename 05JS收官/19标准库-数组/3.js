// 数组去重
var nums = [1, 1, "1", "a", "b", "a", 3, 5, 3, 7];

var res = [nums[0]];
for (var i = 1; i < nums.length; i++) {
  if (!res.includes(nums[i])) {
    res.push(nums[i]);
  }
}
console.log(res);

// 袁进
// for (var i = 0; i < nums.length; i++) {
//   var item = nums[i];
//   for (var j = i + 1; j < nums.length; j++) {
//     if (nums[j] === item) {
//       nums.splice(j, 1);
//       j--;
//     }
//   }
// }
// console.log(nums);
