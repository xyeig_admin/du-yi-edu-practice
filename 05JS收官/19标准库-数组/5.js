// 将下面的伪数组转换为真数组
var fakeArr = {
  0: "a",
  1: "b",
  2: "c",
  length: 3,
};

console.log(Object.assign([], fakeArr));

// 袁进
var arr = Array.prototype.slice.call(fakeArr);
console.log(arr);
