// 定时器固定结构
var timer = null;
function start() {
  if (timer) return;
  timer = setInterval(() => {
    console.clear();
    console.log(new Date().toLocaleString());
  }, 1000);
}
function stop() {
  clearInterval(timer);
  timer = null;
}
