// 根据hero.js提供的数据heros，创建合适的元素，将英雄数据显示到页面上

var container = document.querySelector(".container");
var html = document.createDocumentFragment();
for (var hero of heros) {
  let a = document.createElement("a");
  a.href = `https://pvp.qq.com/web201605/herodetail/${hero.ename}.shtml`;
  a.target = "_blank";
  a.className = "item";

  let img = document.createElement("img");
  img.src = `https://game.gtimg.cn/images/yxzj/img201606/heroimg/${hero.ename}/${hero.ename}.jpg`;

  let span = document.createElement("span");
  span.innerHTML = hero.cname;

  a.appendChild(img);
  a.appendChild(span);
  html.appendChild(a);
}
container.appendChild(html);
