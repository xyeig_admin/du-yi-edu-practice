var img = document.querySelector("img");
var curIdx = 1;
var timer = setInterval(() => {
  // 每隔一段时间，切换英雄的图片，让英雄动起来
  if (++curIdx > 4) curIdx = 1;
  // 等价于
  // curIdx = (curIdx % 4) + 1;
  img.src = `./img/${curIdx}.png`;

  // 每隔一段时间，改变英雄的位置，让英雄向右移动
  img.style.transform = `translateX(${30 * curIdx}px)`;
}, 300);
