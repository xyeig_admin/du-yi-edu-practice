/**
 * 根据协议的多选框是否选中设置注册按钮状态
 */
var policyBox = document.querySelector(".form-item.policy");
var registBtn = document.querySelector("button.left");
function setSubmitButtonStatus() {
  var policyInput = document.querySelector(".form-item.policy input");
  policyInput.addEventListener("change", function (e) {
    e.target.checked
      ? registBtn.removeAttribute("disabled")
      : registBtn.setAttribute("disabled", true);
  });
}
/**
 * 根据手机号文本框中的文本，设置发送验证码按钮的状态
 */
var phoneInput = document.querySelector(".form-item input.txt");
var codeBtn = document.querySelector(".form-item.captcha button.right");
function setSendCodeButtonStatus(e) {
  e.target.value.length === 11
    ? codeBtn.removeAttribute("disabled")
    : codeBtn.setAttribute("disabled", true);
}
/**
 * 根据当前选中的爱好，设置已选择爱好文本
 */
var selectBox = document.querySelector(".form-item select");
var resultBox = selectBox.parentElement.nextElementSibling;
function setSelectedLoves(e) {
  var spanElements = resultBox.querySelectorAll("span");
  for (var i = 0; i < spanElements.length; i++) {
    resultBox.removeChild(spanElements[i]);
  }
  var options = e.target.selectedOptions;
  for (var i = 0; i < options.length; i++) {
    let span = document.createElement("span");
    span.innerText = options[i].innerText;
    resultBox.appendChild(span);
  }
}

// 将上面的函数和用户事件连接
policyBox.addEventListener("click", setSubmitButtonStatus);
phoneInput.addEventListener("input", setSendCodeButtonStatus);
selectBox.addEventListener("change", setSelectedLoves);

// 给所有的文本框注册事件，若用户在输入的过程中按下了ESC，则将文本框清空
var inputs = document.querySelectorAll("input");
for (var i = 0; i < inputs.length; i++) {
  (function (i) {
    inputs[i].addEventListener("keydown", function (e) {
      if (e.key === "Escape") inputs[i].value = "";
    });
  })(i);
}

// /**
//  * 袁进
//  */
// function $(selector) {
//   return document.querySelector(selector);
// }
// /**
//  * 根据协议的多选框是否选中设置注册按钮状态
//  */
// function setSubmitButtonStatus() {
//   $('button[type="submit"]').disabled = !$(".policy input").checked;
// }
// /**
//  * 根据手机号文本框中的文本，设置发送验证码按钮的状态
//  */
// function setSendCodeButtonStatus(e) {
//   $(".captcha button").disabled = $("#txtPhone").value.length !== 11;
// }
// /**
//  * 根据当前选中的爱好，设置已选择爱好文本
//  */
// function setSelectedLoves(e) {
//   var sel = $("select");
//   var choose = $("#selChoose");
//   var loves = [];
//   for (var i = 0; i < sel.children.length; i++) {
//     var option = sel.children[i];
//     if (option.selected) {
//       loves.push(option.innerText);
//     }
//   }
//   var str = loves.join(",");
//   choose.innerText = "已选择的爱好：" + str;
// }

// setSendCodeButtonStatus();
// setSendCodeButtonStatus();
// setSelectedLoves();

// // 将上面的函数和用户事件连接
// $("#txtPhone").addEventListener("input", setSendCodeButtonStatus);
// $(".policy input").addEventListener("change", setSubmitButtonStatus);
// $("select").addEventListener("change", setSelectedLoves);

// // 给所有的文本框注册事件，若用户在输入的过程中按下了ESC，则将文本框清空
// var txts = document.querySelectorAll(".txt");
// for (var i = 0; i < txts.length; i++) {
//   txts[i].addEventListener("keydown", function (e) {
//     if (e.key === "Escape") {
//       this.value = "";
//     }
//   });
// }
