// 每隔1秒自动切换图片
var img = document.querySelector("img");
var curIdx = 1;
function changeImage() {
  if (++curIdx > 4) curIdx = 1;
  img.src = `./img/${curIdx}.jpeg`;
}
var timer = setInterval(changeImage, 1000);

// 当鼠标移动到元素上时停止切换，移出后开始切换
var container = document.querySelector(".container");
container.addEventListener("mouseenter", function (e) {
  clearInterval(timer);
});
container.addEventListener("mouseleave", function (e) {
  timer = setInterval(changeImage, 1000);
});

// /**
//  * 袁进
//  */
// var img = document.querySelector(".container img");
// var curIndex = 1;
// var timerId;
// // 开始切换
// function start() {
//   if (timerId) return;
//   timerId = setInterval(() => {
//     curIndex = (curIndex % 4) + 1;
//     img.src = "./img/" + curIndex + ".jpeg";
//   }, 1000);
// }

// // 停止切换
// function stop() {
//   clearInterval(timerId);
//   timerId = null;
// }

// // 每隔1秒自动切换图片
// start();

// // 当鼠标移动到元素上时停止切换，移出后开始切换
// img.addEventListener("mouseenter", stop);
// img.addEventListener("mouseleave", start);
