// 输入待办事项，按下回车后，添加事项到列表
var inputBox = document.querySelector("input.txt");
var listBox = document.querySelector("ul.todo-list");
inputBox.addEventListener("keydown", function (e) {
  if (e.key === "Enter" && inputBox.value !== "") {
    var li = document.createElement("li");
    var span = document.createElement("span");
    span.innerHTML = inputBox.value;
    li.appendChild(span);
    var button = document.createElement("button");
    button.innerText = "删除";
    button.addEventListener("click", function () {
      listBox.removeChild(li);
    });
    li.appendChild(button);
    listBox.appendChild(li);
    inputBox.value = "";
  }
});

// 点击删除后，删除对应的待办事项
var delBtns = document.querySelectorAll("li button");
for (var i = 0; i < delBtns.length; i++) {
  (function (i) {
    delBtns[i].addEventListener("click", function () {
      var parent = delBtns[i].parentElement;
      listBox.removeChild(parent);
    });
  })(i);
}

// /**
//  * 袁进
//  */
// // 输入待办事项，按下回车后，添加事项到列表
// var txt = document.querySelector(".txt");
// var ul = document.querySelector(".todo-list");

// function createLi(content) {
//   var li = document.createElement("li");
//   var span = document.createElement("span");
//   var button = document.createElement("button");
//   li.appendChild(span);
//   li.appendChild(button);
//   span.innerText = content;
//   button.innerText = "删除";
//   ul.appendChild(li);

//   // 点击删除后，删除对应的待办事项
//   // 监听按钮的点击事件
//   button.addEventListener("click", function () {
//     li.remove();
//   });
// }

// txt.addEventListener("keydown", function (e) {
//   if (e.key === "Enter") {
//     if (!this.value.trim()) return;

//     createLi(this.value.trim());

//     // 文本框清空
//     this.value = "";
//   }
// });
