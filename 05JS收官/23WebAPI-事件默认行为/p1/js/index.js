// 完成表单验证
var formItems = document.querySelectorAll(".form-item");
for (var i = 0; i < formItems.length - 1; i++) {
  (function (i) {
    var input = formItems[i].querySelector("input");
    input.addEventListener("input", function () {
      if (this.value === "") {
        formItems[i].classList.add("err");
      } else {
        formItems[i].classList.remove("err");
      }
    });
  })(i);
}
var submitBtn = document.querySelector(".form-item button");
var inputBox = document.querySelectorAll(".form-item input");
submitBtn.addEventListener("click", function (e) {
  e.preventDefault();
  for (var i = 0; i < inputBox.length; i++) {
    if (inputBox[i].value === "")
      inputBox[i].parentElement.classList.add("err");
    else inputBox[i].parentElement.classList.remove("err");
  }
});

// /**
//  * 袁进
//  */
// // 完成表单验证
// /**
//  * 验证账号
//  */
// function validateLoginId() {
//   var div = document.querySelector("#loginId");
//   var inp = div.querySelector("input");
//   var msg = div.querySelector(".msg");

//   var err = ""; // 错误消息
//   var loginId = inp.value.trim();
//   if (!loginId) {
//     err = "账号不能为空";
//   } else if (loginId.length < 3 || loginId.length > 12) {
//     err = "账号是3-12位";
//   }
//   msg.innerText = err;
//   div.className = err ? "form-item err" : "form-item";
//   return !err;
// }

// /**
//  * 验证密码
//  */
// function validatePassword() {
//   var div = document.querySelector("#loginPwd");
//   var inp = div.querySelector("input");
//   var msg = div.querySelector(".msg");

//   var err = ""; // 错误消息
//   var loginPwd = inp.value.trim();
//   if (!loginPwd) {
//     err = "密码不能为空";
//   } else if (loginPwd.length < 3 || loginPwd.length > 16) {
//     err = "密码是3-16位";
//   }
//   msg.innerText = err;
//   div.className = err ? "form-item err" : "form-item";
//   return !err;
// }

// /**
//  * 验证整个表单，设置元素内容和状态
//  * @return {boolean} 验证通过返回true，不通过返回false
//  */
// function validateForm() {
//   var r1 = validateLoginId();
//   var r2 = validatePassword();
//   return r1 && r2;
// }

// // 注册事件
// var loginId = document.querySelector("#loginId input");
// var loginPwd = document.querySelector("#loginPwd input");
// loginId.addEventListener("input", validateLoginId);
// loginPwd.addEventListener("input", validatePassword);
// var form = document.querySelector(".form-container");
// form.addEventListener("submit", function (e) {
//   var res = validateForm();
//   if (!res) {
//     e.preventDefault();
//   }
// });
