// 让便签可被拖动，但不能超出视口
var moveBar = document.querySelector(".move-bar");
var note = document.querySelector(".note");
var borderX = document.documentElement.clientWidth - note.clientWidth;
var borderY = document.documentElement.clientHeight - note.clientHeight;
var disX,
  disY,
  endX,
  endY,
  isDragging = false;
moveBar.addEventListener("mousedown", function (e) {
  disX = e.clientX - note.offsetLeft;
  disY = e.clientY - note.offsetTop;
  isDragging = true;
});
document.addEventListener("mousemove", function (e) {
  if (isDragging) {
    var endX = e.clientX - disX;
    var endY = e.clientY - disY;
    if (endX < 0) endX = 0;
    if (endY < 0) endY = 0;
    if (endX > borderX) endX = borderX;
    if (endY > borderY) endY = borderY;
    note.style.left = `${endX}px`;
    note.style.top = `${endY}px`;
  }
});
document.addEventListener("mouseup", function () {
  isDragging = false;
});

// /**
//  * 袁进
//  */
// // 让便签可被拖动，但不能超出视口
// var moveBar = document.querySelector(".move-bar");
// var note = document.querySelector(".note");
// moveBar.onmousedown = function (e) {
//   // 鼠标按下的坐标
//   var x = e.clientX;
//   var y = e.clientY;
//   // 元素的坐标
//   var rect = moveBar.getBoundingClientRect();
//   var ex = rect.left;
//   var ey = rect.top;

//   // 获取视口宽高、元素宽高【费时操作，事件触发前获取最佳】
//   var w = document.documentElement.clientWidth;
//   var h = document.documentElement.clientHeight;
//   var ew = note.offsetWidth;
//   var eh = note.offsetHeight;
//   // 计算移动有效区域最大值
//   var maxLeft = w - ew;
//   var maxTop = h - eh;

//   // 鼠标按下后，监听整个屏幕的鼠标移动
//   window.onmousemove = function (e) {
//     // 鼠标移动后的坐标距离按下时的坐标的距离
//     var disX = e.clientX - x;
//     var disY = e.clientY - y;

//     // 元素移动后的坐标
//     var left = ex + disX;
//     var top = ey + disY;
//     // 限制移动的有效范围
//     if (left < 0) left = 0;
//     if (left > maxLeft) left = maxLeft;
//     if (top < 0) top = 0;
//     if (top > maxTop) top = maxTop;
//     // 更新元素位置
//     note.style.left = left + "px";
//     note.style.top = top + "px";
//   };

//   // 鼠标抬起后，不再监听鼠标移动和抬起
//   window.onmouseup = function () {
//     window.onmousemove = null;
//     window.onmouseup = null;
//   };
// };
