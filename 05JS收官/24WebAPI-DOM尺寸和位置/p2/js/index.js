// 让小球向右下运动，遇到边缘后反弹
var borderX = document.documentElement.clientWidth;
var borderY = document.documentElement.clientHeight;
var ball = document.querySelector(".ball");
ball.style.transition = "all 0.5s linear";
var ballLeft, ballTop, timer1, timer2;
function moveForward() {
  ballLeft = ball.getBoundingClientRect().left;
  ballTop = ball.getBoundingClientRect().top;
  var forwardLeft = ballLeft + 100;
  var forwardTop = ballTop + 50;
  if (forwardLeft >= borderX || forwardTop >= borderY) {
    clearInterval(timer1);
    timer2 = setInterval(moveBackward, 300);
  } else {
    ball.style.left = forwardLeft + "px";
    ball.style.top = forwardTop + "px";
  }
}
function moveBackward() {
  ballLeft = ball.getBoundingClientRect().left;
  ballTop = ball.getBoundingClientRect().top;
  var backwardLeft = ballLeft - 100;
  var backwardTop = ballTop - 50;
  if (backwardLeft <= 0 || backwardTop <= 0) {
    clearInterval(timer2);
    timer1 = setInterval(moveForward, 300);
  } else {
    ball.style.left = backwardLeft + "px";
    ball.style.top = backwardTop + "px";
  }
}
timer1 = setInterval(moveForward, 300);

// /**
//  * 袁进
//  */
// // 让小球向右下运动，遇到边缘后反弹
// var ball = document.querySelector(".ball");
// var disX = 10,
//   disY = 10;
// // 屏幕的宽高
// var w = document.documentElement.clientWidth,
//   h = document.documentElement.clientHeight;
// // 小球的宽高
// var ew = ball.clientWidth,
//   eh = ball.clientHeight;
// // 移动坐标最大值
// var maxLeft = w - ew,
//   maxTop = h - eh;
// // 随机颜色
// function getRandom(min, max) {
//   return Math.floor(Math.random() * (max - min)) + min;
// }
// function changeBg() {
//   ball.style.backgroundColor =
//     "rgb(" +
//     getRandom(0, 200) +
//     "," +
//     getRandom(0, 200) +
//     "," +
//     getRandom(0, 200) +
//     ")";
// }
// // 每隔一段时间（20ms）改变小球的left和top
// setInterval(() => {
//   var rect = ball.getBoundingClientRect();
//   var x = rect.left,
//     y = rect.top;
//   var left = x + disX,
//     top = y + disY;

//   // 控制范围
//   if (left > maxLeft) {
//     left = maxLeft;
//     disX = -disX;
//     changeBg();
//   }
//   if (left < 0) {
//     left = 0;
//     disX = -disX;
//     changeBg();
//   }
//   if (top > maxTop) {
//     top = maxTop;
//     disY = -disY;
//     changeBg();
//   }
//   if (top < 0) {
//     top = 0;
//     disY = -disY;
//     changeBg();
//   }

//   ball.style.left = left + "px";
//   ball.style.top = top + "px";
// }, 20);
