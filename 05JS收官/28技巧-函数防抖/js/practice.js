// 当窗口尺寸变化后，调用layout函数
function debounce(fn, delay) {
  var timer = null;
  return function () {
    clearTimeout(timer);
    var _this = this;
    var args = Array.prototype.slice.call(arguments, 0);
    timer = setTimeout(() => {
      fn.apply(_this, args);
    }, delay);
  };
}
var handleResize = debounce(layout, 500);
window.addEventListener("resize", handleResize);
