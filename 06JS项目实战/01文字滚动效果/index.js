// 获取DOM元素
var list = document.querySelector(".list");
var items = list.querySelectorAll(".item");
// 获取ul列表限定的高度（每一个子项的高度）
var listHeight = list.getBoundingClientRect().height;
// 获取ul列表的整体内容高
var totalHeight = listHeight * items.length - 1;
// 滚动间隔时间
var duration = 1500;
// 滚动列表：每次滚动一个子项的高度
function handleListScroll() {
  // 列表当前滚动高度
  var curTop = list.scrollTop;
  // 下一项的高度
  var nextTop = curTop + listHeight;
  // 滚动时变化动画的总时间
  var totalChangeTime = 500;
  // 滚动时变化动画的间隔
  var changeDuration = 15;
  // 滚动时变化动画执行次数
  var times = Math.floor(totalChangeTime / changeDuration);
  console.log(nextTop, curTop);
  // 滚动时变化动画每次滚动的间隔
  var disY = listHeight / times;
  var timer = setInterval(() => {
    curTop += disY;
    if (curTop >= nextTop) {
      clearInterval(timer);
      if (nextTop >= totalHeight) curTop = nextTop = 0;
    }
    list.scrollTo(0, curTop);
  }, changeDuration);
}
// 每隔一段时间滚动一次
setInterval(handleListScroll, duration);
