/**
 * 创建一个动画
 * @param {object} options 配置参数
 * @augments {number} options.from 起始数字
 * @augments {number} options.to 结束数字
 * @augments {number} options.totalMS 可选，动画持续时间，默认1000ms
 * @augments {number} options.duration 可选，动画每次变化的间隔，默认15ms
 * @augments {Function} options.onmove 可选，动画变化过程中的回调
 * @augments {Function} options.onend 可选，动画变化结束后的回调
 */
function createAnimation(options) {
  var from = options.from;
  var to = options.to;
  var totalMS = options.totalMS || 1000;
  var duration = options.duration || 15;

  // 计算动画变化的次数
  var times = Math.floor(totalMS / duration);
  // 计算数字每次动画变化多少
  var dis = (to - from) / times;

  // 当前变化次数
  var curTime = 0;
  var timer = setInterval(() => {
    from += dis;
    curTime++;
    if (curTime >= times) {
      from = to;
      clearInterval(timer);
      options.onend && options.onend();
    }
    options.onmove && options.onmove(from);
  }, duration);
}
