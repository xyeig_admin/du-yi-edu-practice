var menuTitle = document.querySelectorAll(".menu-title");

// 动画时长
var totalMS = 200;
var duration = 10;

// 计算二级菜单总高度
var menuSub = menuTitle[0].nextElementSibling;
var menuItemSub = menuSub.querySelectorAll(".menu-item-sub");
var subMenuHeight = menuItemSub[0].clientHeight * menuItemSub.length;

// 点击一级菜单标题
for (var i = 0; i < menuTitle.length; i++) {
  menuTitle[i].addEventListener("click", function () {
    var curMenuSubShown = document.querySelector(
      ".menu-sub[data-status='show']"
    );
    if (curMenuSubShown) {
      hideMenu(curMenuSubShown);
    }
    changeMenu(this.nextElementSibling);
  });
}

// 展开菜单
function showMenu(menuSub) {
  // 展开菜单动画
  createAnimation({
    from: 0,
    to: subMenuHeight,
    totalMS,
    duration,
    onmove: function (n) {
      menuSub.style.height = n + "px";
    },
    onend: function () {
      menuSub.setAttribute("data-status", "show");
    },
  });
}

// 收起菜单
function hideMenu(menuSub) {
  // 收起菜单动画
  createAnimation({
    from: subMenuHeight,
    to: 0,
    totalMS,
    duration,
    onmove: function (n) {
      menuSub.style.height = n + "px";
    },
    onend: function () {
      menuSub.setAttribute("data-status", "hide");
    },
  });
}

// 切换菜单状态
function changeMenu(menuSub) {
  var curMenuSubStatus = menuSub.getAttribute("data-status");
  if (curMenuSubStatus && curMenuSubStatus === "show") {
    hideMenu(menuSub);
  } else {
    showMenu(menuSub);
  }
}
