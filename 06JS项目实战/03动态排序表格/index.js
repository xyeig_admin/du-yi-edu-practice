var checkAllBox = document.querySelector("#check_all");
var thead = document.querySelector("thead");
var tbody = document.querySelector("tbody");
var checkboxes = tbody.querySelectorAll("input[type='checkbox']");
var rows = tbody.querySelectorAll("tr");

// 全选/全不选
function changeCheckAll() {
  for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].checked = checkAllBox.checked;
  }
}
checkAllBox.addEventListener("change", changeCheckAll);

// 每一项勾选/不勾选
function changeCheck() {
  var checkCount = 0;
  for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].checked && checkCount++;
  }
  checkAllBox.checked = checkCount === checkboxes.length;
}
tbody.addEventListener("click", function (e) {
  e.target.tagName === "INPUT" && changeCheck();
});

// 点击标题
function handleTableSort(el) {
  var dataIdx = parseInt(el.id.slice(-1));
  var items = Array.prototype.slice.call(rows).sort((a, b) => {
    var val1 = a.children[dataIdx].innerHTML,
      val2 = b.children[dataIdx].innerHTML;
    if (dataIdx === 1 || dataIdx === 3) {
      // 数字排序
      return val1 - val2;
    } else {
      // 中文排序
      return val1.localeCompare(val2, "zh");
    }
  });
  for (var i = 0; i < items.length; i++) {
    tbody.appendChild(items[i]);
  }
}
thead.addEventListener("click", function (e) {
  e.target.tagName === "TH" && handleTableSort(e.target);
});
