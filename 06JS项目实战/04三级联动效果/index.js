var provinceEl = document.querySelector("#province");
var cityEl = document.querySelector("#city");
var schoolEl = document.querySelector("#school");

// 初始化——渲染省会列表
function displayProvince() {
  var fragment = document.createDocumentFragment();
  for (var pro in province) {
    var option = document.createElement("option");
    option.value = pro;
    option.innerHTML = province[pro];
    fragment.appendChild(option);
  }
  provinceEl.appendChild(fragment);
}
displayProvince();

// 渲染学校列表
function displaySchool(ci) {
  var fragment = document.createDocumentFragment("fragment");
  for (var school in allschool[ci]) {
    var option = document.createElement("option");
    option.value = school;
    option.innerHTML = allschool[ci][school];
    fragment.appendChild(option);
  }
  schoolEl.appendChild(fragment);
}

// 渲染城市列表
function displayCity(pro) {
  var fragment = document.createDocumentFragment("fragment");
  for (var ci in city[pro]) {
    var option = document.createElement("option");
    option.value = ci;
    option.innerHTML = city[pro][ci];
    fragment.appendChild(option);
  }
  cityEl.appendChild(fragment);
  // 渲染学校列表
  displaySchool(cityEl.value);
}

// 省会列表变化——渲染城市列表和学校列表
provinceEl.addEventListener("change", function () {
  cityEl.innerHTML = "";
  schoolEl.innerHTML = "";
  if (this.value === -1) return;
  displayCity(this.value);
});

// 城市列表变化——渲染学校列表
cityEl.addEventListener("change", function () {
  schoolEl.innerHTML = "";
  displaySchool(this.value);
});
