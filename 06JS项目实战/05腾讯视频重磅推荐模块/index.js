var container = document.querySelector(".container");
var header = document.querySelector(".header");
var banner = document.querySelector(".banner");
var menu = document.querySelector(".menu");

// 当前显示的视频
var curIdx = 0;
// 轮播计时器
var timer = null;

// 根据下标渲染模块内容
function displayAll(idx) {
  banner.style.backgroundColor = data[idx].bg;
  banner.src = data[idx].img;
}

// 切换视频内容
function changeVideoContent(id) {
  curIdx = id;
  var el = document.querySelector(".menu-item[data-index='" + id + "']");
  // 切换激活样式
  var activeMenu = document.querySelector(".menu-item.active");
  if (activeMenu) {
    activeMenu.classList.remove("active");
  }
  el.classList.add("active");
  // 更换背景和header颜色
  displayAll(id);
}

// 初始化
function init() {
  // 计算右侧菜单栏高度
  var finalHeight = menu.clientHeight + data.length * 40;
  container.style.height = finalHeight + header.clientHeight + "px";
  menu.style.height = finalHeight + "px";
  // 渲染二级列表
  var fragment = document.createDocumentFragment();
  for (var id in data) {
    var a = document.createElement("a");
    a.setAttribute("data-index", id);
    a.href = "#";
    a.className = "menu-item";
    a.title = data[id].title;
    var span1 = document.createElement("span");
    span1.innerHTML = data[id].title;
    span1.className = "menu-item-title";
    a.appendChild(span1);
    var span2 = document.createElement("span");
    span2.innerHTML = data[id].desc;
    span2.className = "menu-item-desc";
    a.appendChild(span2);
    if (id === "0") a.classList.add("active");
    fragment.appendChild(a);
  }
  menu.appendChild(fragment);
  // 二级列表绑定鼠标悬浮事件
  var subMenu = document.querySelectorAll(".menu-item");
  for (var i = 0; i < subMenu.length; i++) {
    subMenu[i].addEventListener("mouseenter", function () {
      if (timer) {
        clearInterval(timer);
      }
      changeVideoContent(this.getAttribute("data-index"));
    });
    subMenu[i].addEventListener("mouseleave", function () {
      autoChange();
    });
  }
  displayAll(curIdx);
  autoChange();
}
init();

// 自动播放
function autoChange() {
  timer = setInterval(() => {
    curIdx++;
    changeVideoContent(curIdx);
  }, 1500);
}
