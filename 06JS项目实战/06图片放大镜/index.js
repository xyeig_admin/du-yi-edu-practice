var imageList = document.querySelector(".img-list");
var middleImg = document.querySelector(".middle-img");
var bigImg = document.querySelector(".big-img");
var mirror = document.querySelector(".mirror");

// 图片数据
var images = {
  // 小图
  small: ["imgA_1.jpg", "imgB_1.jpg", "imgC_1.jpg"],
  // 中图
  middle: ["imgA_2.jpg", "imgB_2.jpg", "imgC_2.jpg"],
  // 大图
  large: ["imgA_3.jpg", "imgB_3.jpg", "imgC_3.jpg"],
};

// 填充缩略图列表
function initImageList() {
  var fragment = document.createDocumentFragment();
  for (var i = 0; i < images.small.length; i++) {
    var img = document.createElement("img");
    img.id = "small" + i;
    img.src = `./丁永志/images/${images.small[i]}`;
    img.className = i === 0 ? "list-item active" : "list-item";
    fragment.appendChild(img);
  }
  imageList.appendChild(fragment);
}
initImageList();

// 根据当前激活的小图，渲染左侧中图和右侧大图
function displayImages(idx) {
  middleImg.style.backgroundImage = `url(./丁永志/images/${images.middle[idx]})`;
  bigImg.style.backgroundImage = `url(./丁永志/images/${images.large[idx]})`;
}
displayImages(0);

// 点击缩略图切换当前显示图片
function handleChangeImages(e) {
  if (e.target.tagName === "IMG") {
    var currentActive = imageList.querySelector(".list-item.active");
    if (currentActive) currentActive.classList.remove("active");
    e.target.classList.add("active");
    var idx = e.target.id.slice(-1);
    displayImages(idx);
  }
}
imageList.addEventListener("click", handleChangeImages);

var middleRect = middleImg.getBoundingClientRect();
var middleLeft = middleRect.left;
var middleTop = middleRect.top;
var mirrorRect = mirror.getBoundingClientRect();
// 鼠标进入左侧中图区域时，显示放大镜和右侧大图
function handleMouseEnterMiddle(e) {
  mirror.style.opacity = 1;
  bigImg.style.opacity = 1;
  middleImg.addEventListener("mousemove", handleMouseMoveMiddle);
}
// 鼠标在左侧中图区域内部移动时，放大镜跟随且鼠标位于其中心，右侧大图也跟随移动
function handleMouseMoveMiddle(e) {
  // 放大镜移动
  var disX = e.clientX - middleLeft,
    disY = e.clientY - middleTop;
  var newX = disX - mirrorRect.width / 2,
    newY = disY - mirrorRect.height / 2;
  if (newX < 0) newX = 0;
  if (newX > middleRect.width / 2) newX = middleRect.width - mirrorRect.width;
  if (newY < 0) newY = 0;
  if (newY > middleRect.height / 2)
    newY = middleRect.height - mirrorRect.height;
  mirror.style.left = newX + "px";
  mirror.style.top = newY + "px";
  // 大图移动
  bigImg.style.backgroundPosition = `${-newX}px ${-newY}px`;
}
// 鼠标离开左侧中图区域时，隐藏放大镜和右侧大图
function handleMouseLeaveMiddle(e) {
  mirror.style.opacity = 0;
  bigImg.style.opacity = 0;
}
middleImg.addEventListener("mouseenter", handleMouseEnterMiddle);
middleImg.addEventListener("mouseleave", handleMouseLeaveMiddle);
