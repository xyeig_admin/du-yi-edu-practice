var countText = document.querySelector("#count");
var prizes = document.querySelectorAll(".prize");
var lotteryBtn = document.querySelector(".center-play");
var dialogWrapper = document.querySelector(".dialog-wrapper");
var dialog = document.querySelector(".dialog");
var closeBtn = document.querySelector(".header .close");
var msgText = document.querySelector(".dialog .message");
var moreBtn = document.querySelector("button.footer");

// 渲染抽奖次数
var totalCount = 5;
var currentCount = 0;
countText.innerHTML = totalCount;

// 弹框关闭按钮
function handleCloseDialog() {
  dialogWrapper.style.display = "none";
}
closeBtn.addEventListener("click", handleCloseDialog);

// 弹框再来一次按钮
function handlePlayLotteryOneMore() {
  handleCloseDialog();
  currentCount < totalCount && playLotteryOnce();
}
moreBtn.addEventListener("click", handlePlayLotteryOneMore);

// 抽一次奖
var currentActive = 0;
var timer = null;
function playLotteryOnce() {
  if (currentCount < totalCount) {
    if (timer) clearInterval(timer);
    // 随机转
    var random = Math.floor(Math.random() * 10000 + 1000);
    timer = setInterval(() => {
      // 随机递减转的时间
      random -= Math.floor(Math.random() * 100 + 50);
      // 转完了
      if (random <= 0) {
        clearInterval(timer);
        // 显示弹框
        dialogWrapper.style.display = "block";
        // 渲染抽奖提示
        msgText.innerHTML =
          currentActive === 4
            ? `很遗憾您没有中奖`
            : `恭喜您获得${
                prizes[currentActive].querySelector(".prize-title").innerHTML
              }！`;
        // 计算剩余抽奖次数
        currentCount++;
        countText.innerHTML = totalCount - currentCount;
        // 弹框底部按钮
        moreBtn.innerHTML = currentCount === totalCount ? `确定` : `再来一次`;
        return;
      }
      prizes[currentActive].classList.remove("active");
      var nextActive = (currentActive + 1) % prizes.length;
      prizes[nextActive].classList.add("active");
      currentActive = nextActive;
    }, 50);
  }
}

// 绑定点击事件
lotteryBtn.addEventListener("click", playLotteryOnce);
