// 定时更换数字
function handleChangeTime(el, delay) {
  setInterval(() => {
    var firstChild = el.querySelectorAll("li")[0];
    el.style.marginTop = "-120px";
    el.style.transition = "all 0.1s linear";
    el.addEventListener("transitionend", function () {
      el.style.marginTop = "0px";
      el.style.transition = "none";
      el.appendChild(firstChild);
    });
  }, delay);
}
handleChangeTime(document.querySelector("#hour_ten"), 1000 * 60 * 60 * 3);
handleChangeTime(document.querySelector("#hour_one"), 1000 * 60 * 60);
handleChangeTime(document.querySelector("#minute_ten"), 1000 * 60 * 10);
handleChangeTime(document.querySelector("#minute_one"), 1000 * 60);
handleChangeTime(document.querySelector("#second_ten"), 1000 * 10);
handleChangeTime(document.querySelector("#second_one"), 1000);
