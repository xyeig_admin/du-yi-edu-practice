(function () {
  var container = document.querySelector(".container");
  var imagesList = document.querySelector(".images-list");
  var toggleBar = document.querySelector(".toggle-bar");
  var prev = document.querySelector(".prev");
  var next = document.querySelector(".next");

  /**
   * 初始化
   */
  // 容器/每张图片宽度
  var containerWidth = container.clientWidth;
  // 当前正在显示的图片下标
  var curIndex = 0;
  // 图片数据
  var images = [
    "./袁进/img/Wallpaper1.jpg",
    "./袁进/img/Wallpaper2.jpg",
    "./袁进/img/Wallpaper3.jpg",
    "./袁进/img/Wallpaper4.jpg",
    "./袁进/img/Wallpaper5.jpg",
  ];

  // 添加一张图片
  function createImage(url) {
    var img = document.createElement("img");
    img.src = url;
    imagesList.appendChild(img);
  }

  function init() {
    var fragment = document.createDocumentFragment();
    for (var i = 0; i < images.length; i++) {
      createImage(images[i]);
      var span = document.createElement("span");
      span.id = "bar_item" + i;
      span.className = i === 0 ? "bar-item active" : "bar-item";
      fragment.appendChild(span);
    }
    // 末尾追加第一张图片，动画结束回到第一张时实现假视觉无滚动效果（无缝）
    createImage(images[0]);
    toggleBar.appendChild(fragment);
  }
  init();

  /**
   * 交互
   */
  // 根据当前显示的图片的下标，切换底部条的激活状态
  function handleToggleBarActive() {
    var curActive = toggleBar.querySelector(".active");
    if (curActive) {
      curActive.classList.remove("active");
    }
    var idx = curIndex;
    if (idx > images.length - 1) {
      idx = 0;
    }
    toggleBar.children[idx].classList.add("active");
  }

  // 变换一次图片
  var totalMS = 500;
  var isMoving = false;
  function handleChangeImage(idx, onend) {
    if (isMoving || idx === curIndex) return;
    isMoving = true;
    createAnimation({
      from: parseInt(imagesList.style.marginLeft) || 0,
      to: -idx * containerWidth,
      totalMS,
      onmove: function (n) {
        imagesList.style.marginLeft = n + "px";
      },
      onend: function () {
        isMoving = false;
        onend && onend();
      },
    });
    curIndex = idx;
    handleToggleBarActive();
  }

  // 下一张图片
  function handleChangeImageNext() {
    // handleChangeImage((curIndex + 1) % images.length);
    var idx = curIndex + 1;
    handleChangeImage(idx, function () {
      if (idx === imagesList.children.length - 1) {
        imagesList.style.marginLeft = 0;
        curIndex = 0;
      }
    });
  }
  next.addEventListener("click", handleChangeImageNext);

  // 上一张图片
  function handleChangeImagePrev() {
    // handleChangeImage((curIndex + images.length - 1) % images.length);
    var idx = curIndex - 1;
    var maxLength = imagesList.children.length - 1;
    if (idx === -1) {
      imagesList.style.marginLeft = -maxLength * containerWidth + "px";
      idx = maxLength - 1;
    }
    handleChangeImage(idx);
  }
  prev.addEventListener("click", handleChangeImagePrev);

  // 点击底部条
  toggleBar.addEventListener("click", function (e) {
    if (e.target.tagName === "SPAN") {
      handleChangeImage(parseInt(e.target.id.slice(-1)));
    }
  });

  // 自动轮播
  var timer = null;
  var delay = 1500;
  function start() {
    if (timer) return;
    timer = setInterval(() => {
      handleChangeImageNext();
    }, delay);
  }
  function stop() {
    clearInterval(timer);
    timer = null;
  }
  start();
  container.addEventListener("mouseenter", stop);
  container.addEventListener("mouseleave", start);
})();
