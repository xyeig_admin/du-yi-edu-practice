(function () {
  var container = document.querySelector(".container");

  // 图片数量
  var imageCount = 40;
  // 图片宽度
  var imageWidth = 220;

  /**
   * 初始化
   */
  function init() {
    for (var i = 0; i < imageCount; i++) {
      var img = document.createElement("img");
      img.src = `./谢杰/img/${i}.jpg`;
      img.style.width = imageWidth + "px";
      container.appendChild(img);
      img.onload = handleResizeWindow;
    }
  }
  init();

  /**
   * 交互
   */
  // 计算列数和间隙数
  function calcColumnsAndGaps() {
    // 容器宽度
    var containerWidth = container.clientWidth;
    // 列数（一行最多放多少张图片）
    var columns = Math.floor(containerWidth / imageWidth);
    // 间隙数（左右贴边）
    var gaps = columns - 1;
    // 剩余间隙宽度
    var space = containerWidth - columns * imageWidth;
    // 每个间隙宽度
    var gap = space / gaps;
    return {
      columns,
      gap,
    };
  }

  // 获取数组最小值的下标
  function getMin(array) {
    var min = array[0],
      idx = 0;
    for (var arr in array) {
      if (array[arr] < min) {
        min = array[arr];
        idx = arr;
      }
    }
    return {
      minHeight: min,
      minHeightIndex: idx,
    };
  }

  // 获取数组最大值的高度
  function getMax(array) {
    var max = array[0];
    for (var arr in array) {
      if (array[arr] > max) {
        max = array[arr];
      }
    }
    return max;
  }

  // 布局
  function handleResizeWindow() {
    const { columns, gap } = calcColumnsAndGaps();
    // 存储每一列的当前高度
    var imageArray = new Array(columns).fill(0);
    // 遍历处理每一张图片
    var imageList = container.querySelectorAll("img");
    for (var i = 0; i < imageList.length; i++) {
      var curImage = imageList[i];
      // 获取最小高度的一列
      const { minHeight, minHeightIndex } = getMin(imageArray);
      // 设置top值
      curImage.style.top = minHeight + "px";
      // 更新数组当前列高度：原高度+当前图片高度+间隙
      imageArray[minHeightIndex] += curImage.height + gap;
      // 设置left值
      curImage.style.left =
        minHeightIndex * gap + minHeightIndex * imageWidth + "px";
    }
    // 获取最大高度的一列
    const maxHeight = getMax(imageArray);
    // 设置容器高度
    container.style.height = maxHeight + "px";
  }

  // 窗口变化时重新布局
  var timer = null;
  window.onresize = function () {
    if (timer) clearTimeout(timer);
    timer = setTimeout(() => {
      handleResizeWindow();
    }, 500);
  };
})();
