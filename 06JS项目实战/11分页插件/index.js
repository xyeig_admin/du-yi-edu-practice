/**
 * 创建分页器
 * @param {number} currentPage 当前在第几页
 * @param {number} totalPages 总页数
 * @param {number} showPages 显示的页数
 * @param {number} container 分页容器
 */
function createPagination(currentPage, totalPages, showPages, container) {
  // 清空原有分页器
  container.innerHTML = "";

  // 创建文档片段，存放分页按钮
  var fragment = document.createDocumentFragment();

  /**
   * 创建分页器的分页按钮
   * @param {string} className 当前分页要添加的样式
   * @param {string} innerText 当前分页显示的文本
   * @param {number} toPage 点击当前分页跳转的页码
   */
  function createPaginationItem(className, innerText, toPage) {
    var a = document.createElement("a");
    a.className = className;
    a.innerText = innerText;
    a.addEventListener("click", function () {
      // 第一页/最后一页/当前页 不能跳转
      if (toPage < 1 || toPage > totalPages || toPage === currentPage) return;
      // 跳转下一页（重新创建一个分页器）
      createPagination(toPage, totalPages, showPages, container);
    });
    fragment.appendChild(a);
  }

  /**
   * 创建【首页】、【上一页】
   */
  if (currentPage === 1) {
    createPaginationItem("disabled", "首页", 1);
    createPaginationItem("disabled", "上一页", currentPage - 1);
  } else {
    createPaginationItem("", "首页", 1);
    createPaginationItem("", "上一页", currentPage - 1);
  }

  /**
   * 创建中间的分页
   */
  // 计算显示的最小页码
  var minPage = Math.floor(currentPage - showPages / 2);
  if (minPage < 1) minPage = 1;
  // 计算显示的最大页码
  var maxPage = minPage + showPages - 1;
  if (maxPage > totalPages) maxPage = totalPages;
  // 创建分页
  for (var i = minPage; i <= maxPage; i++) {
    createPaginationItem(i === currentPage ? "active" : "", i, i);
  }

  /**
   * 创建【下一页】、【尾页】
   */
  if (currentPage === totalPages) {
    createPaginationItem("disabled", "下一页", currentPage + 1);
    createPaginationItem("disabled", "尾页", totalPages);
  } else {
    createPaginationItem("", "下一页", currentPage + 1);
    createPaginationItem("", "尾页", totalPages);
  }

  /**
   * 创建当前页码和总页数
   */
  var span = document.createElement("span");
  span.innerHTML = `${currentPage} / ${totalPages}`;
  fragment.appendChild(span);

  container.appendChild(fragment);
}
