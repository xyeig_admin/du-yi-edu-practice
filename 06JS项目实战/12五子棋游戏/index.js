(function () {
  var chessBoard = document.querySelector(".chess-board");

  // 棋盘行列数
  var chessCount = 15;
  // 下一颗棋子的颜色（黑棋先手）
  var chessColor = "black";
  // 存储棋子落下的坐标信息
  var chessArray = [];
  // 游戏是否已结束
  var isGameOver = false;

  /**
   * 初始化
   */
  function init() {
    isGameOver = false;
    chessColor = "black";
    chessArray = [];
    var html = "";
    for (var i = 0; i < chessCount; i++) {
      html += "<tr>";
      for (var j = 0; j < chessCount; j++) {
        html += `<td data-row="${i}" data-col="${j}"></td>`;
      }
      html += "</tr>";
    }
    chessBoard.innerHTML = html;
  }
  init();

  /**
   * 交互
   */
  // 判断当前棋格是否已落子
  function existChess(chess) {
    return chessArray.find((ch) => ch.col === chess.x && ch.row === chess.y);
  }
  // 落子
  function playChess(chess) {
    if (!isGameOver && !existChess(chess)) {
      // 游戏未结束且当前棋格未落子
      const { x, y, c } = chess;
      // 更新棋子数组
      chessArray.push(chess);
      // 生成棋子
      var newChess = `<div class="chess ${c}" data-row="${y}" data-col="${x}"></div>`;
      // 落子
      if (x < chessCount && y < chessCount) {
        // 点击的位置在棋盘内
        var target = chessBoard.querySelector(
          `td[data-row="${y}"][data-col="${x}"]`
        );
        target.innerHTML += newChess;
      } else if (x === chessCount && y < chessCount) {
        // 点击的位置在棋盘右边缘线上
        var target = chessBoard.querySelector(
          `td[data-row="${y}"][data-col="${x - 1}"]`
        );
        target.innerHTML += newChess;
        target.lastChild.style.left = "50%";
      } else if (x < chessCount && y === chessCount) {
        // 点击的位置在棋盘下边缘线上
        var target = chessBoard.querySelector(
          `td[data-row="${y - 1}"][data-col="${x}"]`
        );
        target.innerHTML += newChess;
        target.lastChild.style.top = "50%";
      } else if (x === chessCount && y === chessCount) {
        // 点击的位置在棋盘最右下角
        var target = chessBoard.querySelector(
          `td[data-row="${y - 1}"][data-col="${x - 1}"]`
        );
        target.innerHTML += newChess;
        target.lastChild.style.top = "50%";
        target.lastChild.style.left = "50%";
      }
      // 交换黑白手
      chessColor = chessColor === "black" ? "white" : "black";
    }
    judge();
  }
  // 点击棋盘
  chessBoard.addEventListener("click", function (e) {
    if (!isGameOver) {
      // 游戏未结束
      if (e.target.tagName === "TD") {
        // 当前点击棋格的横纵坐标
        const { row, col } = e.target.dataset;
        // 计算每个棋格的宽高的一半
        var halfBoard = Math.floor(chessBoard.clientWidth / chessCount) / 2;
        // 封装当前棋格相关信息（横坐标、纵坐标、棋子颜色）
        var chess = {
          x: e.offsetX > halfBoard ? parseInt(col) + 1 : parseInt(col),
          y: e.offsetY > halfBoard ? parseInt(row) + 1 : parseInt(row),
          c: chessColor,
        };
        // 落子
        playChess(chess);
      }
    } else {
      // 游戏结束，初始化游戏
      if (window.confirm("游戏已结束，是否开启新一局游戏？")) {
        init();
      }
    }
  });
  // 根据坐标遍历棋子数组，返回棋子对象
  function findChess(x, y, c) {
    return chessArray.find((ch) => ch.x === x && ch.y === y && ch.c === c);
  }
  // 结束游戏
  function endGame() {
    // 更新标志
    isGameOver = true;
    // 显示棋盘上棋子的序号
    for (var i = 0; i < chessArray.length; i++) {
      chessBoard.querySelector(
        `div[data-row="${chessArray[i].y}"][data-col="${chessArray[i].x}"]`
      ).innerHTML = i + 1;
    }
    // 高亮获胜的五颗棋子
    for (var i = 0; i < arguments.length; i++) {
      chessBoard
        .querySelector(
          `div[data-row="${arguments[i].y}"][data-col="${arguments[i].x}"]`
        )
        .classList.add("highlight");
    }
  }
  // 判定胜负
  function judge() {
    for (var i = 0; i < chessArray.length; i++) {
      var chess1 = chessArray[i];
      var chess2, chess3, chess4, chess5;
      // 横向是否有五颗同色棋子
      chess2 = findChess(chess1.x + 1, chess1.y, chess1.c);
      chess3 = findChess(chess1.x + 2, chess1.y, chess1.c);
      chess4 = findChess(chess1.x + 3, chess1.y, chess1.c);
      chess5 = findChess(chess1.x + 4, chess1.y, chess1.c);
      if (chess2 && chess3 && chess4 && chess5) {
        endGame(chess1, chess2, chess3, chess4, chess5);
      }
      // 纵向是否有五颗同色棋子
      chess2 = findChess(chess1.x, chess1.y + 1, chess1.c);
      chess3 = findChess(chess1.x, chess1.y + 2, chess1.c);
      chess4 = findChess(chess1.x, chess1.y + 3, chess1.c);
      chess5 = findChess(chess1.x, chess1.y + 4, chess1.c);
      if (chess2 && chess3 && chess4 && chess5) {
        endGame(chess1, chess2, chess3, chess4, chess5);
      }
      // 斜向左下是否有五颗同色棋子
      chess2 = findChess(chess1.x - 1, chess1.y + 1, chess1.c);
      chess3 = findChess(chess1.x - 2, chess1.y + 2, chess1.c);
      chess4 = findChess(chess1.x - 3, chess1.y + 3, chess1.c);
      chess5 = findChess(chess1.x - 4, chess1.y + 4, chess1.c);
      if (chess2 && chess3 && chess4 && chess5) {
        endGame(chess1, chess2, chess3, chess4, chess5);
      }
      // 斜向右下是否有五颗同色棋子
      chess2 = findChess(chess1.x + 1, chess1.y + 1, chess1.c);
      chess3 = findChess(chess1.x + 2, chess1.y + 2, chess1.c);
      chess4 = findChess(chess1.x + 3, chess1.y + 3, chess1.c);
      chess5 = findChess(chess1.x + 4, chess1.y + 4, chess1.c);
      if (chess2 && chess3 && chess4 && chess5) {
        endGame(chess1, chess2, chess3, chess4, chess5);
      }
    }
  }
})();
