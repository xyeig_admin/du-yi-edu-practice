(function () {
  var roundContainer = document.querySelector(".top-round");
  var resultImage = roundContainer.querySelector(".result-image");
  var roundImage = roundContainer.querySelector(".round-image");
  var resultContainer = document.querySelector(".right-content");

  // 图片数量
  var imagesCount = 16;
  // 游戏是否结束
  var isGameOver = false;

  // 产生一个随机数
  function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }

  /**
   * 初始化
   */
  function init() {
    isGameOver = false;
    roundContainer.style.transition = "";
    roundContainer.style.transform = "";
    roundContainer.removeEventListener("transitionend", handleTransitionEnd);
    roundImage.style.opacity = 1;
    resultContainer.innerHTML = "";

    // 决定结果图形
    var res = getRandom(0, imagesCount);
    resultImage.src = `./谢杰/images/values/${res}.png`;

    // 生成字典
    var html = "";
    for (var i = 0; i < 100; i++) {
      // 决定当前字典序号要显示什么图片（9的倍数必须显示结果图形）
      var itemImage = i % 9 === 0 ? res : getRandom(0, imagesCount);

      html += `<div class="right-item">
        <span>${i}</span>
        <img src="./谢杰/images/values/${itemImage}.png" alt="${itemImage}.png" />
      </div>`;
    }
    resultContainer.innerHTML = html;
  }
  init();

  /**
   * 交互
   */
  // 转盘转动结束后
  function handleTransitionEnd() {
    roundImage.style.opacity = 0;
    isGameOver = true;
  }
  // 点击转盘
  roundContainer.addEventListener("click", function () {
    if (!isGameOver) {
      roundContainer.style.transition = "all 3s ease-in-out";
      roundContainer.style.transform = "rotate(1800deg)";
      roundContainer.addEventListener("transitionend", handleTransitionEnd);
    } else {
      if (window.confirm("是否再来一局？")) {
        init();
      }
    }
  });
})();
