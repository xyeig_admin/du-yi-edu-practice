(function () {
  var topChange = document.querySelector(".top-change");
  var resultHTML = document.querySelector(".bottom-content p");
  var imagesDiv = document.querySelector(".images");
  var imagesRect = document.querySelector(".images-rect");
  var imagesContent = document.querySelector(".images-content");
  var slidesDiv = document.querySelector(".slides");
  var slidesText = document.querySelector(".slides span");
  var slidesButton = document.querySelector(".slides-btn");

  // 获得一个随机数
  function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }

  /**
   * 初始化
   */
  // 图片数据
  var images = [
    "./谢杰/img/t1.png",
    "./谢杰/img/t2.png",
    "./谢杰/img/t3.png",
    "./谢杰/img/t4.png",
    "./谢杰/img/t5.png",
  ];
  function init() {
    // 重置标题
    resultHTML.innerHTML = "请完成图片验证";
    resultHTML.className = "normal";
    // 显示目标区域
    imagesRect.style.opacity = 1;
    // 隐藏滑块
    imagesContent.style.opacity = 0;
    // 滑动按钮归位
    slidesButton.style.left = "-5px";
    // 显示滑动区域文本
    slidesText.style.opacity = 1;

    // 获得随机一张图片
    var randomImg = getRandom(0, images.length);
    imagesDiv.style.backgroundImage = `url("${images[randomImg]}")`;
    imagesContent.style.backgroundImage = `url(${images[randomImg]})`;
    // 计算目标区域的位置（容器的右半侧）
    var randomTop = getRandom(
      0,
      imagesDiv.clientHeight - imagesRect.clientHeight
    );
    var randomLeft = getRandom(
      imagesDiv.clientWidth / 2,
      imagesDiv.clientWidth - imagesRect.clientWidth
    );
    imagesRect.style.top = randomTop + "px";
    imagesRect.style.left = randomLeft + "px";
    // 滑块和目标区域等高，且在最左侧
    imagesContent.style.top = randomTop + "px";
    imagesContent.style.left = "0px";
    imagesContent.style.backgroundPosition = `${-randomLeft}px ${-randomTop}px`;

    // 绑定鼠标按下事件
    slidesButton.onmousedown = function (ed) {
      // 修改标题
      resultHTML.innerHTML = "拖动滑块完成验证";
      resultHTML.className = "normal";
      // 显示滑块
      imagesContent.style.opacity = 1;
      imagesContent.style.transition = "";
      slidesButton.style.transition = "";
      // 隐藏滑动区域文本
      slidesText.style.opacity = 0;

      // 绑定鼠标拖动事件
      slidesDiv.onmousemove = function (em) {
        // 按钮左侧距离 = 按下时的横坐标 - 按下位置距离按钮左边缘的距离 - 滑动区域距离窗口左边缘的距离
        var buttonLeft = em.clientX - ed.offsetX - slidesDiv.offsetLeft;
        // 按钮最右位置
        var maxLeft = slidesDiv.clientWidth - slidesButton.clientWidth + 5;
        // 滑块最右位置
        var maxRectLeft = imagesDiv.clientWidth - imagesContent.clientWidth;
        // 处理边界值
        if (buttonLeft < -5) buttonLeft = -5;
        else if (buttonLeft > maxLeft) buttonLeft = maxLeft;
        // 更新滑块和滑动按钮位置
        slidesButton.style.left = buttonLeft + "px";
        imagesContent.style.left =
          buttonLeft + 5 > maxRectLeft
            ? maxRectLeft + "px"
            : buttonLeft + 5 + "px";
      };

      // 绑定鼠标抬起事件
      document.onmouseup = function () {
        var distance = imagesRect.offsetLeft - imagesContent.offsetLeft;
        if (distance > -5 && distance < 5) {
          // 验证成功

          // 修改标题
          resultHTML.innerHTML = "验证成功";
          resultHTML.className = "success";
          // 隐藏滑块和目标区域
          imagesRect.style.opacity = 0;
          imagesContent.style.opacity = 1;
          // 移除事件
          slidesButton.onmousedown =
            slidesDiv.onmousemove =
            document.onmouseup =
              null;
        } else {
          // 验证失败

          // 修改标题
          resultHTML.innerHTML = "验证失败";
          resultHTML.className = "error";
          // 滑块和滑动按钮归位
          slidesButton.style.left = "-5px";
          imagesContent.style.left = "0px";
          slidesButton.style.transition = "all 0.2s ease-in-out";
          imagesContent.style.transition = "all 0.2s ease-in-out";
          // 显示滑动文本
          slidesText.style.opacity = 1;
          // 移除事件
          slidesDiv.onmousemove = document.onmouseup = null;
        }
      };
    };
  }
  init();

  /**
   * 交互
   */
  // 换一张
  topChange.addEventListener("click", init);
})();
