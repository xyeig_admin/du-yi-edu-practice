var doms = {
  // 视频元素
  video: document.querySelector("video"),
  // 播放按钮
  btnPlay: document.querySelector("#btnPlay"),
  // 进度条相关元素
  progress: {
    // 进度条
    range: document.querySelector("#progress input[type='range']"),
    // 当前播放时间
    current: document.querySelector("#current"),
    // 总时间
    total: document.querySelector("#total"),
  },
  // 播放速率的容器
  rate: document.querySelector("#rate"),
  // 音量相关元素
  volume: {
    // 滑动块
    range: document.querySelector("#volume input"),
    // 文本
    text: document.querySelector("#volume span"),
  },
  // 保存和设置按钮
  buttons: {
    save: document.querySelector("#save"),
    load: document.querySelector("#load"),
  },
  // 播放器相关元素
  controls: document.querySelectorAll(".controls"),
};

// 初始化
doms.video.addEventListener("loadeddata", init);
function init() {
  setProgress();
  setRate();
  setVolume();
  for (let i = 0; i < doms.controls.length; i++) {
    doms.controls[i].style.display = "block";
  }
}

/**
 * 根据当前视频的播放进度，设置进度条的状态
 */
function setProgress() {
  // 1.设置文本
  doms.progress.current.innerHTML = formatTime(doms.video.currentTime);
  doms.progress.total.innerHTML = formatTime(doms.video.duration);
  // 2.设置进度条
  doms.progress.range.value =
    (doms.video.currentTime / doms.video.duration) * 100;
}

/**
 * 格式化时间戳
 * @param {number} timestamp 时间戳
 */
function formatTime(timestamp) {
  let hours = 0,
    minutes = 0,
    seconds = 0;
  if (timestamp > 3600) {
    hours = Math.floor(timestamp / 3600);
    timestamp -= hours * 3600;
  }
  if (timestamp > 60) {
    minutes = Math.floor(timestamp / 60);
    timestamp -= minutes * 60;
  }
  seconds = Math.floor(timestamp);
  // 不足两位数前补0
  function _format(t) {
    return ("00" + t).slice(-2);
    // return t < 10? `0${t}` : t;
  }
  return `${_format(hours)}:${_format(minutes)}:${_format(seconds)}`;
}

/**
 * 根据当前的视频，设置播放速率的状态
 */
function setRate() {
  let btns = doms.rate.querySelectorAll("button");
  for (let i = 0; i < btns.length; i++) {
    let rate = btns[i].dataset.rate;
    if (+rate === doms.video.playbackRate) {
      btns[i].classList.add("active");
    } else {
      btns[i].classList.remove("active");
    }
  }
}

/**
 * 设置音量的状态
 */
function setVolume() {
  let percents = Math.floor(doms.video.volume * 100);
  if (doms.video.muted) percents = 0;
  // 1.设置文本
  doms.volume.text.innerHTML = percents + "%";
  // 2.设置进度条
  doms.volume.range.value = percents;
}

// 交互
// 播放暂停
doms.btnPlay.addEventListener("click", function () {
  if (doms.video.paused) {
    doms.video.play();
  } else {
    doms.video.pause();
  }
});

// 调整播放进度条
doms.progress.range.addEventListener("input", function () {
  doms.video.currentTime = (this.value / 100) * doms.video.duration;
  setProgress();
});

// 播放时时长逐帧变化
doms.video.addEventListener("timeupdate", setProgress);

// 调整播放速率
let btnRates = doms.rate.querySelectorAll("button");
for (let i = 0; i < btnRates.length; i++) {
  btnRates[i].addEventListener("click", function () {
    doms.video.playbackRate = +this.dataset.rate;
    setRate();
  });
}

// 调整音量
doms.volume.range.addEventListener("input", function () {
  doms.video.volume = +this.value / 100;
  setVolume();
});

// 保存设置
doms.buttons.save.addEventListener("click", function () {
  let obj = {
    currentTime: doms.video.currentTime,
    rate: doms.video.playbackRate,
    volume: doms.video.volume,
  };
  localStorage.setItem("videoData", JSON.stringify(obj));
  alert("保存设置成功！");
});

// 恢复设置
doms.buttons.load.addEventListener("click", function () {
  let obj = JSON.parse(localStorage.getItem("videoData"));
  doms.video.currentTime = obj.currentTime;
  doms.video.playbackRate = obj.rate;
  doms.video.volume = obj.volume;
  setProgress();
  setRate();
  setVolume();
  doms.video.play();
});
