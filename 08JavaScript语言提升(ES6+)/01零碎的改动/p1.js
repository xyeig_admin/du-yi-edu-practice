// 下面的代码段是否有错误，错在哪里？

// 代码段1
// let a;
// const b; // 错误
console.log("const定义变量时必须初始化");

// 代码段2
// let a = 1;
// const b = 2;
// a++;
// b++; // 错误
console.log("const定义的变量是常量，值一经初始化就不可更改");

// 代码段3
// console.log(a); // 错误
// let a = 1;
console.log("变量定义之前不能使用");

// 代码段4
// let a = 1;
// let a = 2; // 错误
console.log("不可以重复定义同名变量");
