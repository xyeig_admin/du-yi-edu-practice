// 得到一个随机数组成的数组，数组长度为10，随机数的范围在0-1之间
// 结果类似于：[0.262, 0.167, 0.841, ...]
let randomArr = new Array(10)
  .fill(0)
  .map(() => parseFloat(Math.random()).toFixed(3));
console.log(randomArr);

// 得到一个随机数组成的数组，数组长度为10，随机数的范围在10-100之间
// 结果类似于：[35, 66, 45, ...]
let randomArr2 = new Array(10)
  .fill(0)
  .map(() => Math.floor(Math.random() * 90 + 10));
console.log(randomArr2);

// 判断某个字符串s是否为 .jpg、.png、.bmp、.gif 中的一个
function judgeString(s) {
  const legalArr = [".jpg", ".png", ".bmp", ".gif"];
  return legalArr.includes(s);
}
console.log(judgeString(".jpg"));
console.log(judgeString(".jpeg"));
