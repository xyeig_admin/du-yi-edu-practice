const obj = {
  a: 1,
  b: 2,
  c: 3,
};
// 遍历对象的所有属性名
console.log(...Object.keys(obj));

// 遍历对象的所有属性值
console.log(...Object.values(obj));

// 遍历对象的所有属性名和属性值
for (const [key, val] of Object.entries(obj)) {
  console.log(`${key}: ${val}`);
}
// Object.entries(obj).forEach(function ([k, v]) {
//   console.log(k, v);
// });

// 复制obj的所有属性到一个新的对象
const newObj = { ...obj };
console.log(newObj, Object.is(obj, newObj));

// 复制obj除a以外的所有属性到一个新的对象
const { a, ...newObj2 } = obj;
console.log(newObj2);
