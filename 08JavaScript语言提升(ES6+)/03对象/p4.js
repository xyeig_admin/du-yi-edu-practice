// 遍历对象 user， 将其每一个属性变为 getter 和 setter， 保持读写功能不变
// 读取属性时，输出：正在读取xxx属性，值为xxx
// 给属性赋值时，输出：正在设置xxx属性，新的值为xxx
const user = {
  name: "monica",
  age: 17,
  sex: "female",
};

Object.entries(user).forEach(([key, val]) => {
  Object.defineProperty(user, key, {
    get() {
      console.log(`正在读取${key}属性，值为${val}`);
      return val;
    },
    set(newVal) {
      console.log(`正在设置${key}属性，新的值为${newVal}`);
      val = newVal;
    },
  });
});
console.log(user.name);
user.name = "Sutee";
console.log(user.age);
user.age = 24;
console.log(user.sex);
user.sex = "女";
