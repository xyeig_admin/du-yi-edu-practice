const counter = {
  count: 0,
  // 完成该函数，调用该函数后，每隔一秒就会增加count的值，然后输出它
  timer: null,
  startIncrease() {
    if (this.timer) {
      clearInterval(this.timer);
      this.count = 0;
    }
    this.timer = setInterval(() => {
      this.count++;
      console.log(this.count);
    }, 1000);
  },
};
// counter.startIncrease();
