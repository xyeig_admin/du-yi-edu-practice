// 下面代码的输出结果是什么

new Promise((resolve, reject) => {
  resolve();
})
  .then((res) => {
    console.log(res.toString()); // undefined没有toString方法，rejected
    return 2;
  })
  .catch((err) => {
    return 3; // 处理上一个任务的失败，fulfilled，3
  })
  .then((res) => {
    console.log(res); // 3
  });
