// 下面代码的输出结果是什么

new Promise((resolve, reject) => {
  throw new Error(1); // rejected，Error: 1
})
  .then((res) => {
    // 没有对上一个任务失败作处理，状态和上一个任务一致，即：rejected，Error: 1
    console.log(res);
    return new Error("2");
  })
  .catch((err) => {
    throw err; // 抛出错误，Error: 1
    return 3;
  })
  .then((res) => {
    console.log(res); // 没有对上一个任务失败作处理，状态和上一个任务一致，即：rejected，Error: 1
  });
