// 下面代码的输出结果是什么

new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(1);
  }, 1000);
})
  .then((data) => {
    throw 3; // 抛出错误，3，没有catch处理报错
    return data + 1;
  })
  .then((data) => {
    console.log(data);
  });

// node:internal/process/promises:288
//   triggerUncaughtException(err, true /* fromPromise */);
//   ^
// [UnhandledPromiseRejection: This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). The promise rejected with the reason "3".] {
//   code: 'ERR_UNHANDLED_REJECTION'
// }
