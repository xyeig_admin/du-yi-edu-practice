// 下面代码的输出结果是什么

const pro = new Promise((resolve, reject) => {
  resolve(1);
})
  .then((res) => {
    console.log(res); // 1
    return 2;
  })
  .catch((err) => {
    return 3;
  })
  .then((res) => {
    console.log(res); // 2
  });

setTimeout(() => {
  console.log(pro); // Promise {<fulfilled>} undefined
}, 1000);
