// 下面代码的输出结果是什么

const pro = new Promise((resolve, reject) => {
  resolve();
})
  .then((res) => {
    console.log(res.toString()); // 抛出错误，undefined没有toString，rejected
    return 2;
  })
  .catch((err) => {
    return 3; // 处理上一个任务的失败，fulfilled，3
  })
  .then((res) => {
    console.log(res); // 3，处理上一个任务的成功，fulfilled，undefined（没有return
  });

setTimeout(() => {
  console.log(pro); // Promise {<fulfilled>} undefined
}, 1000);
