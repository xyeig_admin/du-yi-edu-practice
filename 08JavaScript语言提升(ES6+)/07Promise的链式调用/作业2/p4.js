// 下面代码的输出结果是什么

new Promise((resolve, reject) => {
  resolve(1);
})
  .then((res) => {
    console.log(res); // 1
    return new Error("2"); // 抛出错误，Error: '2'
  })
  .catch((err) => {
    throw err; // 抛出错误，'2'
    return 3;
  })
  .then((res) => {
    console.log(res); // 没有对上一个任务的失败作处理，不运行
  });

/**
1
Error: 2
  at p4.js:8:12
*/
