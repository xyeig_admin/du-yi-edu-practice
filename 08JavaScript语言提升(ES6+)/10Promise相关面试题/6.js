async function m1() {
  return 1;
}

async function m2() {
  const n = await m1();
  console.log(n);
  return 2;
}

async function m3() {
  const n = m2();
  console.log(n);
  return 3;
}

m3().then((n) => {
  console.log(n);
});

m3();

console.log(4);

/**
 * 输出
 * Promise {<pending>}
 * Promise {<pending>}
 * 4
 * 1
 * 3
 * 1
 */

/**
 * 解析
 * Promise {<pending>} // 13行，输出的是m2的状态（执行17行后的输出：m3没有await，不用等待12行m2完成，往后执行）
 * Promise {<pending>} // 13行，输出的是m2的状态（执行21行后的输出：m2依旧pending）
 * 4 // 23行
 * 1 // 7行（微队列任务1，m2状态fulfilled并return 2）
 * 3 // 18行（微队列任务2，17行的m3成功后的回调输出）
 * 1 // 7行（微队列任务3，21行的m3执行时的m1的回调输出）
 */
