var a;
var b = new Promise((resolve, reject) => {
  console.log("promise1");
  setTimeout(() => {
    resolve();
  }, 1000);
})
  .then(() => {
    console.log("promise2");
  })
  .then(() => {
    console.log("promise3");
  })
  .then(() => {
    console.log("promise4");
  });

a = new Promise(async (resolve, reject) => {
  console.log(a);
  await b;
  console.log(a);
  console.log("after1");
  await a;
  resolve(true);
  console.log("after2");
});

console.log("end");

/**
 * 输出
 * promise1
 * undefined
 * end
 * promise2
 * promise3
 * promise4
 * Promise {<pending>}
 * after1
 */

/**
 * 解析
 * promise1 // 延迟1s，转去执行a的new Promise
 * undefined // 19行（a变量提升，a的new Promise还未执行完成，所以还未赋值给a）
 * end // a在await b，转去执行全局剩余代码，全局执行完成继续执行微队列中的任务
 * promise2
 * promise3
 * promise4
 * Promise {<pending>} // 21行（await b完成，但是b没有resolve，状态未改变）
 * after1 // await a，但是a的resolve在await之后，所以状态一直是pending
 */
