(async () => {
  const lyricsUl = document.querySelector("ul.lyrics");
  const music = document.querySelector("audio");
  let lrcArr = [];
  const lrcSize = {
    lineHeight: 0,
    listHeight: 0,
  };

  /**
   * 从网络获取歌词数据
   * @returns {Promise} 歌词字符串
   */
  async function getLrc() {
    return await fetch("https://study.duyiedu.com/api/lyrics")
      .then((resp) => resp.json())
      .then((resp) => resp.data);
  }

  /**
   * 格式化歌词出现时间
   * @param {String} lrcTime 每一行歌词出现时间
   */
  function formatLrcTime(lrcTime) {
    const minute = parseInt(lrcTime.substring(1, lrcTime.indexOf(":")));
    const second = parseFloat(lrcTime.substring(lrcTime.indexOf(":") + 1));
    return +parseFloat(minute * 60 + second).toFixed(2);
  }

  /**
   * 格式化每一行歌词信息
   * @param {String} lrc 歌词字符串
   * @returns {Object} 每一行歌词出现时间、每一行歌词文本
   */
  function formatLrc(lrc) {
    return {
      time: formatLrcTime(lrc.substring(1, lrc.lastIndexOf("]"))),
      text: lrc.substring(lrc.lastIndexOf("]") + 1),
    };
  }

  /**
   * 将歌词显示到页面上
   * @param {Array} lrcArr 格式化后的歌词信息数组
   */
  function displayLyrics(lrcArr) {
    lyricsUl.innerHTML = lrcArr
      .map((lrc) => `<li data-time="${lrc.time}">${lrc.text}</li>`)
      .join("");
    lrcSize.listHeight = lyricsUl.clientHeight;
    lrcSize.lineHeight = lyricsUl.children[0].clientHeight;
  }

  /**
   * 修改歌词激活样式
   * @param {Number} activeIndex 当前激活行的下标
   */
  function handleChangeLyricsActive(activeIndex) {
    if (activeIndex < 0) return;
    const curActive = lyricsUl.querySelector(".active");
    curActive && curActive.classList.remove("active");
    lyricsUl.children[activeIndex].classList.add("active");
  }

  /**
   * 初始化
   */
  const init = async () => {
    const data = await getLrc();
    lrcArr = data
      .split("\n")
      .filter((l) => l)
      .map((lrc) => formatLrc(lrc));
    displayLyrics(lrcArr);
  };
  await init();

  /**
   * ================================================================================
   */

  /**
   * 根据播放时间激活相应的歌词行
   * @param {Number} curTime 当前音频播放时间
   */
  function changeLyrics(curTime) {
    // 1.找到当前播放时间对应的歌词行下标，高亮
    let idx = lrcArr.findIndex((l) => l.time >= curTime) - 1;
    if (idx === -2 || idx === lrcArr.length - 1) idx = lrcArr.length - 2;
    handleChangeLyricsActive(idx);
    // 2.滚动歌词列表，使高亮行在列表正中间
    let top =
      lrcSize.lineHeight * idx +
      lrcSize.lineHeight / 2 -
      lrcSize.listHeight / 2;
    top = -top;
    if (top > 0) top = 0;
    lyricsUl.style.transform = `translateY(${top}px)`;
  }

  /**
   * 交互
   */
  music.addEventListener("timeupdate", function () {
    changeLyrics(this.currentTime);
  });
})();
