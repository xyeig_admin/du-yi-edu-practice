(async () => {
  const radioList = document.querySelectorAll(".bar-radio");
  const bottomList = document.querySelector(".bottom-list");
  const searchBar = document.querySelector(".bar-search input");
  const searchIcon = document.querySelector(".bar-search img");
  let heros = [],
    filterHeros = [];

  /**
   * 从网络获取当前的英雄数据
   * @returns Promise
   */
  async function getHeroes() {
    return fetch("https://study.duyiedu.com/api/herolist")
      .then((resp) => resp.json())
      .then((resp) => resp.data.reverse());
  }

  /**
   * 将英雄数组显示到页面中
   * @param {Array} heros 英雄数据数组
   */
  function displayHeroes(heros) {
    bottomList.innerHTML = heros
      .map(
        (hero) =>
          `<div class="list-item">
            <a href="https://pvp.qq.com/web201605/herodetail/${hero.ename}.shtml" target="_blank">
              <div class="list-item-img">
                <img src="https://game.gtimg.cn/images/yxzj/img201606/heroimg/${hero.ename}/${hero.ename}.jpg" alt="${hero.ename}" />
              </div>
            </a>
            <span>${hero.cname}</span>
          </div>`
      )
      .join("");
  }

  /**
   * 根据选项的值切换radio选中状态
   * @param {HTMLElement} radio 选项radio
   */
  function handleChecked(radio) {
    const curChecked = document.querySelector(".bar-radio.checked");
    curChecked && curChecked.classList.remove("checked");
    radio.classList.add("checked");
  }

  /**
   * 初始化
   */
  const init = async () => {
    heros = await getHeroes();
    filterHeros = heros;
    displayHeroes(heros);
    handleChecked(radioList[2]);
  };
  await init();

  /**
   * ================================================================================
   */

  /**
   * 点击radio筛选列表数据
   */
  function handleClickRadios() {
    // 1.激活当前radio
    handleChecked(this);
    // 2.更改列表数据
    const type = this.dataset.type;
    const value = this.dataset.value;
    switch (type) {
      case "pay_type":
        filterHeros = heros.filter((hero) => hero.pay_type === +value);
        break;
      case "all":
        filterHeros = heros;
        break;
      case "hero_type":
        filterHeros = heros.filter(
          (hero) => hero.hero_type === +value || hero.hero_type2 === +value
        );
        break;
    }
    displayHeroes(filterHeros);
  }

  /**
   * 输入英雄名筛选数据
   * @param {String} keyWord 搜索的英雄名
   */
  let timer = null;
  function handleSearchKeywords(keyWord) {
    if (timer) clearTimeout(timer);
    timer = setTimeout(() => {
      filterHeros = heros.filter((hero) => hero.cname.includes(keyWord));
      displayHeroes(filterHeros);
      handleChecked(document.querySelector(".bar-radio[data-type='all']"));
    }, 500);
  }

  /**
   * 交互
   */
  radioList.forEach((radio) =>
    radio.addEventListener("click", handleClickRadios)
  );
  searchBar.addEventListener("input", function () {
    handleSearchKeywords(this.value);
  });
  searchIcon.addEventListener("click", function () {
    handleSearchKeywords(searchBar.value);
  });
})();
