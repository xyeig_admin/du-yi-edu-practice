(async () => {
  const selProvince = document.querySelector("#province");
  const selCity = document.querySelector("#city");
  const selArea = document.querySelector("#area");
  let dataArray = [];

  /**
   * 远程获取省市区数据，当获取完成后，得到一个数组
   * @returns Promise
   */
  async function getDatas() {
    return fetch("https://study.duyiedu.com/api/citylist")
      .then((resp) => resp.json())
      .then((resp) => resp.data);
  }

  /**
   * 为下拉框绑定options数据
   * @param {HTMLElement} el 要绑定数据的下拉框
   * @param {Array} datas 要绑定的数据
   * @description 空数组表示当前下拉框禁用
   */
  function displaySelectOptions(el, datas) {
    const hasDatas = datas && datas.length > 0;
    el.querySelector(".selected-text").innerHTML = `请选择${el.dataset.name}`;
    el.classList = `select ${hasDatas ? "" : "disabled"}`;
    el.querySelector(".options").innerHTML = hasDatas
      ? datas
          .map(
            (data) =>
              `<option class="option-item" value="${data.value}">${data.label}</option>`
          )
          .join("")
      : "";
  }

  /**
   * 初始化
   */
  const init = async () => {
    dataArray = await getDatas();
    displaySelectOptions(selProvince, dataArray);
    displaySelectOptions(selCity, []);
    displaySelectOptions(selArea, []);
  };
  await init();

  /**
   * ================================================================================
   */

  /**
   * 为下拉框绑定点击事件
   * @param {HTMLElement} select 当前下拉框
   */
  function handleClickSelects(select) {
    const bar = select.querySelector(".bar");
    const options = select.querySelector(".options");
    // 点击下拉框
    bar.addEventListener("click", (e) => {
      e.stopPropagation();
      // 禁用状态
      if (select.classList.contains("disabled")) return;
      // 下拉列表
      const curActived = document.querySelector(".select.active");
      curActived &&
        curActived !== select &&
        curActived.classList.remove("active");
      select.classList.toggle("active");
    });
    // 点击下拉列表
    options.addEventListener("click", (e) => {
      e.stopPropagation();
      if (e.target.tagName === "OPTION") {
        const curSelected = options.querySelector(".selected");
        curSelected &&
          curSelected !== e.target &&
          curSelected.classList.remove("selected");
        e.target.classList.add("selected");
        bar.querySelector(".selected-text").innerHTML = e.target.innerHTML;
        select.classList.remove("active");
      }
    });
  }

  /**
   * 选择省份
   * @param {Array} datas 省份下拉框的下拉列表数据
   * @param {HTMLElement} option 当前点击的下拉列表项
   */
  function handleSelectProvince(datas, option) {
    const cities = datas.filter((data) => data.value === option.value)[0]
      .children;
    // 渲染城市
    displaySelectOptions(selCity, cities);
    selCity.querySelector(".options").addEventListener("click", (e) => {
      e.stopPropagation();
      if (e.target.tagName === "OPTION") {
        handleSelectCity(cities, e.target);
      }
    });
    // 渲染地区
    displaySelectOptions(selArea, []);
  }

  /**
   * 选择城市
   * @param {Array} datas 省份下拉框的下拉列表数据
   * @param {HTMLElement} option 当前点击的下拉列表项
   */
  function handleSelectCity(datas, option) {
    const areas = datas.filter((data) => data.value === option.value)[0];
    displaySelectOptions(selArea, areas?.children || []);
  }

  /**
   * 交互
   */
  handleClickSelects(selProvince);
  handleClickSelects(selCity);
  handleClickSelects(selArea);
  selProvince.querySelector(".options").addEventListener("click", (e) => {
    e.stopPropagation();
    if (e.target.tagName === "OPTION") {
      handleSelectProvince(dataArray, e.target);
    }
  });
})();
