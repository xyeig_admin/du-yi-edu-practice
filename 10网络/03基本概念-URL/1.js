// 1.写出下面url地址中每部分的值

/**
 * http://www.duyiedu.com
 * 协议：http
 * 域名：www.duyiedu.com
 */

/**
 * http://news.baidu.com/guonei
 * 协议：http
 * 域名：news.baidu.com
 * 路径：/guonei
 */

/**
 * https://baijiahao.baidu.com/s?id=1730140517646479713&wfr=spider&for=pc
 * 协议：https
 * 域名：baijiahao.baidu.com
 * 路径：/s
 * 参数：id=1730140517646479713&wfr=spider&for=pc
 */
