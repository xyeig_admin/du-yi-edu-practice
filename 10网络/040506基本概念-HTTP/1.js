// 1. 使用postman向`http://www.douyutv.com`发送GET请求，回答下面的问题：
/**
 * 状态码：301 Moved Permanently，资源已被永久重定向
 * Location：https://www.douyu.com/，表示请求`http://www.douyutv.com`时会自动重定向到`https://www.douyu.com/`
 */

// 2. 使用postman向`https://www.taobao.com`发送GET请求，回答下面的问题：
/**
 * Content-Type：text/html; charset=utf-8，表示响应体是html文档，浏览器将以页面形式渲染
 */

// 3. 阅读[注册接口文档](http://127.0.0.1:4523/m1/2429576-0-default/api/user/reg)，使用postman完成下面的作业：
/**
 * 请求错误数据
 * Post：https://study.duyiedu.com/api/user/reg
 * Body：
 *  {
      "loginId": "test",
      "nickname": "Testing",
      "loginPwd": "testing"
    }
 * 请求头中的Content-Type：application/json
 * 请求体：
 *  {
      "loginId": "test",
      "nickname": "Testing",
      "loginPwd": "testing"
    }
 * 响应的状态码：200
 * 响应体：
 *  {
      "code": 400,
      "msg": "账号已存在",
      "data": null
    }
 * 响应头中的Content-Type：application/json
 */
/**
 * 请求正确数据
 * Post：https://study.duyiedu.com/api/user/reg
 * Body：
 *  {
      "loginId": "Sutee",
      "nickname": "Sutee",
      "loginPwd": "sutee"
    }
 * 请求头中的Content-Type：application/json
 * 请求体：
 *  {
      "loginId": "Sutee",
      "nickname": "Sutee",
      "loginPwd": "sutee"
    }
 * 响应的状态码：200
 * 响应体：
 *  {
      "code": 0,
      "msg": "",
      "data": {
        "id": "64c36ab3d015907e789fc59e",
        "loginId": "Sutee",
        "nickname": "Sutee"
      }
    }
 * 响应头中的Content-Type：application/json; charset=utf-8
 */
