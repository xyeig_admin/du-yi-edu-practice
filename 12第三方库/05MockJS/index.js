// 获取购物车数据
async function getCart() {
  return await axios.get("/api/cart").then((resp) => resp.data.data);
}

getCart().then((resp) => {
  const init = () => {
    $(".list").html(
      resp
        .map(
          (res) =>
            `<div class="item">
      <div class="check">
        <input type="checkbox" class="checkItem" />
      </div>
      <div class="info">
        <img src="${res.productUrl}" alt="${res.productName}" />
        <a href="">${res.productName}</a>
      </div>
      <div class="price">
        <em>￥${res.unitPrice.toFixed(2)}</em>
      </div>
      <div class="num">
        <a href="" class="decr">-</a>
        <input type="text" value="${res.count}" class="txt" />
        <a href="" class="incr">+</a>
      </div>
      <div class="sum">
        <em>￥${(res.unitPrice * res.count).toFixed(2)}</em>
      </div>
      <div class="del">
        <a href="">删除</a>
      </div>
    </div>`
        )
        .join("")
    );
  };
  init();

  /**
   * 设置商品总价
   */
  const displayTotal = () => {
    const checkItems = $(":checked:not(.checkAll)");
    let total = 0;
    checkItems.each((_, el) => {
      total += +$(el).parents(".item").find(".sum em").text().replace("￥", "");
    });
    $(".footer .right .nums em").text(checkItems.length);
    $(".footer .right .sums em").text("￥" + total.toFixed(2));
  };

  /**
   * 找到所有“全选”的多选框，注册change事件
   */
  $(".checkAll").change(function () {
    $(":checkbox").not(this).prop("checked", this.checked);
    displayTotal();
  });

  /**
   * 找到所有商品的多选框，注册change事件
   */
  $(".checkItem").change(function () {
    $(".checkAll").prop("checked", $(".checkItem:checked").length === 3);
    displayTotal();
  });

  /**
   * 改变商品数量
   */
  const handleChangeCount = (newVal, input) => {
    if (newVal < 1) newVal = 1;
    input.val(newVal);
    const price =
      +$(input).parents(".item").find(".price em").text().replace("￥", "") *
      newVal;
    $(input)
      .parents(".item")
      .find(".sum em")
      .text("￥" + price.toFixed(2));
    displayTotal();
  };

  /**
   * 增加商品数量
   */
  $(".item .num .incr").click(function (e) {
    e.preventDefault();
    const input = $(this).prev("input");
    handleChangeCount(+input.val() + 1, input);
  });

  /**
   * 减少商品数量
   */
  $(".item .num .decr").click(function (e) {
    e.preventDefault();
    const input = $(this).next("input");
    handleChangeCount(+input.val() - 1, input);
  });

  /**
   * 删除商品
   */
  $(".del a").click(function (e) {
    e.preventDefault();
    $(this).parents(".item").remove();
    $(".checkAll").prop(
      "checked",
      $(":checked:not(.checkAll)").length === $(".item").length
    );
    displayTotal();
  });

  /**
   * 删除选中商品
   */
  $(".delChecked").click(function (e) {
    e.preventDefault();
    $(":checked:not(.checkAll)").parents(".item").remove();
    $(".checkAll").prop("checked", false);
    displayTotal();
  });

  /**
   * 清空购物车
   */
  $(".clearAll").click(function (e) {
    e.preventDefault();
    $(".item").remove();
    $(".checkAll").prop("checked", false);
    displayTotal();
  });
});
