// https://github.com/nuysoft/Mock/wiki/Mock.setup()
Mock.setup({
  timeout: "200-500",
});

// https://github.com/nuysoft/Mock/wiki/Mock.mock()
Mock.mock("/api/cart", "get", {
  code: 0,
  msg: "",
  "data|5-10": [
    {
      // https://github.com/nuysoft/Mock/wiki/Text#randomword-min-max-
      productName: "@cword(1, 5)", // 商品名称
      // https://github.com/nuysoft/Mock/wiki/Image
      productUrl: "@image(100x100, @hex, @hex, @productName)", // 商品图片url地址
      "unitPrice|1-1000.2": 0, // 商品单价
      "count|1-3": 0, // 购物数量
    },
  ],
});
