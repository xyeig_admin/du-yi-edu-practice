(() => {
  /**
   * 设置各地区时间
   */
  const displayGlobalTime = () => {
    const now = moment();
    const zones = $("[data-zone]");
    $(zones).each((_, zone) => {
      const val = +$(zone).attr("data-zone");
      $(zone).html(now.utcOffset(val).format("YYYY-MM-DD HH:mm:ss"));
    });
  };

  /**
   * 初始化
   */
  const init = () => {
    displayGlobalTime();
  };
  init();

  /**
   * 渲染用户生日信息
   */
  const container = $("#birthInfo");
  const TODAY = moment().startOf("day");
  const displayUserBirth = () => {
    const birthday = moment($("#birthInput").val());
    if (!birthday.isValid() || birthday > TODAY) {
      container.html("");
      return;
    }
    let infoHtml = "";

    // 出生日期
    infoHtml += `<p>
      <strong>出生日期：</strong>
      <span>${birthday.format("YYYY-MM-DD")}</span>
    </p>`;

    // 年龄
    const age = TODAY.diff(birthday, "years");
    infoHtml += `<p>
      <strong>年龄：</strong>
      <span>${age}</span>
    </p>`;

    // 秒数
    const seconds = moment().diff(birthday, "seconds");
    infoHtml += `<p>
      你在这个世界上已存在了
      <strong>${seconds}</strong>
      秒钟
    </p>`;

    // 下一次生日
    let nextBirth;
    const curYearBirth = moment(birthday).year(TODAY.year());
    if (curYearBirth > TODAY) {
      nextBirth = curYearBirth;
    } else {
      nextBirth = moment(birthday).year(TODAY.year() + 1);
    }
    infoHtml += `<p>
      你还有
      <strong>${nextBirth.diff(TODAY, "days")}</strong>
      天就会迎来你
      <span>${age + 1}</span>
      岁的生日
    </p>`;

    // 上一次生日距今
    const CALENDAR = curYearBirth.calendar({
      sameDay: "今天",
      nextDay: "明天",
      nextWeek: "dddd",
      lastDay: "昨天",
      lastWeek: "上一个 dddd",
      sameElse: "YYYY-MM-DD",
    });
    infoHtml +=
      curYearBirth > TODAY
        ? `<p>
        你将在
        <strong>${CALENDAR}</strong>
        迎来你下个生日
      </p>`
        : `<p>
        你已在
        <strong>${CALENDAR}</strong>
        过了生日
      </p>`;

    $(container).html(infoHtml);
  };

  /**
   * 交互
   */
  // 各时区时间跳动
  setInterval(() => {
    displayGlobalTime();
    displayUserBirth();
  }, 1000);
  // 日期选择器失焦时显示信息
  $("#birthInput").change(function () {
    displayUserBirth();
  });
})();
