(async () => {
  /**
   * 根据下标获取DOM元素
   */
  const getItemDom = (idx) => {
    return document.querySelectorAll(".item")[idx];
  };

  /**
   * 加载图表1：折线图
   */
  const loadChart1 = () => {
    const chart = echarts.init(getItemDom(0));
    chart.setOption({
      title: {
        text: "ECharts 折线图",
      },
      tooltip: {},
      legend: {
        data: ["手机销量", "平板销量"],
      },
      xAxis: {
        data: Array(12)
          .fill(0)
          .map((month) => `${month + 1}月`),
      },
      yAxis: {}, // 纵坐标让其自动生成，不配置会报错
      series: [
        {
          name: "手机销量",
          type: "line",
          data: Array(12)
            .fill(0)
            .map(() => Math.floor(Math.random() * 1000 + 800)),
        },
        {
          name: "平板销量",
          type: "line",
          data: Array(12)
            .fill(0)
            .map(() => Math.floor(Math.random() * 1000 + 800)),
          smooth: true,
        },
      ],
    });
  };

  /**
   * 加载图表2：柱状图
   */
  const loadChart2 = () => {
    const chart = echarts.init(getItemDom(1));
    chart.setOption({
      title: {
        text: "ECharts 柱状图",
      },
      tooltip: {},
      legend: {
        data: ["手机销量", "平板销量"],
      },
      xAxis: {
        data: Array(12)
          .fill(0)
          .map((_, month) => `${month + 1}月`),
      },
      yAxis: {}, // 纵坐标让其自动生成，不配置会报错
      series: [
        {
          name: "手机销量",
          type: "bar",
          data: Array(12)
            .fill(0)
            .map(() => Math.floor(Math.random() * 1000 + 800)),
        },
        {
          name: "平板销量",
          type: "bar",
          data: Array(12)
            .fill(0)
            .map(() => Math.floor(Math.random() * 1000 + 800)),
        },
      ],
    });
  };

  /**
   * 加载图表3：饼图
   */
  const loadChart3 = () => {
    const chart = echarts.init(getItemDom(2));
    chart.setOption({
      title: {
        text: "ECharts 饼图",
      },
      tooltip: {},
      series: [
        {
          name: "访问来源",
          type: "pie",
          radius: "50%",
          roseType: "area",
          data: [
            { value: 235, name: "视频广告" },
            { value: 274, name: "联盟广告" },
            { value: 310, name: "邮件营销" },
            { value: 335, name: "直接访问" },
            { value: 400, name: "搜索引擎" },
          ],
        },
      ],
    });
  };

  /**
   * 加载图表4：K线图（蜡烛图）
   */
  const loadChart4 = () => {
    const chart = echarts.init(getItemDom(3));
    chart.setOption({
      title: {
        text: "ECharts K线图（蜡烛图）",
      },
      tooltip: {},
      xAxis: {
        data: Array(12)
          .fill(0)
          .map((_, month) => `${month + 1}月`),
      },
      yAxis: {}, // 纵坐标让其自动生成，不配置会报错
      series: [
        {
          type: "k",
          data: [
            [20, 34, 38, 10],
            [34, 35, 50, 30],
            [35, 38, 44, 33],
            [38, 33, 40, 30],
            [33, 27, 32, 22],
            [27, 26, 29, 22],
            [26, 27, 28, 25],
            [27, 33, 34, 32],
            [33, 37, 44, 32],
            [37, 30, 39, 28],
            [30, 26, 33, 22],
            [26, 18, 28, 13],
          ],
          encode: {
            tooltip: [1, 2, 3, 4],
          },
          dimensions: [null, "开盘价", "收盘价", "最高价", "最低价"],
        },
      ],
    });
  };

  /**
   * 加载图表5：远程加载数据
   */
  const loadChart5 = async () => {
    // 模拟远程数据
    Mock.mock("/api/pie-datas", "get", {
      datas: [
        { value: 235, name: "视频广告" },
        { value: 274, name: "联盟广告" },
        { value: 310, name: "邮件营销" },
        { value: 335, name: "直接访问" },
        { value: 400, name: "搜索引擎" },
      ],
    });
    Mock.setup({
      timeout: "2000-5000",
    });

    const chart = echarts.init(getItemDom(4));
    chart.setOption({
      title: {
        text: "ECharts 饼图，远程数据加载中...",
      },
    });
    chart.showLoading();
    const resp = await axios.get("/api/pie-datas");
    chart.setOption({
      title: {
        text: "ECharts 饼图",
      },
      tooltip: {},
      series: [
        {
          name: "访问来源",
          type: "pie",
          radius: "50%",
          roseType: "area",
          data: resp.data.datas,
        },
      ],
    });
    chart.hideLoading();
  };

  /**
   * 加载图表6：远程加载地图数据
   */
  const loadChart6 = async () => {
    const chart = echarts.init(getItemDom(5));
    chart.showLoading();
    const resp = await axios.get("./china.geojson.json");
    const users = await axios.get("./user.json");
    // 注册地图数据
    echarts.registerMap("China", resp.data);
    chart.setOption({
      title: {
        text: "ECharts 用户地图",
      },
      tooltip: { formatter: "{b} 注册用户 {c}人" },
      visualMap: {
        left: "right",
        min: 0,
        max: 10000,
        text: ["高", "低"],
        calculable: true,
      },
      series: [
        {
          type: "map",
          map: "China",
          roam: true,
          scaleLimit: {
            min: 0.7,
            max: 3,
          },
          data: users.data,
        },
      ],
    });
    chart.hideLoading();
  };

  /**
   * 初始化
   */
  const init = async () => {
    loadChart1();
    loadChart2();
    loadChart3();
    loadChart4();
    await loadChart5();
    await loadChart6();
  };
  await init();
})();
