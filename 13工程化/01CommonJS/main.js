/**
 * 4. 主模块 main.js
 * 这是启动模块，它会利用其它模块，逐字打印出所有的文本，打印每个字的间隔时间在 `config.js` 中已有配置
 */

const config = require("./config");
const print = require("./print");
const delay = require("./delay");
/**
 * 运行该函数，会逐字打印config.js中的文本
 * 每个字之间的间隔在config.js已有配置
 */
async function run() {
  for (let i = 0; i < config.text.length; i++) {
    print(i);
    await delay(config.wordDuration);
    config.text.slice(0, i);
  }
}

run();
