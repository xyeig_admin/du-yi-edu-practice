// 提供一些数学相关的函数
const isOdd = (n) => n % 2 !== 0;

const sum = (a, b) => a + b;

module.exports = {
  isOdd,
  sum,
};
