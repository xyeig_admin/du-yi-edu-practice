/**
 * 3. 打印模块 print.js
 * 该模块负责导出一个打印函数，该函数需要获取当前的打印配置
 */

const config = require("./config");
/**
 * 该函数会做以下两件事：
 * 1. console.clear() 清空控制台
 * 2. 读取config.js中的text配置，打印开始位置到index位置的字符
 * @param {number} index
 */
function print(index) {
  console.clear();
  console.log(config.text.substring(0, index));
}

module.exports = print;
