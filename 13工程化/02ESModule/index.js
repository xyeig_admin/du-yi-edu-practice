// 启动文件 通过node命令运行的文件

// const math = require("./math");
// console.log(math);
// console.log(math.sum(1, 3));

// import { sum, isOdd } from "./math.js";
// console.log(sum(1, 2), isOdd(5));

// import math from "./math.js";
// console.log(math.sum(1, 2), math.isOdd(5));

setTimeout(() => {
  import("./math.js").then((m) => {
    const math = m.default;
    console.log(math.sum(1, 2), math.isOdd(5));
  });
}, 1000);
