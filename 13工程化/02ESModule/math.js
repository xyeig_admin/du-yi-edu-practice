// 提供一些数学相关的函数

// const isOdd = (n) => n % 2 !== 0;
// const sum = (a, b) => a + b;
// module.exports = {
//   isOdd,
//   sum,
// };

// export const sum = (a, b) => a + b;
// export const isOdd = (n) => n % 2 !== 0;

// const sum = (a, b) => a + b;
// const isOdd = (n) => n % 2 !== 0;
// export { sum, isOdd };

// const he = (a, b) => a + b;
// const jiShu = (n) => n % 2 !== 0;
// export { he as sum, jiShu as isOdd };

export default {
  sum: (a, b) => a + b,
  isOdd: (n) => n % 2 !== 0,
};
