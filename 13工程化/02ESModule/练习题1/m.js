/**
 * 书写一个 ESM 模块，查阅文档，导出下面的模块对象
 * 你可以写出多少种导出的方式
  {
    a: 1,
    sum: fn,
    isOdd: fn
  }
 */

// export const a = 1;

// const a = 1;
// export { a };

const temp = 1;
export { temp as a };
