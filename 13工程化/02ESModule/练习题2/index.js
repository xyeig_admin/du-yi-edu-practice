/**
 * 书写一个 ESM 模块，查阅文档，按照下面的要求分别写出导入代码：
 * 1. 仅导入 default
 * 2. 仅导入 a 和 b
 * 3. 同时导入 default、a、b
 * 4. 导入整个模块对象
 * 5. 不导入任何东西，仅运行一次该模块
 */

// import m from "./m.js";

// import { a, b } from "./m.js";

// import m, { a, b } from "./m.js";

// import * as m from "./m.js";

import "./m.js";
