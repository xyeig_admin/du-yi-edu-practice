/**
 * 书写一个 ESM 模块，查阅文档，导出下面的模块对象
  {
    a: 1,
    b: 2,
    c: function() {},
    default: {
      a: 1,
      b: 2
    }
  }
 */

export const a = 1;
export const b = 2;
export const c = () => {};
export default {
  a,
  b,
};
