// 负责和用户相关的远程请求
const BASE_URL = "https://study.duyiedu.com";

// 具名导出 登录方法
export const login = async (loginId, loginPwd) => {
  const res = await fetch(`${BASE_URL}/api/user/login`, {
    method: "POST",
    headers: {
      "content-type": "application/json",
    },
    body: JSON.stringify({
      loginId,
      loginPwd,
    }),
  }).then((resp) => resp.json());
  return res.data;
};
