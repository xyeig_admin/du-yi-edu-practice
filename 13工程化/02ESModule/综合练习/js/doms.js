// 找出所有可能用到的DOM元素
export default {
  formContainer: document.querySelector("#formContainer"),
  userName: document.querySelector("#userName"),
  userPassword: document.querySelector("#userPassword"),
  btnSubmit: document.querySelector("#btnSubmit"),
};
