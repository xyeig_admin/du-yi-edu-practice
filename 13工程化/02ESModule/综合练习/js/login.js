// 导出一个函数，调用时会自动获取文本框的值完成登录

import doms from "./doms.js";
import { login } from "./api/user.js";

let isLogin = false;

export const submit = async () => {
  if (isLogin) return;
  const loginId = doms.userName.value;
  const loginPwd = doms.userPassword.value;
  if (!loginId || !loginPwd) {
    alert("请填写账号密码");
    return;
  }
  isLogin = true;
  doms.btnSubmit.innerText = "登录中...";
  const res = await login(loginId, loginPwd);
  alert(
    res ? `登录成功，欢迎您 ${res.nickname}` : "登录失败，请检查账号或密码"
  );
  isLogin = false;
  doms.btnSubmit.innerText = "登录";
};
