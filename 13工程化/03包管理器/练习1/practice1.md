# 练习 1：全局安装练习

1. 全局安装 `moeda`
2. 运行命令 `moeda 1 cny`，查看当前人民币汇率

---

# 结果

- 无法运行命令
- moeda 库请求失败

```bash
GBP Converting currency...
/Users/ikuko/.nvm/versions/node/v20.5.0/lib/node_modules/moeda/node_modules/got/index.js:73
                        ee.emit('error', new got.RequestError(err, opts));
                                         ^
ErrorClass [RequestError]: Client network socket disconnected before secure TLS connection was established
    at ClientRequest.<anonymous> (/Users/ikuko/.nvm/versions/node/v20.5.0/lib/node_modules/moeda/node_modules/got/index.js:73:21)
    at Object.onceWrapper (node:events:629:26)
    at ClientRequest.emit (node:events:514:28)
    at TLSSocket.socketErrorListener (node:_http_client:495:9)
    at TLSSocket.emit (node:events:514:28)
    at emitErrorNT (node:internal/streams/destroy:151:8)
    at emitErrorCloseNT (node:internal/streams/destroy:116:3)
    at process.processTicksAndRejections (node:internal/process/task_queues:82:21) {
  code: 'ECONNRESET',
  host: 'api.ratesapi.io',
  hostname: 'api.ratesapi.io',
  method: 'GET',
  path: '/api/latest?base=CNY&symbols=USD,EUR,GBP,BRL'
}

Node.js v20.5.0
```
