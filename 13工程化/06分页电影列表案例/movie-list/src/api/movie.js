import axios from "axios";

export const getMovies = async (page, size) => {
  const res = await axios.get("/api/movies", {
    params: {
      page,
      size,
    },
  });
  return res.data;
};
