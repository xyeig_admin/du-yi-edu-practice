import { createMovieTags } from "./list";
import { getMovies } from "@/api/movie";
import { createPagers } from "./pager";

const init = async () => {
  const res = await getMovies(1, 30);
  createMovieTags(res.data.movieList);
  createPagers(1, 30, res.data.movieTotal);
};
init();
