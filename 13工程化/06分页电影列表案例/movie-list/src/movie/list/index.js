import $ from "jquery";
import styles from "./index.module.less";

let container = null;

/**
 * 初始化函数，负责创建容器
 */
const init = () => {
  container = $("<div>").addClass(styles.container).appendTo("#app");
};

init();

/**
 * 根据传入的电影数组，创建元素，填充到容器中
 * @params movies 电影数组
 */
export const createMovieTags = (movies) => {
  container[0].innerHTML = movies
    .map(
      (m) => `<div class="${styles.items}">
    <a href="${m.url}" target="_blank">
      <img class="${styles.cover}" src="${m.cover}" alt="${m.title}" />
    </a>
    <a href="${m.url}" target="_blank">
      <span class="${styles.title}">${m.title}</span>
    </a>
    <span class="${styles.rate}">评分 ${m.rate}</span>
  </div>`
    )
    .join("");
};
