import request from "./request";

export const getAbout = async () => {
  return await request.get("/api/about");
};
