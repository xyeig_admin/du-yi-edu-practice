import request from "./request";

/**
 * 获取博客分类
 */
export const getBlogTypes = async () => {
  return await request.get("/api/blogtype");
};

/**
 * 获取博客列表
 * @param {Number} page 当前页码
 * @param {Number} limit 页容量
 * @param {Number} categoryid 所属分类，-1表示全部
 * @param {String} keyword 模糊查询的关键字
 */
export const getBlogs = async ({
  page = 1,
  limit = 10,
  categoryid = -1,
  keyword = "",
}) => {
  return await request.get("/api/blog", {
    params: {
      page,
      limit,
      categoryid,
      keyword,
    },
  });
};

/**
 * 获取单个博客
 * @param {String} blogId 博客id
 */
export const getBlogDetail = async (blogId) => {
  return await request.get(`/api/blog/${blogId}`);
};

/**
 * 提交评论
 * @param {Object} commentInfo 评论信息
 */
export const postComment = async (commentInfo) => {
  return await request.post("/api/comment", commentInfo);
};

/**
 * 获取指定页码的评论列表
 * @param {String} blogId 博客id，-1表示不限文章
 * @param {Number} page 当前页码
 * @param {Number} limit 页容量
 * @param {String} keyword 模糊查询的关键字
 */
export const getComments = async (
  blogId,
  page = 1,
  limit = 10,
  keyword = ""
) => {
  return await request.get("/api/comment", {
    params: {
      blogId,
      page,
      limit,
      keyword,
    },
  });
};
