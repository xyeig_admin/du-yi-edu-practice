import request from "./request";

export const getProjects = async () => {
  return await request.get("/api/project");
};
