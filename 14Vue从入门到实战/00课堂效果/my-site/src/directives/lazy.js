import eventBus from "@/eventBus";
import { debounce } from "@/utils";
import defaultGif from "@/assets/default.gif";

// 当前指令绑定的组件所有的图片
let images = [];

/**
 * 遍历images，处理每一张图片
 * @param {Object} img 每一张图片的信息对象
 */
const displayImage = (img) => {
  img.dom.src = defaultGif;
  // 视口高度
  const clientHeight = document.documentElement.clientHeight;
  // 图片高度（未加载时不知道实际高度，默认定为150）
  const rect = img.dom.getBoundingClientRect();
  const height = rect.height || 150;
  if (rect.top >= -height && rect.top <= clientHeight) {
    const temp = new Image();
    temp.onload = () => {
      img.dom.src = img.src;
    };
    temp.src = img.src;
    images = images.filter((image) => image !== img);
  }
};

/**
 * 事件总线主区域滚动事件
 */
const handleScroll = () => {
  images.forEach((img) => {
    displayImage(img);
  });
};
eventBus.$on("mainScroll", debounce(handleScroll, 50));

export default {
  inserted(el, bindings) {
    const img = {
      dom: el,
      src: bindings.value,
    };
    images.push(img);
    // 指令每绑定一张图片就处理
    displayImage(img);
  },
  unbind(el) {
    images = images.filter((img) => img.dom !== el);
  },
};
