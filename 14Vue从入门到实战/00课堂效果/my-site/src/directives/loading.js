import loadingSrc from "@/assets/loading.svg";
import styles from "./loading.module.less";

/**
 * 得到el中的loadingDOM元素
 * @param {HTMLElement} el 容器DOM元素
 * @returns el中的loadingDOM元素，不存在则返回null
 */
const getLoading = (el) => {
  return el.querySelector("img[data-role='loading']");
};

/**
 * 创建loadingDOM元素，绑定自定义属性data-role为loading
 * @returns loadingDOM元素
 */
const createLoading = () => {
  const img = document.createElement("img");
  img.dataset.role = "loading";
  img.src = loadingSrc;
  img.className = styles.loading;
  return img;
};

// 导出指令配置对象
export default (el, binding) => {
  // 根据binding.value的值，决定创建或删除img元素
  const img = getLoading(el);
  if (binding.value) {
    if (img) return;
    const newImg = createLoading();
    el.appendChild(newImg);
  } else {
    img && img.remove();
  }
};
