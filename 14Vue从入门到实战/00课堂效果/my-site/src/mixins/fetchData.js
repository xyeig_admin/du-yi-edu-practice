// 公共的远程获取数据代码

export default (defaultValue = null) => {
  return {
    data() {
      return {
        isLoading: true,
        data: defaultValue,
      };
    },
    async created() {
      this.data = await this.fetchData();
      this.isLoading = false;
    },
  };
};
