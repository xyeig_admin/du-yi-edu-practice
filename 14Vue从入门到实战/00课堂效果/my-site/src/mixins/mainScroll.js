// 页面可滚动时返回顶部

export default (refEl) => {
  return {
    data() {
      return {
        scrollTop: 0,
      };
    },
    mounted() {
      // 监听页面滚动
      this.$refs[refEl].addEventListener("scroll", this.handleMainScroll);
      // 页面主区域超出视口时，注册修改滚动高度事件
      this.$bus.$on("setMainScroll", this.handleSetMainScroll);
    },
    methods: {
      handleMainScroll() {
        // 页面滚动时触发滚动事件，用于激活右侧目录标题
        this.$bus.$emit("mainScroll", this.$refs[refEl]);
      },
      handleSetMainScroll(top) {
        // 修改当前页面滚动高度
        this.$refs[refEl].scrollTop = top;
      },
    },
    beforeDestroy() {
      // 传递undefined给滚动事件，表示当前el不存在
      this.$bus.$emit("mainScroll");
      // 移除当前页面对滚动事件的监听
      this.$refs[refEl].removeEventListener("scroll", this.handleMainScroll);
      // 取消监听修改滚动高度事件
      this.$bus.$off("setMainScroll", this.handleSetMainScroll);
    },
  };
};
