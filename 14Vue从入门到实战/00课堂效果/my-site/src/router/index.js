import Vue from "vue";
import VueRouter from "vue-router";

if (!window.VueRouter) {
  Vue.use(VueRouter);
}

import routes from "./routes";
import { displaySiteTitle } from "@/utils";

const router = new VueRouter({
  routes,
  mode: "history",
});

router.afterEach((to, from) => {
  if (to.meta.title) {
    displaySiteTitle.setRouteTitle(to.meta.title);
  }
});

export default router;
