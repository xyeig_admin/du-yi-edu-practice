// 路由匹配规则

// import Home from "@/views/Home";
// import Blog from "@/views/Blog";
// import Detail from "@/views/Blog/Detail";
// import About from "@/views/About";
// import Project from "@/views/Project";
// import Message from "@/views/Message";
import NotFound from "@/views/NotFound";

import "nprogress/nprogress.css";
import { start, done, configure } from "nprogress";

configure({
  trickleSpeed: 20,
  showSpinner: false,
});

const delay = (duration) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, duration);
  });
};

const loadComponentProgress = (asyncComponent) => {
  return async () => {
    start();
    if (process.env.NODE_ENV === "development") {
      await delay(2000);
    }
    const comp = await asyncComponent();
    done();
    return comp;
  };
};

export default [
  {
    name: "Home",
    path: "/",
    // component: Home,
    component: loadComponentProgress(() => import("@/views/Home")),
    meta: {
      title: "首页",
    },
  },
  {
    name: "Blog",
    path: "/blog",
    // component: Blog,
    component: loadComponentProgress(() => import("@/views/Blog")),
    meta: {
      title: "文章",
    },
  },
  {
    name: "BlogCategory",
    path: "/blog/cate/:categoryId",
    // component: Blog,
    component: loadComponentProgress(() => import("@/views/Blog")),
    meta: {
      title: "文章分类",
    },
  },
  {
    name: "Detail",
    path: "/blog/:blogId",
    // component: Detail,
    component: loadComponentProgress(() => import("@/views/Blog/Detail")),
    meta: {
      title: "文章详情",
    },
  },
  {
    name: "About",
    path: "/about",
    // component: About,
    component: loadComponentProgress(() => import("@/views/About")),
    meta: {
      title: "关于我",
    },
  },
  {
    name: "Project",
    path: "/project",
    // component: Project,
    component: loadComponentProgress(() => import("@/views/Project")),
    meta: {
      title: "项目&效果",
    },
  },
  {
    name: "Message",
    path: "/message",
    // component: Message,
    component: loadComponentProgress(() => import("@/views/Message")),
    meta: {
      title: "留言板",
    },
  },
  {
    name: "NotFound",
    path: "*",
    component: NotFound,
    meta: {
      title: "404",
    },
  },
];
