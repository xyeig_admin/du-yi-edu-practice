// 格式化时间戳

const fixZero = (num) => num.toString().padStart(2, "0");

export default (timestamp) => {
  const date = new Date(+timestamp);
  return `${date.getFullYear()}-${fixZero(date.getMonth() + 1)}-${fixZero(
    date.getDate()
  )} ${fixZero(date.getHours())}:${fixZero(date.getMinutes())}:${fixZero(
    date.getSeconds()
  )}`;
};
