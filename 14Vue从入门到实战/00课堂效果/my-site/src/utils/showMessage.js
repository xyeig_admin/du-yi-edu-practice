/**
 * 弹出消息
 * @param {String} content 消息内容
 * @param {String} type 消息类型 info、error、success、warn
 * @param {Number} duration 多久后消失
 * @param {HTMLElement} container 容器，消息会显示到该容器的正中间；如果不传则显示到页面正中间
 * @param {Function} cb 回调函数，消息消失后执行
 */
import getComponentRootDom from "./getComponentRootDom";
import Icon from "@/components/Icon";
import styles from "./showMessage.module.less";

export default (options = {}) => {
  const content = options.content || "";
  const type = options.type || "info";
  const duration = options.duration || 2000;
  const container = options.container || document.body;
  const cb = options.cb || undefined;

  // 创建消息元素
  const div = document.createElement("div");
  const icon = getComponentRootDom(Icon, { type: type });
  div.innerHTML = `<span class="${styles.icon}">
    ${icon.outerHTML}
  </span>
  <span>
    ${content}
  </span>`;
  div.className = `${styles.message} ${styles["message-" + type]}`;

  // body不能设置为relative
  if (options.container) {
    // 容器的position是否static
    if (getComputedStyle(container).position === "static") {
      container.style.position = "relative";
    }
  }

  // 将div加入容器中
  container.appendChild(div);

  // 浏览器强行渲染：否则直接修改style，没有过渡样式
  div.clientHeight; // 导致reflow

  // 回归到正常位置
  div.style.opacity = 1;
  div.style.transform = `translate(-50%, -50%)`;

  // 等一段时间再消失
  setTimeout(() => {
    div.style.opacity = 0;
    div.style.transform = `translate(-50%, -50%) translateY(-20px)`;
    div.addEventListener(
      "transitionend",
      () => {
        div.remove();
        cb && cb();
      },
      {
        once: true,
      }
    );
  }, duration);
};
