import VueRouter from "vue-router";
import routes from "./routes";
import Vue from "vue";
import store from "../store";

Vue.use(VueRouter);

const router = new VueRouter({
  routes,
  mode: "history",
});

router.beforeEach((to, from, next) => {
  if (to.meta.auth) {
    const status = store.getters["loginUser/status"];
    if (status === "loading") {
      next({
        path: "/loading",
        query: {
          returnUrl: to.fullPath,
        },
      });
    } else if (status === "logged") {
      next();
    } else {
      alert("您还未登录，暂时无权访问该页面，正在为您跳转登录");
      next({
        path: "/login",
        query: {
          returnUrl: to.fullPath,
        },
      });
    }
  } else {
    next();
  }
});

export default router;
