import Vuex from "vuex";
import Vue from "vue";
import loginUser from "./loginUser";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    loginUser,
  },
  strict: true,
});

window.store = store;
export default store;
