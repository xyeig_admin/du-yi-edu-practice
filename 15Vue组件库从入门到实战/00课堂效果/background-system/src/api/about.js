import request from "@/utils/request";

export const getAbout = () => {
  return request({
    url: "/api/about",
    method: "get",
  });
};

export const editAbout = (data) => {
  return request({
    url: "/api/about",
    method: "post",
    data,
  });
};
