import request from "@/utils/request";

export const getBanners = () => {
  return request({
    url: "/api/banner",
    method: "get",
  });
};

export const editBanner = (data) => {
  return request({
    url: "/api/banner",
    method: "post",
    data,
  });
};
