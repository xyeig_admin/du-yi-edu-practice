import request from "@/utils/request";

export const getBlogs = (page = 1, limit = 10) => {
  return request({
    url: "/api/blog",
    method: "get",
    params: {
      page,
      limit,
    },
  });
};

export const deleteBlog = (id) => {
  return request({
    url: `/api/blog/${id}`,
    method: "delete",
  });
};

export const createBlog = (data) => {
  return request({
    url: "/api/blog",
    method: "post",
    data,
  });
};

export const editBlog = (blogInfo) => {
  return request({
    url: `/api/blog/${blogInfo.id}`,
    method: "put",
    data: blogInfo,
  });
};

export const getBlog = (id) => {
  return request({
    url: `/api/blog/${id}`,
    method: "get",
  });
};

export const getBlogTypes = () => {
  return request({
    url: "/api/blogtype",
    method: "get",
  });
};

export const createBlogType = (data) => {
  return request({
    url: "/api/blogtype",
    method: "post",
    data,
  });
};

export const deleteBlogType = (id) => {
  return request({
    url: `/api/blogtype/${id}`,
    method: "delete",
  });
};

export const getBlogType = (id) => {
  return request({
    url: `/api/blogtype/${id}`,
    method: "get",
  });
};

export const updateBlogType = (blogInfo) => {
  return request({
    url: `/api/blogtype/${blogInfo.id}`,
    method: "put",
    data: blogInfo.data,
  });
};
