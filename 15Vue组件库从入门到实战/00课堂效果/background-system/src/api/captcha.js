import request from "@/utils/request";

export const getCaptcha = () => {
  return request({
    url: "/res/captcha",
    method: "get",
  });
};
