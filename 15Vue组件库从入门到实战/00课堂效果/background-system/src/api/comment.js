import request from "@/utils/request";

export const getComments = (page = 1, limit = 10) => {
  return request({
    url: "/api/comment",
    method: "get",
    params: {
      page,
      limit,
    },
  });
};

export const deleteComment = (id) => {
  return request({
    url: `/api/comment/${id}`,
    method: "delete",
  });
};
