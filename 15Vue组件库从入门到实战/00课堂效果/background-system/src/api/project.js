import request from "@/utils/request";

export const createProject = (data) => {
  return request({
    url: "/api/project",
    method: "post",
    data,
  });
};

export const getProjects = () => {
  return request({
    url: "/api/project",
    method: "get",
  });
};

export const editProject = (id, data) => {
  return request({
    url: `/api/project/${id}`,
    method: "put",
    data,
  });
};

export const deleteProject = (id) => {
  return request({
    url: `/api/project/${id}`,
    method: "delete",
  });
};
