import request from "@/utils/request";

export const getSetting = () => {
  return request({
    url: "/api/setting",
    method: "get",
  });
};

export const editSetting = (data) => {
  return request({
    url: "/api/setting",
    method: "put",
    data,
  });
};
