import request from "@/utils/request";

export const login = (data) => {
  return request({
    url: "/api/admin/login",
    method: "post",
    data,
  });
};

export const getInfo = (token) => {
  return request({
    url: "/api/admin/whoami",
    method: "get",
    params: { token },
  });
};

export const editUser = (data) => {
  return request({
    url: "/api/admin",
    method: "put",
    data,
  });
};
