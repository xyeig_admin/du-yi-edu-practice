import request from "./request";

export const getBanners = async () => {
  return await request.get("/api/banner");
};
