import request from "./request";

export const getMessages = async (page = 1, limit = 10) => {
  return await request.get("/api/message", {
    params: {
      page,
      limit,
    },
  });
};

export const postMessage = async (msgInfo) => {
  return await request.post("/api/message", msgInfo);
};
