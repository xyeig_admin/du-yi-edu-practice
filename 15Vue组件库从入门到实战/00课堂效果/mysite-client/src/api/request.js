import axios from "axios";
import { showMessage } from "../utils";

const ins = axios.create(); // 创建axios实例
ins.interceptors.response.use((res) => {
  if (res.data.code !== 0) {
    showMessage({
      content: res.data.msg,
      type: "error",
    });
    return null;
  }
  return res.data.data;
});

export default ins;
