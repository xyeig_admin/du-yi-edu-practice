import request from "./request";

export const getSetting = async () => {
  return await request.get("/api/setting");
};
