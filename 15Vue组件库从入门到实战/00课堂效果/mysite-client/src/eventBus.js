// // 事件总线

// /**
//  * {
//  *  "event1": [handler1, handler2],
//  *  "event2": [handler1, handler2],
//  * }
//  */
// const listeners = {};

// export default {
//   // 监听某一个事件
//   $on(eventName, handler) {
//     if (!listeners[eventName]) {
//       listeners[eventName] = new Set();
//     }
//     listeners[eventName].add(handler);
//   },
//   // 取消监听某一个事件
//   $off(eventName, handler) {
//     if (!listeners[eventName]) return;
//     listeners[eventName].delete(handler);
//   },
//   // 触发事件
//   $emit(eventName, ...args) {
//     if (!listeners[eventName]) return;
//     for (const handler of listeners[eventName]) {
//       handler(...args);
//     }
//   },
// };

import Vue from "vue";

// 方式一
// export default new Vue({});

// 方式二
const app = new Vue({});
Vue.prototype.$bus = app;

// 其他js模块调用
export default app;
