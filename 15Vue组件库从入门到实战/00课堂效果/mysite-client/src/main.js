import Vue from "vue";
import App from "./App.vue";
import "./styles/global.less";

import router from "./router";

import { showMessage } from "./utils";
Vue.prototype.$showMessage = showMessage;

// import "./mock";

// 注册全局指令
import vLoading from "./directives/loading";
Vue.directive("loading", vLoading);
import vLazy from "./directives/lazy";
Vue.directive("lazy", vLazy);

// 事件总线
/**
 * 事件名： mainScroll
 * 主区域滚动条位置变化后触发
 * 参数：滚动的DOM元素，如果为undefined则表示DOM元素不存在
 *
 * 事件名： setMainScroll
 * 当需要设置主区域滚动条位置时触发
 * 参数：滚动高度
 */
import "./eventBus";

import store from "./store";
store.dispatch("setting/fetchSetting");

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
