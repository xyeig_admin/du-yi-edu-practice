import Mock from "mockjs";

Mock.mock("/api/setting", "get", {
  code: 0,
  msg: "",
  data: {
    // avatar: "http://www.duyiedu.com/source/img/logo.png",
    // siteTitle: "我的个人空间",
    // github: "https://github.com/DuYi-Edu",
    // qq: "3263023350",
    // qqQrCode:
    //   "http://www.duyiedu.com/source/img/%E5%B0%8F%E6%B8%A1%E5%BE%AE%E4%BF%A1%E4%BA%8C%E7%BB%B4%E7%A0%81.png",
    // weixin: "yh777bao",
    // weixinQrCode:
    //   "http://www.duyiedu.com/source/img/%E5%85%AC%E4%BC%97%E5%8F%B7%E4%BA%8C%E7%BB%B4%E7%A0%81.png",
    // mail: "duyi@gmail.com",
    // icp: "黑ICP备17001719号",
    // githubName: "DuYi-Edu",
    // favicon: "http://mdrs.yuanjin.tech/Fs4CDlC6mwe_WXLMIiXcmSJLHO4f",

    avatar: "https://yuziikuko.gitee.io/avatar.jpg",
    siteTitle: "郁子IKUKO",
    github: "https://github.com/yuziikuko",
    qq: "1147998314",
    qqQrCode: "https://yuziikuko.gitee.io/assets/QQ-080dad53.jpg",
    weixin: "Suteeeee_IKUKO715",
    weixinQrCode: "https://yuziikuko.gitee.io/assets/WeChat-72416db2.png",
    mail: "cst1147998314@gmail.com",
    icp: `Copyright &copy; 2023-present
          <br />
          <span><a href="https://yuziikuko.gitee.io/">郁子IKUKO</a></span>`,
    githubName: "yuziikuko",
    favicon: "https://yuziikuko.gitee.io/logo.png",
  },
});
