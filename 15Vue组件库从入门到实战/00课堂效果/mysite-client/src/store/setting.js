// 用户信息
import { getSetting } from "@/api/setting";
import { displaySiteTitle } from "@/utils";

export default {
  namespaced: true,
  state: {
    loading: false,
    data: null,
  },
  mutations: {
    setLoading(state, payload) {
      state.loading = payload;
    },
    setData(state, payload) {
      state.data = payload;
    },
  },
  actions: {
    async fetchSetting(ctx) {
      if (ctx.state.data) return;
      ctx.commit("setLoading", true);
      const res = await getSetting();
      if (res) {
        ctx.commit("setData", res);
        // 修改页签图标
        if (res.favicon) {
          // <link rel=" icon " type="images/x-icon" href="./favicon.ico">
          let link = document.querySelector("link[rel='icon']");
          if (link) return;
          link = document.createElement("link");
          link.rel = "icon";
          link.href = res.favicon;
          document.querySelector("head").appendChild(link);
        }
        // 修改页面标题
        if (res.siteTitle) {
          displaySiteTitle.setSiteTitle(res.siteTitle);
        }
      }
      ctx.commit("setLoading", false);
    },
  },
};
