// 设置网站标题：路由标题-网站标题

let routeTitle = "",
  siteTitle = "";

const setTitle = () => {
  if (!routeTitle && !siteTitle) {
    document.title = "Loading...";
  } else if (routeTitle && !siteTitle) {
    document.title = routeTitle;
  } else if (!routeTitle && siteTitle) {
    document.title = siteTitle;
  } else {
    document.title = `${routeTitle}-${siteTitle}`;
  }
};

export default {
  setRouteTitle(title) {
    routeTitle = title;
    setTitle();
  },
  setSiteTitle(title) {
    siteTitle = title;
    setTitle();
  },
  getSiteTitle() {
    return siteTitle;
  },
};
