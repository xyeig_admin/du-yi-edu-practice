// vue-cli的配置文件
module.exports = {
  devServer: {
    proxy: {
      "/api": {
        // target: "https://study.duyiedu.com",
        target: "http://localhost:7001",
      },
      "/static": {
        target: "http://localhost:7001",
      },
    },
  },
  configureWebpack: require("./webpack.config"),
};
