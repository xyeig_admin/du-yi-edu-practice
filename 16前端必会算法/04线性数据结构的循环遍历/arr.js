const array = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const searchArray = (arr) => {
  // 算法严谨性要求：必须有严谨性判断，arr.length可能会报错
  if (!arr) return;

  for (let i = 0; i < arr.length; i++) {
    console.log(arr[i]);
  }
};

searchArray(array);
