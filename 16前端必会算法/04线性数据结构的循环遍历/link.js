function Node(val) {
  this.value = val;
  this.next = null;
}

const node1 = new Node(1);
const node2 = new Node(2);
const node3 = new Node(3);
const node4 = new Node(4);
const node5 = new Node(5);

node1.next = node2;
node2.next = node3;
node3.next = node4;
node4.next = node5;

const searchLink = (root) => {
  if (!root) return;
  let node = root;
  while (true) {
    console.log(node.value);
    if (node.next) {
      node = node.next;
    } else {
      break;
    }
    // // 或者
    // if (node) {
    //   console.log(node.value);
    // } else {
    //   break;
    // }
    // node = node.next;
  }
};

searchLink(node1);
