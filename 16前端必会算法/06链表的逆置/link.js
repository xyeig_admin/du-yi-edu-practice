function Node(val) {
  this.value = val;
  this.next = null;
}
const node1 = new Node(1);
const node2 = new Node(2);
const node3 = new Node(3);
const node4 = new Node(4);
const node5 = new Node(5);
node1.next = node2;
node2.next = node3;
node3.next = node4;
node4.next = node5;

// const findLastNode = (root) => {
//   return root.next ? findLastNode(root.next) : root;
// };
// console.log(findLastNode(node1));
// // Node { value: 5, next: null }

const searchLink = (root) => {
  if (!root) return;
  console.log(root.value);
  searchLink(root.next);
};

searchLink(node1); // 1 2 3 4 5

const reverseLink = (root) => {
  if (!root.next.next) {
    // 当前是倒数第二个节点（node4）

    // 最后一个节点（node5）的next指向当前节点（node4）
    root.next.next = root;

    // 返回逆置后的根节点（node5）【递归出口】
    return root.next;
  } else {
    // 当前root位置在倒数第二个之前

    // 保存递归逆置后的根节点（node5）
    const newRoot = reverseLink(root.next);

    // 位置靠前的节点都要让下一个节点指向自己，同时自己的next置空（才能消除node1和node2互指的循环）
    root.next.next = root;
    root.next = null;

    // 将逆置后的根节点（node5）返还给上一级递归保存
    return newRoot;
  }
};

searchLink(reverseLink(node1)); // 5 4 3 2 1
