// 比较后得出是否需要交换当前a和b
const compare = (a, b) => b - a < 0;

// 将数组中的a和b位置上的值交换
const exchange = (arr, a, b) => {
  let temp = arr[a];
  arr[a] = arr[b];
  arr[b] = temp;
};

const array = [1, 7, 4, 2, 3, 6, 5, 9, 8];

// sort函数可以是冒泡排序、选择排序或其他任何排序算法
const sort = (arr) => {
  // 数组中每一位都要经历一趟排序
  for (let i = 0; i < arr.length; i++) {
    // -i 表示下一趟排序时不用再比较前面几趟排好的值
    let maxIndex = 0;
    for (let j = 0; j < arr.length - i; j++) {
      if (compare(arr[j], arr[maxIndex])) {
        maxIndex = j;
      }
    }
    // 交换最后一位满足的值 和 内层循环最后一个数（最后一个没有排过序的值）
    exchange(arr, arr.length - i - 1, maxIndex);
  }
};

sort(array);
console.log(array); // [1, 2, 3, 4, 5, 6, 7, 8, 9]
