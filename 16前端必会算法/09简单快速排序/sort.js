// 比较后得出是否需要交换当前a和b
const compare = (a, b) => b - a < 0;

// 将数组中的a和b位置上的值交换
const exchange = (arr, a, b) => {
  let temp = arr[a];
  arr[a] = arr[b];
  arr[b] = temp;
};

const array = [1, 7, 4, 2, 3, 6, 5, 9, 8];

// sort函数可以是冒泡排序、选择排序或其他任何排序算法
const sort = (arr) => {
  // 严谨性判断
  if (!arr || arr.length === 0) return [];

  // 选定基准元素
  const leader = arr[0];

  // 遍历排序左右数组
  let left = [],
    right = [];
  for (let i = 1; i < arr.length; i++) {
    if (compare(leader, arr[i])) left.push(arr[i]);
    else right.push(arr[i]);
  }
  console.log(left, leader, right);

  // 递归排序左右数组
  return [...sort(left), leader, ...sort(right)];
};

console.log(sort(array)); // [1, 2, 3, 4, 5, 6, 7, 8, 9]
