// 比较后得出是否需要交换当前a和b
const compare = (a, b) => b - a < 0;

// 将数组中的a和b位置上的值交换
const exchange = (arr, a, b) => {
  let temp = arr[a];
  arr[a] = arr[b];
  arr[b] = temp;
};

const array = [1, 7, 4, 2, 3, 6, 5, 9, 8];

// sort函数可以是冒泡排序、选择排序或其他任何排序算法
const sort = (arr) => {
  const quickSort = (arr, begin, end) => {
    // 严谨性判断
    if (begin >= end - 1) return;

    // 声明左右指针
    let left = begin,
      right = end;

    // 选定begin为基准元素划分左右区间
    do {
      // 判断条件前会先执行一次，实际上left是从begin+1开始
      do {
        left++;
      } while (left < right && arr[left] < arr[begin]);
      do {
        right--;
      } while (left < right && arr[right] >= arr[begin]);
      if (left < right) exchange(arr, left, right);
    } while (left < right);

    // 保存基准元素交换点
    const swapTemp = left === right ? right - 1 : right;

    // 交换基准元素
    exchange(arr, begin, swapTemp);

    // 递归排序左右区间
    quickSort(arr, begin, swapTemp);
    quickSort(arr, swapTemp + 1, end);
  };
  quickSort(arr, 0, arr.length);
};

sort(array);
console.log(array); // [1, 2, 3, 4, 5, 6, 7, 8, 9]
