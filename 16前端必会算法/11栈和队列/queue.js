function Queue() {
  this.arr = [];

  this.push = (value) => {
    this.arr.push(value);
  };

  this.pop = () => {
    return this.arr.shift();
  };
}

const queue = new Queue();

queue.push(1);
queue.push(2);
queue.push(3);
console.log(queue.arr); // [ 1, 2, 3 ]

queue.pop();
console.log(queue.arr); // [ 2, 3 ]

queue.pop();
console.log(queue.arr); // [ 3 ]

queue.pop();
console.log(queue.arr); // []
