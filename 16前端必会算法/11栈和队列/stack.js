function Stack() {
  this.arr = [];

  this.push = (value) => {
    this.arr.push(value);
  };

  this.pop = () => {
    return this.arr.pop();
  };
}

const stack = new Stack();

stack.push(1);
stack.push(2);
stack.push(3);
console.log(stack.arr); // [ 1, 2, 3 ]

stack.pop();
console.log(stack.arr); // [ 1, 2 ]

stack.pop();
console.log(stack.arr); // [ 1 ]

stack.pop();
console.log(stack.arr); // []
