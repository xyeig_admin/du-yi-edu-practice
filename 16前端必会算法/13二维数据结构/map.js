function Node(val) {
  this.value = val;
  this.neighbor = [];
}
const a = new Node("a");
const b = new Node("b");
const c = new Node("c");
const d = new Node("d");
const e = new Node("e");
const f = new Node("f");

a.neighbor.push(b);
a.neighbor.push(c);
a.neighbor.push(f);

b.neighbor.push(a);
b.neighbor.push(d);
b.neighbor.push(e);

c.neighbor.push(a);

d.neighbor.push(b);

e.neighbor.push(b);

f.neighbor.push(a);
