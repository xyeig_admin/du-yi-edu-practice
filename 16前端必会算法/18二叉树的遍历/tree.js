function Node(val) {
  this.value = val;
  this.left = null;
  this.right = null;
}
const a = new Node("a");
const b = new Node("b");
const c = new Node("c");
const d = new Node("d");
const e = new Node("e");
const f = new Node("f");
const g = new Node("g");

a.left = c;
a.right = b;
c.left = f;
c.right = g;
b.left = d;
b.right = e;

// 前序遍历
// let res = "";
// const firstSearch = (root) => {
//   if (!root) return;
//   res += root.value;
//   firstSearch(root.left);
//   firstSearch(root.right);
//   return res;
// };
// console.log(firstSearch(a)); // acfgbde

// // 中序遍历
// let res = "";
// const centerSearch = (root) => {
//   if (!root) return;
//   centerSearch(root.left);
//   res += root.value;
//   centerSearch(root.right);
//   return res;
// };
// console.log(centerSearch(a)); // fcgadbe

// 后序遍历
let res = "";
const lastSearch = (root) => {
  if (!root) return;
  lastSearch(root.left);
  lastSearch(root.right);
  res += root.value;
  return res;
};
console.log(lastSearch(a)); // fgcdeba
