function Node(val) {
  this.value = val;
  this.left = null;
  this.right = null;
}

// // 根据前序中序还原二叉树
// const first = ["a", "c", "f", "g", "b", "d", "e"];
// const center = ["f", "c", "g", "a", "d", "b", "e"];
// const getTreeFromFirstAndCenter = (first, center) => {
//   if (
//     !first ||
//     !center ||
//     first.length === 0 ||
//     center.length === 0 ||
//     first.length !== center.length
//   )
//     return null;

//   const root = new Node(first[0]);

//   // 找出中序遍历中根节点的下标
//   const rootIndex = center.indexOf(root.value);

//   // 划分前序和中序中的左右子树区间
//   const firstLeft = first.slice(1, rootIndex + 1);
//   const firstRight = first.slice(rootIndex + 1, first.length);
//   const centerLeft = center.slice(0, rootIndex);
//   const centerRight = center.slice(rootIndex + 1, center.length);

//   // 递归生成左右子树
//   root.left = getTreeFromFirstAndCenter(firstLeft, centerLeft);
//   root.right = getTreeFromFirstAndCenter(firstRight, centerRight);
//   return root;
// };
// console.log(getTreeFromFirstAndCenter(first, center));

// 根据中序后序还原二叉树
const center = ["f", "c", "g", "a", "d", "b", "e"];
const last = ["f", "g", "c", "d", "e", "b", "a"];
const getTreeFromCenterAndLast = (center, last) => {
  if (
    !center ||
    !last ||
    center.length === 0 ||
    last.length === 0 ||
    last.length !== center.length
  )
    return null;

  const root = new Node(last[last.length - 1]);

  // 找出中序遍历中根节点的下标
  const rootIndex = center.indexOf(root.value);

  // 划分中序和后序中的左右子树区间
  const centerLeft = center.slice(0, rootIndex);
  const centerRight = center.slice(rootIndex + 1, center.length);
  const lastLeft = last.slice(0, rootIndex);
  const lastRight = last.slice(rootIndex, last.length - 1);

  // 递归生成左右子树
  root.left = getTreeFromCenterAndLast(centerLeft, lastLeft);
  root.right = getTreeFromCenterAndLast(centerRight, lastRight);
  return root;
};
console.log(getTreeFromCenterAndLast(center, last));
