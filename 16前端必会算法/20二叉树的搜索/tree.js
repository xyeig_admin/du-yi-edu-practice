function Node(val) {
  this.value = val;
  this.left = null;
  this.right = null;
}
const a = new Node("a");
const b = new Node("b");
const c = new Node("c");
const d = new Node("d");
const e = new Node("e");
const f = new Node("f");
const g = new Node("g");

a.left = c;
a.right = b;
c.left = f;
c.right = g;
b.left = d;
b.right = e;

// 深度优先搜索
const deepSearch = (root, target) => {
  if (!root) return false;
  // console.log(root.value); // a c f g b d e
  if (root.value === target) return true;
  return deepSearch(root.left, target) || deepSearch(root.right, target);
};
console.log(deepSearch(a, "f")); // true
console.log(deepSearch(a, "n")); // false

// 广度优先搜索
const breadthSearch = (rootList, target) => {
  if (!rootList || rootList.length === 0) return false;
  const children = [];
  for (let i = 0; i < rootList.length; i++) {
    if (rootList[i]) {
      // console.log(rootList[i].value); // a c b f g d e
      if (rootList[i].value === target) return true;
      children.push(rootList[i].left);
      children.push(rootList[i].right);
    }
  }
  return breadthSearch(children, target);
};
console.log(breadthSearch([a], "f")); // true
console.log(breadthSearch([a], "n")); // false
