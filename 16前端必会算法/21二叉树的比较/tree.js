function Node(val) {
  this.value = val;
  this.left = null;
  this.right = null;
}

// 树1
const a1 = new Node("a");
const b1 = new Node("b");
const c1 = new Node("c");
const d1 = new Node("d");
const e1 = new Node("e");
const f1 = new Node("f");
const g1 = new Node("g");

a1.left = c1;
a1.right = b1;
c1.left = f1;
c1.right = g1;
b1.left = d1;
b1.right = e1;

// 树2
const a2 = new Node("a");
const b2 = new Node("b");
const c2 = new Node("c");
const d2 = new Node("d");
const e2 = new Node("e");
const f2 = new Node("f");
const g2 = new Node("g");

a2.left = c2;
a2.right = b2;
c2.left = f2;
c2.right = g2;
b2.left = d2;
b2.right = e2;

// 左右子树互换后不是同一棵树
// const compareTree = (root1, root2) => {
//   // 都为空或结构相同
//   if ((!root1 && !root2) || root1 === root2) return true;

//   // 一棵为空另一棵不为空
//   if ((!root1 && root2) || (root1 && !root2)) return false;

//   // 相同位置的节点的值不同
//   if (root1.value !== root2.value) return false;

//   // 左右子树分别相同时才相同
//   return (
//     compareTree(root1.left, root2.left) && compareTree(root1.right, root2.right)
//   );
// };
// console.log(compareTree(a1, a2)); // true

// 左右子树互换后视为同一棵树
const compareTree = (root1, root2) => {
  // 都为空或结构相同
  if ((!root1 && !root2) || root1 === root2) return true;

  // 一棵为空另一棵不为空
  if ((!root1 && root2) || (root1 && !root2)) return false;

  // 相同位置的节点的值不同
  if (root1.value !== root2.value) return false;

  // 左右子树分别相同或镜像相同时才相同
  return (
    (compareTree(root1.left, root2.left) &&
      compareTree(root1.right, root2.right)) ||
    (compareTree(root1.left, root2.left) &&
      compareTree(root1.right, root2.right))
  );
};
console.log(compareTree(a1, a2)); // true
