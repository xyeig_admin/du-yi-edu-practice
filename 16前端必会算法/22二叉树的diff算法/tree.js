function Node(val) {
  this.value = val;
  this.left = null;
  this.right = null;
}

// 树1
const a1 = new Node("a");
const b1 = new Node("b");
const c1 = new Node("c");
const d1 = new Node("d");
const e1 = new Node("e");
const f1 = new Node("f");
const g1 = new Node("g");

a1.left = c1;
a1.right = b1;
c1.left = f1;
c1.right = g1;
b1.left = d1;
b1.right = e1;

// 树2
const a2 = new Node("a");
const b2 = new Node("b");
const c2 = new Node("c");
const d2 = new Node("d");
const e2 = new Node("e");
const f2 = new Node("f");
const g2 = new Node("g");

a2.left = c2;
a2.right = b2;
c2.left = f2;
c2.right = g2;
// b2.left = d2;
b2.right = e2;
e2.left = new Node("h");

const diffTree = (root1, root2, diffList) => {
  if (root1 === root2) return diffList;
  if (!root1 && root2) {
    // 新增
    diffList.push({
      type: "Add",
      origin: null,
      new: root2,
    });
  } else if (root1 && !root2) {
    // 删除
    diffList.push({
      type: "Delete",
      origin: root1,
      new: null,
    });
  } else if (root1.value !== root2.value) {
    // 修改
    diffList.push({
      type: "Edit",
      origin: root1,
      new: root2,
    });
    diffTree(root1.left, root2.left, diffList);
    diffTree(root1.right, root2.right, diffList);
  } else {
    diffTree(root1.left, root2.left, diffList);
    diffTree(root1.right, root2.right, diffList);
  }
  return diffList;
};
const diffList = [];
diffTree(a1, a2, diffList);
console.log(diffList);
