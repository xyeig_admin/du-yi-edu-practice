const MAX = 1000000;

function Node(val) {
  this.value = val;
  this.neighbors = [];
}

// 点集合
const POINTS = ["A", "B", "C", "D", "E"].map((p) => new Node(p));

// 边集合
const DISTANCE = [
  [0, 4, 7, MAX, MAX],
  [4, 0, 8, 6, MAX],
  [7, 8, 0, 5, MAX],
  [MAX, 6, 5, 0, 7],
  [MAX, MAX, MAX, 7, 0],
];

/**
 * 普利姆算法
 */
const connected = [];

// 根据当前节点在点集中的下标，找到其在边集中的下标
const getNodeIndexInPoints = (val) => {
  for (let i = 0; i < POINTS.length; i++) {
    if (POINTS[i].value === val) return i;
  }
  return -1;
};

// 遍历已连接的点集，找到以每个节点为起点且路径最短的边
const getMinDistanceNode = (points, distance, connected) => {
  let startNode = null,
    nextNode = null,
    min = MAX;
  for (let i = 0; i < connected.length; i++) {
    const row = distance[getNodeIndexInPoints(connected[i].value)];
    for (let j = 0; j < row.length; j++) {
      // 不是当前点本身，与当前点之间有通路，没有被连接过的点才能继续比较距离长短
      if (row[j] === 0 || row[j] === MAX || connected.includes(points[j]))
        continue;

      if (row[j] < min) {
        startNode = connected[i];
        nextNode = points[j];
        min = row[j];
      }
    }
  }
  // 连接起点和最短边端点
  startNode.neighbors.push(nextNode);
  nextNode.neighbors.push(startNode);
  return nextNode;
};

const prim = (points, distance, start) => {
  connected.push(start);
  // 获取最小代价的边
  while (true) {
    // 所有的节点都连通了
    if (connected.length === points.length) break;

    const minDistanceNode = getMinDistanceNode(points, distance, connected);
    connected.push(minDistanceNode);
  }
};
prim(POINTS, DISTANCE, POINTS[2]);
console.log(connected);

/* 
[
  Node { value: 'C', neighbors: [ [Node] ] },
  Node { value: 'D', neighbors: [ [Node], [Node], [Node] ] },
  Node { value: 'B', neighbors: [ [Node], [Node] ] },
  Node { value: 'A', neighbors: [ [Node] ] },
  Node { value: 'E', neighbors: [ [Node] ] }
]
 */
