const MAX = 1000000;

function Node(val) {
  this.value = val;
  this.neighbors = [];
}

// 点集合
const POINTS = ["A", "B", "C", "D", "E"].map((p) => new Node(p));

// 边集合
const DISTANCE = [
  [0, 4, 7, MAX, MAX],
  [4, 0, 8, 6, MAX],
  [7, 8, 0, 5, MAX],
  [MAX, 6, 5, 0, 7],
  [MAX, MAX, MAX, 7, 0],
];

/**
 * 克鲁斯卡尔算法
 */
// 二维数组，每一个子数组表示图上已连接的组
const connected = [];

// 获取节点在已连接的集合中所属的组
const getGroupInConnected = (node) => {
  for (let i = 0; i < connected.length; i++) {
    if (connected[i].includes(node)) {
      return connected[i];
    }
  }
  return null;
};

// 判断当前两个点是否满足连接的要求
const judgeConnect = (node1, node2) => {
  /**
   * 1.两个点都未连接（不属于任何组）=> 可以连接，产生一个新的组
   * 2.点1在A组，点2未连接 => 可以连接，点2加入A组
   * 3.点1未连接，点2在B组 => 可以连接，点1加入B组
   * 4.点1在A组，点2在B组 => 可以连接，合并两个组产生一个新的组，移除原先的两个组
   * 5.点1和点2在同一个组 => 不可以连接
   */
  groupA = getGroupInConnected(node1);
  groupB = getGroupInConnected(node2);
  if (groupA && groupB && groupA === groupB) return false;
  return true;
};

// 连接最短路径的左右端点
const handleConnectNode = (node1, node2) => {
  /**
   * 1.两个点都未连接（不属于任何组）=> 可以连接，产生一个新的组
   * 2.点1在A组，点2未连接 => 可以连接，点2加入A组
   * 3.点1未连接，点2在B组 => 可以连接，点1加入B组
   * 4.点1在A组，点2在B组 => 可以连接，合并两个组产生一个新的组，移除原先的两个组
   */
  groupA = getGroupInConnected(node1);
  groupB = getGroupInConnected(node2);
  if (!groupA && !groupB) {
    connected.push([node1, node2]);
  } else if (groupA && !groupB) {
    groupA.push(node2);
  } else if (!groupA && groupB) {
    groupB.push(node1);
  } else if (groupA && groupB && groupA !== groupB) {
    connected.push([...groupA, ...groupB]);

    // A、B组查找到后就立即删除，否则B组先找到的下标在A组被删掉后会变化
    const groupAIndex = connected.indexOf(groupA);
    connected.splice(groupAIndex, 1);
    const groupBIndex = connected.indexOf(groupB);
    connected.splice(groupBIndex, 1);
  }
  node1.neighbors.push(node2);
  node2.neighbors.push(node1);
};

const kruskal = (points, distance) => {
  while (true) {
    if (connected.length === 1 && connected[0].length === POINTS.length) break;

    let startNode = null,
      nextNode = null,
      min = MAX;
    for (let i = 0; i < distance.length; i++) {
      for (let j = 0; j < distance[i].length; j++) {
        const dis = distance[i][j];

        // 不是当前点本身，与当前点之间有通路，距离比当前已找到的最短路径还短才能连接
        if (dis === 0 || dis === MAX) continue;

        if (dis < min && judgeConnect(points[i], points[j])) {
          startNode = points[i];
          nextNode = points[j];
          min = dis;
        }
      }
    }
    handleConnectNode(startNode, nextNode);
  }
};

kruskal(POINTS, DISTANCE);
console.log(connected);

// for (let i = 0; i < connected[0].length; i++) {
//   console.log(connected[0][i].value, ...connected[0][i].neighbors);
// }

/* 
[
  [
    Node { value: 'A', neighbors: [Array] },
    Node { value: 'B', neighbors: [Array] },
    Node { value: 'C', neighbors: [Array] },
    Node { value: 'D', neighbors: [Array] },
    Node { value: 'E', neighbors: [Array] }
  ]
]
 */
