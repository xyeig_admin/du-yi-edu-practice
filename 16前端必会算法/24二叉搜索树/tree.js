const arr = [];
for (let i = 0; i < 10000; i++) {
  arr[i] = Math.floor(Math.random() * 10000);
}

/**
 * 用数组实现
 */
let count = 0;
const normalSearch = (arr, target) => {
  for (let i = 0; i < arr.length; i++) {
    count++;
    if (arr[i] === target) return true;
  }
  return false;
};
console.log(normalSearch(arr, 1000), count); // true 3394

/**
 * 用二叉搜索树实现
 */
function Node(val) {
  this.value = val;
  this.left = null;
  this.right = null;
}

// 添加节点
const addNode = (root, val) => {
  if (!root || root.value === val) return;
  if (root.value < val) {
    if (!root.right) root.right = new Node(val);
    else addNode(root.right, val);
  } else {
    if (!root.left) root.left = new Node(val);
    else addNode(root.left, val);
  }
};

// 构建二叉搜索树
const buildSearchTree = (arr) => {
  if (!arr || arr.length === 0) return;
  const root = new Node(arr[0]);
  for (let i = 1; i < arr.length; i++) {
    addNode(root, arr[i]);
  }
  return root;
};

// 搜索二叉搜索树
let count2 = 0;
const searchBySearchTree = (root, target) => {
  if (!root) return false;
  count2++;
  if (root.value === target) return true;
  else if (root.value < target) return searchBySearchTree(root.right, target);
  else return searchBySearchTree(root.left, target);
};

const root = buildSearchTree(arr);
console.log(searchBySearchTree(root, 1000), count2); // true 11
