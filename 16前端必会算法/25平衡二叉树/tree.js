function Node(val) {
  this.value = val;
  this.left = null;
  this.right = null;
}
const a = new Node("a");
const b = new Node("b");
const c = new Node("c");
const d = new Node("d");
const e = new Node("e");
const f = new Node("f");
const g = new Node("g");

a.left = c;
a.right = b;
c.left = f;
c.right = g;
b.left = d;
b.right = e;

// 获取树的深度
const getDeep = (root) => {
  if (!root) return 0;
  return Math.max(getDeep(root.left), getDeep(root.right)) + 1;
};

// 判断是否是平衡二叉树
const isBalanceTree = (root) => {
  if (!root) return true;
  if (Math.abs(getDeep(root.left) - getDeep(root.right)) > 1) return false;
  return isBalanceTree(root.left) && isBalanceTree(root.right);
};
console.log(isBalanceTree(a));
