function Node(val) {
  this.value = val;
  this.left = null;
  this.right = null;
}
const node2 = new Node("2");
const node3 = new Node("3");
const node5 = new Node("5");
const node6 = new Node("6");

// 左深
node6.left = node3;
node3.left = node2;
node3.right = node5;

// 获取树的深度
const getDeep = (root) => {
  if (!root) return 0;
  return Math.max(getDeep(root.left), getDeep(root.right)) + 1;
};

// 判断是否是平衡二叉树
const isBalanceTree = (root) => {
  if (!root) return true;
  if (Math.abs(getDeep(root.left) - getDeep(root.right)) > 1) return false;
  return isBalanceTree(root.left) && isBalanceTree(root.right);
};

// 左单旋
const handleLeftRotate = (root) => {
  // 找到新根
  let newRoot = root.right;
  // 找到变化分支
  let changeBranch = root.right.left;
  // 当前旋转节点的右子树节点为变化分支
  root.right = changeBranch;
  // 新根的左子树节点为旋转节点
  newRoot.left = root;
  // 返回新的根节点
  return newRoot;
};

// 右单旋
const handleRightRotate = (root) => {
  // 找到新根
  let newRoot = root.left;
  // 找到变化分支
  let changeBranch = root.left.right;
  // 当前旋转节点的左子树节点为变化分支
  root.left = changeBranch;
  // 新根的右子树节点为旋转节点
  newRoot.right = root;
  // 返回新的根节点
  return newRoot;
};

// 转换不平衡二叉树，返回平衡后的新根
const handleChangeToBalanceTree = (root) => {
  if (isBalanceTree(root)) return root;

  if (!root.left) root.left = handleChangeToBalanceTree(root.left);
  if (!root.right) root.right = handleChangeToBalanceTree(root.right);

  const leftDeep = getDeep(root.left),
    rightDeep = getDeep(root.right);
  if (Math.abs(leftDeep - rightDeep) < 2) {
    return root;
  } else if (leftDeep > rightDeep) {
    // 左边深，右旋
    return handleRightRotate(root);
  } else {
    // 右边深，左旋
    return handleLeftRotate(root);
  }
};

// 右旋
console.log(isBalanceTree(node6)); // false
const newRoot = handleChangeToBalanceTree(node6);
console.log(isBalanceTree(newRoot)); // true
console.log(newRoot);
