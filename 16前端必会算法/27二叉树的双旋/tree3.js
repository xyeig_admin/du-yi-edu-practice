function Node(val) {
  this.value = val;
  this.left = null;
  this.right = null;
}
const node9 = new Node("9");
const node8 = new Node("8");
const node7 = new Node("7");
const node6 = new Node("6");
const node5 = new Node("5");
const node4 = new Node("4");

node9.left = node6;
node6.left = node5;
node6.right = node7;
node5.left = node4;
node7.right = node8;

// 获取树的深度
const getDeep = (root) => {
  if (!root) return 0;
  return Math.max(getDeep(root.left), getDeep(root.right)) + 1;
};

// 判断是否是平衡二叉树
const isBalanceTree = (root) => {
  if (!root) return true;
  if (Math.abs(getDeep(root.left) - getDeep(root.right)) > 1) return false;
  return isBalanceTree(root.left) && isBalanceTree(root.right);
};

// 左单旋
const handleLeftRotate = (root) => {
  // 找到新根
  let newRoot = root.right;
  // 找到变化分支
  let changeBranch = root.right.left;
  // 当前旋转节点的右子树节点为变化分支
  root.right = changeBranch;
  // 新根的左子树节点为旋转节点
  newRoot.left = root;
  // 返回新的根节点
  return newRoot;
};

// 右单旋
const handleRightRotate = (root) => {
  // 找到新根
  let newRoot = root.left;
  // 找到变化分支
  let changeBranch = root.left.right;
  // 当前旋转节点的左子树节点为变化分支
  root.left = changeBranch;
  // 新根的右子树节点为旋转节点
  newRoot.right = root;
  // 返回新的根节点
  return newRoot;
};

// 转换不平衡二叉树，返回平衡后的新根
const handleChangeToBalanceTree = (root) => {
  if (isBalanceTree(root)) return root;

  if (!root.left) root.left = handleChangeToBalanceTree(root.left);
  if (!root.right) root.right = handleChangeToBalanceTree(root.right);

  const leftDeep = getDeep(root.left),
    rightDeep = getDeep(root.right);
  if (Math.abs(leftDeep - rightDeep) < 2) {
    return root;
  } else if (leftDeep > rightDeep) {
    // 左边深
    // 判断是否需要右左双旋
    const changeBranchDeep = getDeep(root.left.right);
    const noChangeBranchDeep = getDeep(root.left.left);
    if (changeBranchDeep > noChangeBranchDeep) {
      root.left = handleLeftRotate(root.left);
    }
    // 判断是否需要右右双旋
    let newRoot = handleRightRotate(root);
    newRoot.right = handleChangeToBalanceTree(newRoot.right);
    // 右子树修改后新的二叉树可能不平衡
    newRoot = handleChangeToBalanceTree(newRoot);
    return newRoot;
  } else {
    // 右边深
    // 判断是否需要左右双旋
    const changeBranchDeep = getDeep(root.right.left);
    const noChangeBranchDeep = getDeep(root.right.right);
    if (changeBranchDeep > noChangeBranchDeep) {
      root.right = handleRightRotate(root.right);
    }
    // 判断是否需要左左双旋
    let newRoot = handleLeftRotate(root);
    newRoot.left = handleChangeToBalanceTree(newRoot.left);
    // 左子树修改后新的二叉树可能不平衡
    newRoot = handleChangeToBalanceTree(newRoot);
    return newRoot;
  }
};

console.log(isBalanceTree(node9)); // false
const newRoot = handleChangeToBalanceTree(node9);
console.log(isBalanceTree(newRoot)); // true
console.log(newRoot);
