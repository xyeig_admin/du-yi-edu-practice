function Node(val) {
  this.value = val;
  this.children = [];
}
const a = new Node("a");
const b = new Node("b");
const c = new Node("c");
const d = new Node("d");
const e = new Node("e");
const f = new Node("f");

a.children.push(c);
a.children.push(f);
a.children.push(b);
b.children.push(d);
b.children.push(e);

const deepSearchTree = (root, target) => {
  if (!root) return false;
  if (root.value === target) return true;
  let res = false;
  root.children.forEach((child) => (res |= deepSearchTree(child, target)));
  return !!res;
};
console.log(deepSearchTree(a, "c")); // true
console.log(deepSearchTree(a, "n")); // false
