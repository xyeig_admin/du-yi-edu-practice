function Node(val) {
  this.value = val;
  this.children = [];
}
const a = new Node("a");
const b = new Node("b");
const c = new Node("c");
const d = new Node("d");
const e = new Node("e");
const f = new Node("f");

a.children.push(c);
a.children.push(f);
a.children.push(b);
b.children.push(d);
b.children.push(e);

const broadcastSearchTree = (roots, target) => {
  if (!roots || roots.length === 0) return false;
  let children = [];
  for (const root of roots) {
    if (root.value === target) return true;
    else children = [...children, ...root.children];
  }
  return broadcastSearchTree(children, target);
};
console.log(broadcastSearchTree([a], "c")); // true
console.log(broadcastSearchTree([a], "n")); // false
