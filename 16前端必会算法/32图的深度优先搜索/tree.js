function Node(val) {
  this.value = val;
  this.neighbor = [];
}
const a = new Node("a");
const b = new Node("b");
const c = new Node("c");
const d = new Node("d");
const e = new Node("e");

a.neighbor.push(b);
a.neighbor.push(c);
b.neighbor.push(a);
b.neighbor.push(c);
b.neighbor.push(d);
c.neighbor.push(a);
c.neighbor.push(b);
c.neighbor.push(d);
d.neighbor.push(b);
d.neighbor.push(c);
d.neighbor.push(e);
e.neighbor.push(d);

const deepSearchGraph = (node, target, searched) => {
  if (!node) return false;
  if (searched.includes(node)) return false;
  if (node.value === target) return true;
  searched.push(node);
  let res = false;
  node.neighbor.forEach(
    (child) => (res |= deepSearchGraph(child, target, searched))
  );
  return !!res;
};
console.log(deepSearchGraph(a, "c", [])); // true
console.log(deepSearchGraph(a, "n", [])); // false
