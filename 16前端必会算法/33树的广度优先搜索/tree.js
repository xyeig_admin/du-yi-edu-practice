function Node(val) {
  this.value = val;
  this.children = [];
}
const a = new Node("a");
const b = new Node("b");
const c = new Node("c");
const d = new Node("d");
const e = new Node("e");
const f = new Node("f");

a.children.push(c);
a.children.push(f);
a.children.push(b);
b.children.push(d);
b.children.push(e);

const broadcastSearchGraph = (nodes, target, searched) => {
  if (!nodes || nodes.length === 0) return false;
  let children = [];
  for (const node of nodes) {
    if (searched.includes(node)) continue;
    searched.push(node);
    if (node.value === target) return true;
    else children = [...children, ...node.children];
  }
  return broadcastSearchGraph(children, target, searched);
};
console.log(broadcastSearchGraph([a], "c", [])); // true
console.log(broadcastSearchGraph([a], "n", [])); // false
