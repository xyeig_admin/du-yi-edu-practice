// const fibonacci = (n) => {
//   if (n <= 0) return -1;
//   if (n === 1) return 0;
//   if (n === 2) return 1;
//   let a = 0,
//     b = 1,
//     c;
//   for (let i = 3; i <= n; i++) {
//     c = a + b;
//     a = b;
//     b = c;
//   }
//   return c;
// };
// console.log(fibonacci(5)); // 3

const fibonacci = (n) => {
  if (n <= 0) return -1;
  if (n === 1) return 0;
  if (n === 2) return 1;
  return fibonacci(n - 1) + fibonacci(n - 2);
};
console.log(fibonacci(5)); // 3
