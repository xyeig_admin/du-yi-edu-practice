const jump = (n) => {
  if (n <= 0) return -1;
  if (n === 1) return 1;
  if (n === 2) return 2;
  return jump(n - 1) + jump(n - 2);
};
console.log(jump(4));
/**
 * 1 1 1 1
 * 1 1 2
 * 1 2 1
 * 2 1 1
 * 2 2
 */
