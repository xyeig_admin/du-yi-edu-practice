const jump = (n) => {
  if (n <= 0) return -1;
  if (n === 1) return 1;
  if (n === 2) return 2;
  let res = 0;
  for (let i = 1; i < n; i++) {
    res += jump(n - i);
  }
  // + 1 表示从第0级台阶跳上来，即 f(0)
  return res + 1;
};
console.log(jump(4));
/**
 * 1 1 1 1
 * 1 1 2
 * 1 2 1
 * 2 1 1
 * 2 2
 * 1 3
 * 3 1
 * 4
 */
