/**
 * 批量导出文件
 * 参数1：API目录的相对路径
 * 参数2：是否查询子目录
 * 参数3：需要查询的文件后缀名
 */

const requireApi = require.context(".", true, /.js$/);
// console.log(requireApi.keys());

let modules = {};

requireApi.keys().forEach((path) => {
  // 当前文件，跳过
  if (path === "./index.js") return;

  Object.assign(modules, requireApi(path));
});

// console.log(modules);
export default modules;