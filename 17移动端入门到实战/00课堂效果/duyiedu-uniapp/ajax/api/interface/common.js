import ajax from "../../http.js";

export const getCurrentVersion = (data) =>
	ajax({
		name: "getCurrentVersion",
		data,
	});