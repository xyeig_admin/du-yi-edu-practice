import ajax from "../../http.js";

export const getLabelList = (data) =>
  ajax({
    name: "getLabelList",
    data,
  });

export const getArticleList = (data) =>
  ajax({
    name: "getArticleList",
    data,
  });

export const updateSaveLikes = (data) =>
  ajax({
    name: "updateSaveLikes",
    data,
  });

export const getSearchHistory = (data) =>
  ajax({
    name: "getSearchHistory",
    data,
  });

export const updateLabelIds = (data) =>
  ajax({
    name: "updateLabelIds",
    data,
  });

export const getArticleDetail = (data) =>
  ajax({
    name: "getArticleDetail",
    data,
  });

export const updateArticleComment = (data) =>
  ajax({
    name: "updateArticleComment",
    data,
  });

export const getCommentList = (data) =>
  ajax({
    name: "getCommentList",
    data,
  });

export const updateFollowAuthor = (data) =>
  ajax({
    name: "updateFollowAuthor",
    data,
  });

export const updateLikeArticle = (data) =>
  ajax({
    name: "updateLikeArticle",
    data,
  });