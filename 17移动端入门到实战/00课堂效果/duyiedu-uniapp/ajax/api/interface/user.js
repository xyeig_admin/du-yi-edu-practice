import ajax from "../../http.js";

export const login = (data) =>
	ajax({
		name: "login",
		data,
	});

export const sendVerificationCode = (data) =>
	ajax({
		name: "sendVerificationCode",
		data,
	});

export const getFollowArticle = (data) =>
	ajax({
		name: "getFollowArticle",
		data,
	});

export const getFollowAuthor = (data) =>
	ajax({
		name: "getFollowAuthor",
		data,
	});

export const getMyArticle = (data) =>
	ajax({
		name: "getMyArticle",
		data,
	});

export const updateFeedback = (data) =>
	ajax({
		name: "updateFeedback",
		data,
	});

export const updateUserAvatar = (data) =>
	ajax({
		name: "updateUserAvatar",
		data,
	});