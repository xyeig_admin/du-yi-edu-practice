export default ({
  name,
  data = {}
}) => {
  const loading = data.isLoading;
  data.isLoading && delete data.isLoading;

  return new Promise((resolve, reject) => {
    !loading &&
      uni.showLoading({
        title: "加载中...",
      });
    uniCloud.callFunction({
      name, // 要调用的云函数名称
      data,
      success({
        result
      }) {
        if (result.code === 0) {
          resolve(result.data);
        } else {
          uni.showToast({
            icon: "error",
            title: result.msg,
          });
        }
      },
      fail(err) {
        // 网络错误或后端错误
        reject(err);
      },
      complete() {
        // 不论请求成功还是失败都执行
        !loading && uni.hideLoading();
      },
    });
  });
};