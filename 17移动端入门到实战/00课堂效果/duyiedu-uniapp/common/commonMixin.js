import {
	mapState,
	mapMutations
} from "vuex";

export default {
	install(Vue) {
		Vue.mixin({
			computed: {
				...mapState(["userInfo"]),
			},
			methods: {
				...mapMutations(["updateUserInfo"]),
				// 检测用户是否登录
				async handleCheckedLogin() {
					return new Promise((resolve) => {
						if (this.userInfo) {
							resolve();
						} else {
							uni.navigateTo({
								url: "/pages/UserInfo/Login/Login",
							});
						}
					});
				},
			},
		});
	},
};