import App from './App'
import Vue from 'vue'
import './uni.promisify.adaptor'
Vue.config.productionTip = false

// 挂载$http方法
import modules from "./ajax/api/index.js";
Vue.prototype.$http = modules;

// 使用混入
import userRulesMixin from "./common/userRulesMixin.js";
Vue.use(userRulesMixin);
import commonMixin from "./common/commonMixin.js";
Vue.use(commonMixin);

// 使用Vuex
import store from "./store/index.js";

// 引入uni-simple-router
import {
  router,
  RouterMount
} from './router.js'
Vue.use(router);

App.mpType = "app";
const app = new Vue({
  ...App,
  store,
});

//v1.3.5起 H5端 你应该去除原有的app.$mount();使用路由自带的渲染方式
// #ifdef H5
RouterMount(app, router, '#app')
// #endif

// #ifndef H5
app.$mount(); //为了兼容小程序及app端必须这样写才有效果
// #endif