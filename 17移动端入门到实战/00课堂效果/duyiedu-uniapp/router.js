// router.js
import {
  RouterMount,
  createRouter
} from 'uni-simple-router';

import store from './store/index.js';

const router = createRouter({
  platform: process.env.VUE_APP_PLATFORM,
  routes: [...ROUTES]
});

// 全局路由前置守卫
router.beforeEach((to, from, next) => {
  // 判断当前页面是否需要登录并且现在是没有登录的状态
  if (to.meta.auth && !store.state.userInfo) {
    next({
      name: "Login",
      NAVTYPE: "push", // 跳转到普通页面，新开保留历史记录
    });
  } else {
    next();
  }
});

// 全局路由后置守卫
// router.afterEach((to, from) => {
//   console.log('跳转结束')
// })

export {
  router,
  RouterMount
}