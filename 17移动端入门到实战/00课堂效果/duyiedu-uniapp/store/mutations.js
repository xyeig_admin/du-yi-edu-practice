export default {
	updateUserInfo(state, userInfo) {
		uni.setStorageSync("userInfo", userInfo);
		state.userInfo = userInfo;
	},
	updateSearchHistory(state, keyword) {
		let history = state.searchHistory;
		if (history.length > 0 && history.findIndex(item => item === keyword) > -1)
			return;
		history.unshift(keyword)
		uni.setStorageSync("searchHistory", history);
		state.searchHistory = history;
	},
	cleanSearchHistory(state) {
		uni.removeStorageSync("searchHistory");
		state.searchHistory = [];
		uni.showToast({
			icon: "success",
			title: "清空完成",
		});
	},
	updateLabelList(state, labelList) {
		uni.setStorageSync('labelList', labelList);
		state.labelList = labelList;
	},
};