"use strict";
const db = uniCloud.database();
exports.main = async (event, context) => {
	const {
		classify,
		page = 1,
		pageSize = 10
	} = event;

	let match = {};
	if (classify !== "全部") {
		match = {
			classify,
		};
	}

	const res = await db
		.collection("article")
		.aggregate() // 使用聚合的形式获取数据
		.match(match) // 根据匹配条件返回数据
		.project({
			content: 0, // 本次查询不需要返回文章内容 content 字段
		})
		.skip(pageSize * (page - 1)) // 首页从0开始
		.limit(pageSize) // 每页最多返回多少条数据
		.end();

	// 统计总数据
	const {
		total
	} = await db.collection("article").where(match).count();

	// 返回数据给客户端
	return {
		code: 0,
		msg: "数据请求成功",
		data: {
			data: res.data,
			total,
		},
	};
};