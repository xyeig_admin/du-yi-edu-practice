"use strict";
const db = uniCloud.database();
exports.main = async (event, context) => {
	const {
		searchVal
	} = event;

	const res = await db
		.collection("article")
		.aggregate() // 使用聚合的形式进行数据的获取
		.match({
			title: new RegExp(searchVal),
		}) // 使用正则表达式进行模糊匹配，只要包含就返回
		.project({
			content: 0, // 本次查询不需要返回文章详情给前端
		})
		.end();

	// 返回数据给客户端
	return {
		code: 0,
		msg: "搜索数据请求成功",
		data: {
			articleList: res.data,
		},
	};
};