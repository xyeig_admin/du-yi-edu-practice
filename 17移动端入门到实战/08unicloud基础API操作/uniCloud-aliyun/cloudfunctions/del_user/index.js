'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	const collection = db.collection('user')
	const {_id} = event;
	const res = await collection.doc(_id).remove()
	//返回数据给客户端
	return {
		code:0,
		list:res
	}
};
