'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	const collection = db.collection('user')
	//const res = await collection.doc('60fa211e9b33ed00016f3d6').update({userName:'测试用户名123'})
	const res = await collection.doc('60fa211e9b33ed00016f3d6').set({userName:'set方法定义的用户名'})
	
	//返回数据给客户端
	return {
		code:0,
		msg:res
	}
};
