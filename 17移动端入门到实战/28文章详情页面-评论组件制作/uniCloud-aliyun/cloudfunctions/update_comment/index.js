'use strict';
const db = uniCloud.database()
const dbCmd = db.command;
exports.main = async (event, context) => {
  const {
    userId,
    articleId,
    content
  } = event

  // 进行用户信息获取
  let user = await db.collection('user').doc(userId).get()
  user = user.data[0]

  const article = await db.collection('article').doc(articleId).get()
  // const comments = article.data[0].comments;

  let commentObj = {
    comment_id: generatedId(5),
    comment_content: content,
    create_time: Date.now(),
    is_reply: false,
    author: {
      author_id: user._id,
      author_name: user.author_name,
      avatar: user.avatar,
      professional: user.professional
    },
    replyArr: []
  }

  commentObj = dbCmd.unshift({ commentObj })
  await db.collection('article').doc(articleId).update({
    comments: commentObj
  })


  function generatedId (num) {
    return Number(Math.random().toString().substr(3, num) + Date.now()).toString(36)
  }

  //返回数据给客户端
  return { code: 0, data: { msg: '添加数据成功' } }
};
