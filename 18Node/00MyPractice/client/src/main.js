import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// 在网站被访问时，需要用token去换取用户的身份
store.dispatch("loginUser/whoAmI");

// 测试API接口
// import * as loginService from "./service/loginService";
// loginService.login("admin", "123456").then((r) => console.log(r));
// loginService.whoAmI().then((r) => console.log(r));

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
