import request from "./request";

// 模拟网络延迟
const delay = (duration) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, duration);
  });
};

export const login = async (loginId, loginPwd) => {
  await delay(2000);
  const resp = await request().post("/api/admin/login", {
    loginId,
    loginPwd,
  });
  return resp.data;
};

export const loginOut = () => {
  localStorage.removeItem("token");
};

export const whoAmI = async () => {
  await delay(2000);
  const resp = await request().get("/api/admin/whoami");
  return resp.data;
};
