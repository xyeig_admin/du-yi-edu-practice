// // 测试连接
// const sequelize = require("./models/db");
// (async () => {
//   try {
//     await sequelize.authenticate();
//     console.log("Connection has been established successfully.");
//   } catch (error) {
//     console.error("Unable to connect to the database:", error);
//   }
// })();

// /**
//  * 增加
//  */
// const adminService = require("./services/adminService");
// adminService.addAdmin({
//   loginId: "admin",
//   loginPwd: "123456",
// });
// /**
//  * 删除
//  */
// adminService.deleteAdmin(2);
// /**
//  * 修改
//  */
// adminService.updateAdmin(1, {
//   loginId: "aaa",
// });

// require("./models/sync");

// require("./models/relation");
// require("./mock/mockStudent");

// require("./spider/fetchBooks");

// const studentService = require("./services/studentService");
// studentService.getStudents(1, 10, false, "秀").then((r) => {
//   console.log(r, JSON.stringify(r.datas[0].Class));
// });

// const md5 = require("md5");
// const res = md5("asdfasdf");
// console.log(res); // 6a204bd89f3c8348afd5c77c717a097a

// const adminService = require("./services/adminService");
// adminService.addAdmin({
//   loginId: "admin",
//   loginPwd: "123456",
// });

require("./init");

// const studentService = require("./services/studentService");
// studentService
//   .addStudent({
//     name: "adfd",
//     birthday: "2010-3-5",
//     sex: true,
//     mobile: "15454545444",
//     ClassId: 3,
//     deletedAt: "2010-1-1",
//     a: 3,
//     b: 4,
//   })
//   .catch((err) => {
//     console.log(err);
//   });

/**
 * ======================  04  ======================
 */

// // 方式1
// const express = require("express");
// const http = require("http");
// // 创建一个express应用
// const app = express();
// // app实际上是一个函数，用于处理请求
// const server = http.createServer(app);
// server.listen(9527, () => {
//   console.log("server listening on 9527");
// });
// // 方式2
// const express = require("express");
// // 创建一个express应用
// const app = express();
// const port = 9527;
// // app实际上是一个函数，用于处理请求
// app.listen(port, () => {
//   console.log(`server listening on ${port}`);
// });

// // 静态路由
// app.get("/abc", (req, res) => {
//   // req 和 res 是被 express 封装过的对象，无需直接操作流
//   console.log("请求头", req.headers);
//   console.log("请求路径", req.path);
//   console.log("请求参数query", req.query);

//   // 设置响应头
//   // res.setHeader("Content-Type", "application/json");

//   // 响应请求
//   // res.send("<h1>你好啊</h1>");
//   // res.send({
//   //   a: 1,
//   // });

//   // 重定向
//   // res.status(302).header("location", "https://duyi.ke.qq.com").end();
//   // res.status(302).location("https://duyi.ke.qq.com").end();
//   res.redirect(302, "https://duyi.ke.qq.com");
// });
// // 动态路由
// app.get("/news/:id", (req, res) => {
//   console.log("动态参数params", req.params);
// });

/**
 * ejs
 */
// const ejs = require("ejs");
// ejs
//   .renderFile("./test.ejs", {
//     number: Math.random(),
//   })
//   .then((result) => {
//     console.log(result);
//   });
// const str = `
// 生成的数字是：<%= number %>
// `;
// const result = ejs.render(str, {
//   number: Math.random(),
// });
// console.log(result); // 0.31928499351079287

/**
 * qrcode
 */
// const QRCode = require("qrcode");
// QRCode.toDataURL("https://duyi.ke.qq.com/?tuin=a5d48d54", (err, url) => {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log(url);
//   }
// });
