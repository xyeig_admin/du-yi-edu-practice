const log4js = require("log4js");
const path = require("path");

// 封装sql和api公共appenders
const getCommonAppenders = (pathName) => {
  return {
    // 定义一个sql日志出口
    type: "dateFile",
    filename: path.resolve(__dirname, "logs", pathName, "logging.log"),
    maxLogSize: 1024 * 1024, // 配置文件的最大字节数
    keepFileExt: true,
    numBackups: 3, // 日志文件保留三天
    layout: {
      type: "pattern",
      pattern: "%c [%d{yyyy-MM-dd hh:mm:ss}] [%p]: %m%n",
    },
  };
};

log4js.configure({
  appenders: {
    // sql: {
    //   // 定义一个sql日志出口
    //   type: "dateFile",
    //   filename: path.resolve(__dirname, "logs", "sql", "logging.log"),
    //   maxLogSize: 1024 * 1024, // 配置文件的最大字节数
    //   keepFileExt: true,
    //   daysToKeep: 3, // 日志文件保留三天
    //   layout: {
    //     type: "pattern",
    //     pattern: "%c [%d{yyyy-MM-dd hh:mm:ss}] [%p]: %m%n",
    //   },
    // },
    sql: getCommonAppenders("sql"),
    default: {
      type: "stdout",
    },
    api: getCommonAppenders("api"),
  },
  categories: {
    /**
     * 分类1
     * 名称：sql（表示使用名为sql的出口）
     * 出口：（异步写入日志）
     *    出口名称：sql
     * 类别级别：需要记录的日志等级
     */
    sql: {
      appenders: ["sql"], // 该分类使用出口sql的配置写入日志
      level: "all",
    },
    default: {
      appenders: ["default"],
      level: "all",
    },
    api: {
      appenders: ["api"],
      level: "all",
    },
  },
});

// 程序正/异常退出时，还没记录完成的日志记录完
process.on("exit", () => {
  log4js.shutdown();
});

const sqlLogger = log4js.getLogger("sql");
const apiLogger = log4js.getLogger("api");
const defaultLogger = log4js.getLogger();

exports.sqlLogger = sqlLogger;
exports.apiLogger = apiLogger;
exports.logger = defaultLogger;
