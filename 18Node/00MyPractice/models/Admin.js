const sequelize = require("./db");
const { DataTypes } = require("sequelize");

// 创建一个模型对象
const Admin = sequelize.define(
  "Admin",
  {
    loginId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    loginPwd: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    // freezeTableName: true,
    // tableName: 'administrator',
    createdAt: false,
    updatedAt: false,
    paranoid: true, // 配置后，该表的数据不会真正的删除，而是增加一列deletedAt，记录删除的时间
  }
);

// // 将模型同步到数据库中，生成数据表
// (async () => {
//   await Admin.sync({
//     alter: true,
//   });
// })();

module.exports = Admin;
