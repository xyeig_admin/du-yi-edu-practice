const sequelize = require("./db");
const { DataTypes } = require("sequelize");

const Book = sequelize.define(
  "Book",
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    imgUrl: {
      type: DataTypes.STRING,
    },
    publishDate: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    author: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT("LONG"),
      allowNull: false,
    },
  },
  {
    paranoid: true,
  }
);

module.exports = Book;
