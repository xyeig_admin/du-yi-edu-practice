const { Sequelize } = require("sequelize");
const { sqlLogger } = require("../logger");

// Windows
const sequelize = new Sequelize("myschooldb", "root", "123123", {
// MacBook Pro
// const sequelize = new Sequelize("myschooldb", "root", "yg990715", {
  host: "localhost",
  dialect: "mysql", // 数据库类型
  // logging: null, // 隐藏数据库操作日志
  logging: (msg) => {
    sqlLogger.debug(msg);
  },
});

module.exports = sequelize;
