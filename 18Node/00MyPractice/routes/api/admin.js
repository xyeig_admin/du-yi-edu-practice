const express = require("express");
const adminService = require("../../services/adminService");
const { asyncHandler } = require("../getSendResult");
const cryptor = require("../../util/crypt");
const jwt = require("../jwt");

const router = express.Router();

router.post(
  "/login",
  asyncHandler(async (req, res) => {
    const result = await adminService.login(
      req.body.loginId,
      req.body.loginPwd
    );
    if (result) {
      // // 登录成功
      // res.header(
      //   "set-cookie",
      //   `token=${result.id}; path=/; domain=localhost; max-age=3600; httponly`
      // );

      // let value = result.id;
      // value = cryptor.encrypt(value.toString());
      // // 登录成功
      // res.cookie("token", value, {
      //   path: "/",
      //   domain: "localhost",
      //   maxAge: 7 * 24 * 3600 * 1000, // 毫秒数
      // });
      // res.header("authorization", value);

      // // 登录成功
      // req.session.loginUser = result;

      let value = result.id;
      // 登录成功
      jwt.publish(res, undefined, { id: value });
    }
    return result;
  })
);

router.get(
  "/whoami",
  asyncHandler(async (req, res) => {
    return await adminService.getAdminById(req.userId);
  })
);

module.exports = router;
