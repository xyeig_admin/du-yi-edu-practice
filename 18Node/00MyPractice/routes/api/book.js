const express = require("express");
const bookService = require("../../services/bookService");
const { asyncHandler } = require("../getSendResult");

const router = express.Router();

router.get(
  "/",
  asyncHandler(async (req, res) => {
    const page = req.query.page || 1;
    const limit = req.query.limit || 10;
    return await bookService.getBooks(page, limit);
  })
);

router.get(
  "/:id",
  asyncHandler(async (req, res) => {
    return await bookService.getBookById(req.params.id);
  })
);

router.post(
  "/",
  asyncHandler(async (req, res, next) => {
    return await bookService.addBook(req.body);
  })
);

module.exports = router;
