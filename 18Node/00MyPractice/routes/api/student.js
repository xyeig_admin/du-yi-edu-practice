const express = require("express");
const studentService = require("../../services/studentService");
const { asyncHandler } = require("../getSendResult");

const router = express.Router();

// // JSONP
// router.get("/", async (req, res) => {
//   const page = req.query.page || 1;
//   const limit = req.query.limit || 10;
//   const sex = req.query.sex || -1;
//   const name = req.query.name || "";

//   const result = await studentService.getStudents(page, limit, sex, name);
//   const json = JSON.stringify(result);
//   const script = `callback(${json})`;
//   res.header("content-type", "application/javascript").send(script);
// });

router.get(
  "/",
  asyncHandler(async (req, res) => {
    const page = req.query.page || 1;
    const limit = req.query.limit || 10;
    const sex = req.query.sex || -1;
    const name = req.query.name || "";
    return await studentService.getStudents(page, limit, sex, name);
  })
);

router.get(
  "/:id",
  asyncHandler(async (req, res) => {
    return await studentService.getStudentById(req.params.id);
  })
);

router.post(
  "/",
  asyncHandler(async (req, res, next) => {
    return await studentService.addStudent(req.body);
  })
);

router.delete(
  "/:id",
  asyncHandler(async (req, res, next) => {
    return await studentService.deleteStudent(req.params.id);
  })
);

router.put(
  "/:id",
  asyncHandler(async (req, res, next) => {
    return await studentService.updateStudent(req.params.id, req.body);
  })
);

module.exports = router;
