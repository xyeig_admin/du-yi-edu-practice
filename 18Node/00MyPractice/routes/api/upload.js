const express = require("express");
const router = express.Router();

// router.post("/", (req, res) => {
//   res.send("服务器完成了图片处理");
// });
// module.exports = router;

const multer = require("multer");
const path = require("path");

const watermark = require("../../util/watermark");
const waterPath = path.resolve(__dirname, "../../public/img/water.jpg");

// 保存的文件没有后缀名，会自动创建目录
// const upload = multer({
//   dest: path.resolve(__dirname, "../../public", "upload"),
// });

// 配置磁盘存储引擎，使保存的文件有后缀名，需要手动创建目录
const storage = multer.diskStorage({
  // 上传的文件存储的目录，一般在gitignore中忽略，服务器本身有自己的目录存放用户上传的文件
  destination: function (req, file, cb) {
    // cb(null, path.resolve(__dirname, "../../public/upload"));
    cb(null, path.resolve(__dirname, "../../public/origin"));
  },
  filename: function (req, file, cb) {
    // 时间戳-6位随机字符.文件后缀
    const timeStamp = Date.now();
    const randomStr = Math.random().toString(36).slice(-6);
    const ext = path.extname(file.originalname);
    const filename = `${timeStamp}-${randomStr}${ext}`;
    cb(null, filename);
  },
});

const upload = multer({
  storage,
  limits: {
    fileSize: 150 * 1024,
  },
  fileFilter(req, file, cb) {
    // 验证文件后缀名
    const extname = path.extname(file.originalname);
    const whitelist = [".jpg", ".gif", "png"];
    if (whitelist.includes(extname)) {
      cb(null, true);
    } else {
      cb(new Error(`your ext name of ${extname} is not support`));
    }
  },
});

router.post("/", upload.single("img"), async (req, res) => {
  const url = `/upload/${req.file.filename}`;

  // 加水印
  const newPath = path.resolve(
    __dirname,
    "../../public/upload",
    req.file.filename
  );
  await watermark(waterPath, req.file.path, newPath);

  res.send({
    code: 0,
    msg: "",
    data: url,
  });
});

module.exports = router;
