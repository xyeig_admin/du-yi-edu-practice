const { apiLogger } = require("../logger.js");
const log4js = require("log4js");

// 手动记录
// module.exports = (req, res, next) => {
//   next();
//   apiLogger.debug(`${req.method} ${req.path} ${req.ip}`);
// };

// 自动记录
module.exports = log4js.connectLogger(apiLogger, {
  level: "auto",
});
