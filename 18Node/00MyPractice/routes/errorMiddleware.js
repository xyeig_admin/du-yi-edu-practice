// 处理错误的中间件
const getMsg = require("./getSendResult");
const multer = require("multer");

module.exports = (err, req, res, next) => {
  // 获取基地址（调用中间件的use函数的第一个参数）
  console.log(req.baseUrl);

  if (err) {
    // const errObj = {
    //   code: 500,
    //   msg: err instanceof Error ? err.message : err,
    // };
    // // 发生了错误
    // res.status(500).send(errObj);

    if (err instanceof multer.MulterError) {
      res.status(200).send(getMsg.getErr(err.message));
      return;
    }
    const errObj = err instanceof Error ? err.message : err;
    res.status(500).send(getMsg.getErr(errObj));
  } else {
    next();
  }
};
