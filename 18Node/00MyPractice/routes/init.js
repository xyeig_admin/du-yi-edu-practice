/**
 * ========== 基本使用 & 中间件 ==========
 */
// const express = require("express");

// // 创建一个express应用
// const app = express();

// app.get("/news/abc", (req, res, next) => {
//   console.log("handler1");

//   // throw new Error("abc");
//   // // next();

//   // 相当于
//   next(new Error("abc"));
// });

// /**
//  * 能匹配：
//  *    /news
//  *    /news/abc
//  *    /news/123
//  *    /news/ab/adfs
//  * 不能匹配：
//  *    /n
//  *    /a
//  *    /
//  *    /newsabc
//  */
// app.use("/news", require("./errorMiddleware"));

// app.use(require("./errorMiddleware"));
// app.use(require("./staticMiddleware"));

// const port = 9527;
// app.listen(port, () => {
//   console.log(`server listen on ${port}`);
// });

/**
 * ========== 常用中间件 ==========
 */
// const express = require("express");

// const path = require("path");
// const staticRoot = path.resolve(__dirname, "../public");

// /**
//  * 下面这段代码的作用：
//  * 当请求时，会根据请求路径(req.path)，从指定的目录中寻找是否存在该文件，如果存在，直接响应文件内容，而不再移交给后续的中间件
//  * 如果不存在文件，则直接移交给后续的中间件处理
//  * 默认情况下，如果映射的结果是一个目录，则会自动使用index.html文件
//  */
// app.use(express.static(staticRoot));
// // // 只有当路径以 /static 开头时，才会调用中间件
// // app.use("/static", express.static(staticRoot));
// // // 原理
// // // app.use("/static", (req, res) => {
// // //   console.log(req.baseUrl, req.path);
// // // });

// app.use(express.urlencoded({
//   extended: true
// }));
// // app.use(require("./myUrlEncoded"));

// app.use(express.json());

// app.post("/api/student", (req, res) => {
//   console.log(req.body);
// });

// const port = 9527;
// app.listen(port, () => {
//   console.log(`server listen on ${port}`);
// });

/**
 * ========== 路由 ==========
 */
const express = require("express");
const app = express();

// 使用session
const session = require("express-session");
app.use(
  session({
    secret: "yuanjin",
    name: "sessionid",
  })
);

// 验证码
app.use("/captcha", require("./captchaMiddleware"));

// 使用代理
app.use(require("./proxyMiddleware"));

// 图片防盗链
app.use(require("./imgProtectMiddleware"));

// 解决客户端打包后刷新404问题
const history = require("connect-history-api-fallback");
app.use(history());

// 映射public目录中的静态资源
const path = require("path");
const staticRoot = path.resolve(__dirname, "../public");
// app.use(express.static(staticRoot));
app.use(
  express.static(staticRoot, {
    setHeaders(res, path) {
      if (!path.endsWith(".html")) {
        res.header("Cache-Control", `max-age=${3600 * 24 * 365 * 100}`);
      }
    },
  })
);

// 使用模板引擎
app.set("views", path.resolve(__dirname, "./views"));
app.use("/student", require("./controller/student"));

// 使用CORS中间件
// app.use(require("./corsMiddleware"));
const cors = require("cors");
// app.use(cors());
// const whiteList = ["http://127.0.0.1:5500", "http://localhost:9527", "null"];
app.use(
  cors({
    origin(origin, callback) {
      if (!origin) {
        callback(null, "*");
        return;
      }
      // if (whiteList.includes(origin)) {
      //   callback(null, origin);
      // } else {
      //   callback(new Error("not allowed"));
      // }
      callback(null, origin);
    },
    credentials: true,
  })
);

// 加入cookie-parser 中间件
// 加入之后，会在req对象中注入cookies属性，用于获取所有请求传递过来的cookie
// 加入之后，会在res对象中注入cookie方法，用于设置cookie
const cookieParser = require("cookie-parser");
app.use(cookieParser());
// 应用token中间件
app.use(require("./tokenMiddleware"));

// 解析 application/x-www-form-urlencoded 格式的请求体
app.use(express.urlencoded({ extended: true }));

// 解析 application/json 格式的请求体
app.use(express.json());

// 记录API请求日志
app.use(require("./apiLoggerMiddleware"));

// 处理 api 的请求
// // app.get("/api/student", (req, res) => {
// //   // 获取学生
// // });
// // app.post("/api/student", (req, res) => {
// //   // 添加学生
// // });
// // app.put("/api/student/:id", (req, res) => {
// //   // 修改学生
// // });

// const studentRouter = express.Router();
// studentRouter.get("/", (req, res) => {
//   // 获取学生
// });
// studentRouter.post("/", (req, res) => {
//   // 添加学生
// });
// studentRouter.put("/:id", (req, res) => {
//   // 修改学生
// });
// app.use("/api/student", studentRouter);

app.use("/api/admin", require("./api/admin"));
app.use("/api/student", require("./api/student"));
app.use("/api/book", require("./api/book"));
// app.use("/api/class", require("./api/class"));
app.use("/api/upload", require("./api/upload"));
app.use("/download", require("./api/download"));

// 处理错误的中间件
app.use(require("./errorMiddleware"));

const port = 9527;
app.listen(port, () => {
  console.log(`server listen on ${port}`);
});
