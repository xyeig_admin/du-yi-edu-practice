module.exports = (req, res, next) => {
  if (req.path.startsWith("/api")) {
    // 说明请求的是 API 接口
    next();
  } else {
    // 说明需要的是静态资源
    if (true) {
      // 静态资源存在
      res.send("静态资源");
    } else {
      next();
    }
  }
};
