/**
 * 正常请求
 */
// fetch("http://localhost:5008/api/student")
//   .then((resp) => resp.json())
//   .then((resp) => {
//     console.log(resp);
//   });

/**
 * JSONP
 */
// function jsonp(url) {
//   const script = document.createElement("script");
//   script.src = url;
//   document.body.appendChild(script);
//   script.onload = function () {
//     script.remove();
//   };
// }
// function callback(data) {
//   console.log(data);
// }
// jsonp("http://localhost:9527/api/student");

/**
 * CORS简单请求
 */
// fetch("http://localhost:9527/api/student")
//   .then((resp) => resp.json())
//   .then((resp) => {
//     console.log(resp);
//   });

/**
 * CORS 预检请求
 */
// fetch("http://localhost:9527/api/student", {
//   method: "POST",
//   headers: {
//     "content-type": "application/json",
//     a: 1,
//   },
//   credentials: "include", // 附带身份凭证
// })
//   .then((resp) => resp.json())
//   .then((resp) => {
//     console.log(resp);
//   });

/**
 * Session 登录
 */
login.onclick = function () {
  fetch("/api/admin/login", {
    method: "POST",
    headers: {
      "content-type": "application/json",
    },
    body: JSON.stringify({
      loginId: "admin",
      loginPwd: "123456",
    }),
  })
    .then((resp) => resp.json())
    .then((resp) => {
      console.log(resp);
    });
};

updateStu.onclick = function () {
  fetch("/api/student/1201", {
    method: "PUT",
    headers: {
      "content-type": "application/json",
    },
    body: JSON.stringify({
      name: "123",
    }),
  })
    .then((resp) => resp.json())
    .then((resp) => {
      console.log(resp);
    });
};
