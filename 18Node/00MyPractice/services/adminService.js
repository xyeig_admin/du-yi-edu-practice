const Admin = require("../models/Admin");
const md5 = require("md5");

/**
 * 管理员初始化
 * TODO 判断数据库中是否有管理员，如果没有，自动添加一个默认管理员
 */

exports.addAdmin = async function (adminObj) {
  // TODO 应该判断adminObj的各种属性是否合理，以及帐号是否已存在，传入operatorId当前操作用户id，判断是否是管理员

  // 方式1
  // // 同步方法，构建一个模型实例
  // const ins = Admin.build({
  //   loginId: "abc",
  //   loginPwd: "123",
  // });
  // ins.loginId = "bcd";
  // // 异步方法，同步到数据库
  // ins.save().then(() => {
  //   console.log("新建管理员成功");
  // });

  // 方式2
  adminObj.loginPwd = md5(adminObj.loginPwd);
  const ins = await Admin.create(adminObj);

  // 将实例对象扁平化
  return ins.toJSON();
};

exports.deleteAdmin = async function (adminId) {
  // 方式1：有实例可以直接删除，没有实例则执行了两条语句
  // // 1.得到实例
  // const ins = await Admin.findByPk(adminId);
  // // 2.删除数据
  // ins && await ins.destroy();

  // 方式2：不需要实例，只执行一条语句
  return await Admin.destroy({
    where: {
      id: adminId,
    },
  });
};

exports.updateAdmin = async function (id, adminObj) {
  // 方式1：有实例可以直接修改，没有实例则执行了两条语句
  // // 1.得到实例
  // const ins = await Admin.findByPk(id);
  // // 2.修改数据
  // ins.loginId = adminObj.loginId;
  // // 3.保存
  // ins.save();

  // 方式2：不需要实例，只执行一条语句
  if (adminObj.loginPwd) {
    adminObj.loginPwd = md5(adminObj.loginPwd);
  }
  return await Admin.update(adminObj, {
    where: {
      id,
    },
  });
};

exports.login = async function (loginId, loginPwd) {
  loginPwd = md5(loginPwd);
  const result = await Admin.findOne({
    where: {
      loginId,
      loginPwd,
    },
  });
  if (result && result.loginId === loginId) {
    return result.toJSON();
  }
  return null;
};

exports.getAdminById = async function (id) {
  const result = await Admin.findByPk(id);
  if (result) {
    return result.toJSON();
  }
  return null;
};

exports.getAdmins = async function () {
  const result = await Admin.findAll();
  return JSON.parse(JSON.stringify(result));
};
