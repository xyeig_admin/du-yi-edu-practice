const axios = require("axios").default;
const cheerio = require("cheerio");
const Book = require("../models/Book");

/**
 * 获取豆瓣读书网页的源代码
 */
const getBooksHTML = async () => {
  const res = await axios.get(
    "https://book.douban.com/latest?subcat=%E5%85%A8%E9%83%A8&p=5"
  );
  return res.data;
};
// getBooksHTML().then((books) => console.log(books));

/**
 * 从豆瓣读书中得到一个完整的网页，并从网页中分析出书籍的基本信息
 * 获得书籍的详情页链接数组
 */
const getBooksLinks = async () => {
  const booksHTML = await getBooksHTML();
  // 获得cheerio操作对象
  const $ = cheerio.load(booksHTML);
  // 获得新书速递首页40本书籍的封面a元素
  const linkElements = $(".chart-dashed-list .media .media__img a");
  // 遍历a元素数组，取出所有详情页链接
  const links = linkElements.map((_, link) => link.attribs["href"]).get();
  return links;
};
// getBooksLinks().then((link) => console.log(link));

/**
 * 根据书籍详情页的地址，获得该书籍的详细信息
 * @param {String} detailUrl 详情页链接
 */
const getBookDetail = async (detailUrl) => {
  // 获得详情页的网页源代码和cheerio操作对象
  const res = await axios.get(detailUrl);
  const $ = cheerio.load(res.data);
  // 获取书籍名称
  const name = $("h1 span").text();
  // 获取书籍封面图片链接
  const imgUrl = $("#content .article #mainpic .nbg img").attr("src");
  // 获取书籍作者
  const spanElements = $("#content .article #info span.pl");
  const authorElement = spanElements.filter((_, el) =>
    $(el).text().includes("作者")
  );
  const author = authorElement.next("a").text();
  // 获取书籍出版年
  const publishElement = spanElements.filter((_, el) =>
    $(el).text().includes("出版年")
  );
  const publishDate = publishElement[0].nextSibling.nodeValue.trim();
  return {
    name,
    imgUrl,
    author,
    publishDate,
  };
};
// getBookDetail("https://book.douban.com/subject/36407254/").then((detail) =>
//   console.log(detail)
// );

/**
 * 获得所有的书籍信息
 */
const getBooksInfo = async () => {
  const booksLinks = await getBooksLinks();
  const promises = booksLinks.map((link) => getBookDetail(link));
  return Promise.all(promises);
};
// getBooksInfo().then((all) => console.log(all));

/**
 * 获得书籍信息，然后保存到数据库
 */
const setBooksInfoToDB = async () => {
  const booksInfo = await getBooksInfo();
  await Book.bulkCreate(booksInfo);
  console.log("书籍信息爬取成功，已保存到数据库");
};

(async () => {
  await setBooksInfoToDB();
})();
