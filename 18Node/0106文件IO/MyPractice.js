const path = require("path");
const fs = require("fs");

class File {
  /**
   * 构造函数
   * @param {String} filename 绝对路径
   * @param {String} basename 文件名
   * @param {String} ext 后缀名，如果是目录则为空字符串
   * @param {Boolean} isFile 是否是一个文件
   * @param {Number} size 文件大小
   * @param {Date} createTime 日期对象，创建的时间
   * @param {Date} updateTime 日期对象，修改的时间
   */
  constructor(filename, basename, ext, isFile, size, createTime, updateTime) {
    this.filename = filename;
    this.basename = basename;
    this.ext = ext;
    this.isFile = isFile;
    this.size = size;
    this.createTime = createTime;
    this.updateTime = updateTime;
  }

  /**
   * 生成 File 对象
   * @param {String} filename 文件完整路径
   * @returns {File} 生成文件对象
   */
  static generateFile = async (filename) => {
    const stat = await fs.promises.stat(filename);
    return new File(
      filename,
      path.basename(filename),
      path.extname(filename),
      stat.isFile(),
      stat.size,
      new Date(stat.birthtime),
      new Date(stat.mtime)
    );
  };

  /**
   * 读取文件内容
   * @param {Boolean} isBuffer 是否以Buffer格式输出
   * @returns 文件内容，如果是目录则返回null
   */
  getContent = async (isBuffer = false) => {
    if (!this.isFile) return null;
    return await fs.promises.readFile(
      this.filename,
      isBuffer ? "utf-8" : undefined
    );
  };

  /**
   * 获得当前目录下的所有子文件对象
   * @returns 子文件对象，如果是文件则返回空数组
   */
  getChildren = async () => {
    if (this.isFile) return [];
    // 获取所有子文件的文件名
    const children = await fs.promises.readdir(this.filename);
    // 遍历子文件名数组，生成文件对象
    const promises = children.map((subBasename) => {
      // 拼接当前目录完整路径和子文件对象名，得到子文件的完整路径
      const subFilename = path.resolve(this.filename, subBasename);
      // 生成文件对象（Promise）
      return File.generateFile(subFilename);
    });
    return Promise.all(promises);
  };
}

/**
 * 读取目录中的所有子目录和子文件
 * @param {String} dirname 目录名
 */
const readDir = async (dirname) => {
  const file = await File.generateFile(dirname);
  return file.isFile ? file : await file.getChildren();
};

const main = async () => {
  const dirname1 = path.resolve(__dirname, "./myfiles/1");
  const res1 = await readDir(dirname1);
  console.log(res1[0]); // File {}
  const content1 = await res1[0].getContent();
  console.log(content1); // <Buffer 61 73 73 61 66 61 73 64 66>

  const dirname2 = path.resolve(__dirname, "./myfiles/1.txt");
  const res2 = await readDir(dirname2);
  console.log(res2); // File {}
  const content2 = await res2.getContent(true);
  console.log(content2); // abc阿斯顿发发放到发
};
main();
