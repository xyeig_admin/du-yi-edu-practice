const fs = require("fs");
const path = require("path");

/**
 * 拷贝文件方式1
 * 直接将文件内容读到内存中，再将内存中的内容写入文件
 * 会产生背压问题
 */
const function1 = async () => {
  const origin = path.resolve(__dirname, "./temp/abc.txt");
  const target = path.resolve(__dirname, "./temp/abc1.txt");
  console.time("first");

  const content = await fs.promises.readFile(origin);
  await fs.promises.writeFile(target, content);

  console.timeEnd("first");
  console.log("方式1复制成功");
};

/**
 * 拷贝文件方式2
 * 读取文件内容到内存中直到通道占满时暂停，等清空后继续读取，边读取边写入文件
 * 解决背压问题
 */
const function2 = async () => {
  const origin = path.resolve(__dirname, "./temp/abc.txt");
  const target = path.resolve(__dirname, "./temp/abc2.txt");
  console.time("second");

  const rs = fs.createReadStream(origin);
  const ws = fs.createWriteStream(target);
  rs.on("data", (chunk) => {
    const flag = ws.write(chunk);
    if (!flag) rs.pause(); // 下一次写入就会造成背压，暂停读取
  });
  ws.on("drain", () => {
    rs.resume(); // 继续读取
  });
  rs.on("close", () => {
    // 读完了
    ws.end(); // 关闭写入流
    console.timeEnd("second");
    console.log("方式2复制成功");
  });
};

function1();
function2();
/**
 * second: 14.531ms
 * 方式2复制成功
 * first: 16.038ms
 * 方式1复制成功
 */
