const net = require("net");

// 建立连接通道【内部完成了三次握手和四次挥手】
const socket = net.createConnection(
  {
    host: "www.baidu.com",
    port: 80,
  },
  () => {
    console.log("连接成功");
  }
);

// 向服务器发送请求【请求头中标明请求的方法】
socket.write(`GET / HTTP/1.1
Host: www.baidu.com
Connection: keep-alive

`);

let receive = null; // 是否接收到服务器的消息

/**
 * 提炼出响应字符串中的响应头和响应体
 * @param {String} response 响应字符串
 */
const parseResponse = (response) => {
  const index = response.indexOf("\r\n\r\n");
  const head = response.substring(0, index);
  const body = response.substring(index + 2); // 响应头后面有两个空行
  const headers = head.split("\r\n");

  // 去掉响应行，将响应头按照":"分割，并且去除首尾空格符
  const headerArray = headers
    .slice(1)
    .map((str) => str.split(":").map((s) => s.trim()));

  // 将响应头字符串组装成对象
  const header = headerArray.reduce((pre, cur) => {
    pre[cur[0]] = cur[1];
    return pre;
  }, {});

  // 将响应体组装成对象
  const bodyObject = body.trimStart();

  return {
    header,
    body: bodyObject,
  };
};

/**
 * 根据 Content-Length 判断是否接收完毕所有字节数
 */
const isReceiveOver = () => {
  const contentLength = +receive.header["Content-Length"]; // 需要接收的响应体总字节数
  const currentLength = Buffer.from(receive.body, "utf-8").byteLength;
  return currentLength > contentLength;
};

// 读取到的数据是二进制字符串
socket.on("data", (chunk) => {
  // 直接调用end可能导致服务器消息还未接收完毕就关闭了连接
  // console.log("来自服务器的消息", chunk.toString("utf-8"));
  // socket.end();

  const response = chunk.toString("utf-8");
  if (!receive) {
    // 第一次接收
    receive = parseResponse(response);
    isReceiveOver() && socket.end(); // 一次就接收完毕
    return;
  }
  receive.body += response;
  if (isReceiveOver()) {
    socket.end();
    return;
  }
});

// 需要客户端调用end关闭连接，服务器才会结束
socket.on("close", () => {
  console.log("结束了", receive.body);
});
