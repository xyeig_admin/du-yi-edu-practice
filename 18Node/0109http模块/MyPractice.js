const http = require("http");
const path = require("path");
const URL = require("url");
const fs = require("fs/promises");

/**
 * 获得文件状态
 * @param {String} filename 文件路径
 * @returns {Stats} 文件信息状态对象，如果函数出错则返回null
 */
const getFileStat = async (filename) => {
  try {
    return await fs.stat(filename);
  } catch {
    return null;
  }
};

/**
 * 获得文件信息
 * @param {String} url 请求的路径
 * @returns {String} 返回文件内容，文件不存在则返回null
 */
const getFile = async (url) => {
  // urlObj.path 包含 query 参数，应该使用 pathname
  const urlObj = URL.parse(url);

  let filename = path.resolve(
    __dirname,
    "public",
    urlObj.pathname.substring(1) // 去除开头的/才不会使最终路径拼接成盘符根目录
  );
  let stat = await getFileStat(filename);

  // 访问的目录/文件不存在
  if (!stat) return null;

  // 访问的是目录，拼接 index.html
  if (stat.isDirectory()) {
    filename = path.resolve(
      __dirname,
      "public",
      urlObj.pathname.substring(1),
      "index.html"
    );
    stat = await getFileStat(filename);
    if (!stat) return null;
    return await fs.readFile(filename);
  }

  // 访问的文件存在
  return await fs.readFile(filename);
};

/**
 * 处理请求
 * @param {http.IncomingMessage} req 请求对象
 * @param {http.ServerResponse} res 响应对象
 */
const handleRequest = async (req, res) => {
  const file = await getFile(req.url);
  if (!file) {
    res.statusCode = 404;
    res.write("Resource is not exist");
  } else {
    res.write(file);
  }
  res.end();
};

const server = http.createServer(handleRequest);
server.listen(7000);
server.on("listening", () => {
  console.log("Server listening on port 7000");
});
