const socketIO = require("socket.io");

// 用户数组，模拟数据库
let users = [];

module.exports = function (server) {
  const io = socketIO(server);

  io.on("connection", (socket) => {
    // 当前用户名
    let curUser = "";

    // 监听客户端消息
    socket.on("login", (data) => {
      if (
        data === "所有人" ||
        users.filter((u) => u.username === data).length > 0
      ) {
        // 昵称不可用
        socket.emit("login", false);
      } else {
        // 昵称可用
        users.push({
          username: data,
          socket,
        });
        curUser = data;
        socket.emit("login", true);
        // 新用户进入了
        socket.broadcast.emit("userin", data);
      }
    });

    // 返回当前聊天室所有成员
    socket.on("users", () => {
      const arr = users.map((u) => u.username);
      socket.emit("users", arr);
    });

    // 发送消息
    socket.on("msg", (data) => {
      if (data.to) {
        // 发送给指定的用户
        const us = users.filter((u) => u.username === data.to);
        const u = us[0];
        u.socket.emit("new msg", {
          from: curUser,
          content: data.content,
          to: data.to,
        });
      } else {
        // 发送给所有人
        socket.broadcast.emit("new msg", {
          from: curUser,
          content: data.content,
          to: data.to,
        });
      }
    });

    // 客户端断开（用户离开）
    socket.on("disconnect", () => {
      socket.broadcast.emit("userout", curUser);
      users = users.filter((u) => u.username !== curUser);
    });
  });
};
