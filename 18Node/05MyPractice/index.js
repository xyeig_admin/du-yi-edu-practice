/**
 * WebSocket 原理
 */
// const net = require("net");

// const server = net.createServer((socket) => {
//   console.log("收到客户端的连接");

//   // 建立连接后立马发送消息，所以只接收一次即可
//   socket.once("data", (chunk) => {
//     const httpContent = chunk.toString("utf-8");

//     // 将请求头分割为对象格式
//     let parts = httpContent.split("\r\n");
//     parts.shift(); // 去掉首行GET / /HTTP1.1
//     parts = parts
//       .filter((s) => s)
//       .map((s) => {
//         const i = s.indexOf(":");
//         return [s.substr(0, i), s.substr(i + 1).trim()];
//       });
//     const headers = Object.fromEntries(parts);

//     // 获取浏览器随机生成的key
//     const crypto = require("crypto");
//     const hash = crypto.createHash("sha1");
//     hash.update(
//       headers["Sec-WebSocket-Key"] + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
//     );
//     const key = hash.digest("base64");

//     // 服务器响应请求，101切换协议
//     socket.write(`HTTP/1.1 101 Switching Protocols
// Upgrade: websocket
// Connection: Upgrade
// Sec-WebSocket-Accept: ${key}

// `);

//     // 接收客户端后续的请求数据
//     socket.on("data", (chunk) => {
//       console.log(chunk);
//     });
//   });
// });

// server.listen(5008);

/**
 * socket.io
 */
// const express = require("express");
// const socketIO = require("socket.io");
// const http = require("http");
// const path = require("path");

// // 搭建express服务器
// const app = express();
// const server = http.createServer(app);
// app.use(express.static(path.resolve(__dirname, "public")));

// // 提供websocket服务
// const io = socketIO(server);

// // 当有一个新的客户端连接到服务器，连接成功后触发
// io.on("connection", (socket) => {
//   console.log("新的客户端连接进来了");

//   // 监听客户端的msg消息
//   socket.on("msg", (chunk) => {
//     console.log(chunk.toString("utf-8"));
//   });

//   // 每隔两秒钟，发送一个消息给客户端，事件名为test
//   const timer = setInterval(function () {
//     socket.emit("test", "test message from server");
//   }, 2000);

//   // 连接断开后触发
//   socket.on("disconnect", () => {
//     clearInterval(timer);
//     console.log("closed");
//   });
// });

// // 监听端口
// server.listen(5008, () => {
//   console.log("server listening on 5008");
// });

/**
 * 在线聊天室
 */
// const express = require("express");
// const http = require("http");
// const path = require("path");

// // express
// const app = express();
// const server = http.createServer(app);
// app.use(express.static(path.resolve(__dirname, "public")));

// // websocket
// require("./chatServer")(server);

// // 监听端口
// server.listen(5008, () => {
//   console.log("server listening on 5008");
// });

/**
 * XSS 攻击和防御
 */
const express = require("express");
const http = require("http");
const path = require("path");
const xss = require("xss");
// express
const app = express();
const server = http.createServer(app);
app.use(express.static(path.resolve(__dirname, "public")));

app.use(express.json());
app.use(express.urlencoded());
const myxss = new xss.FilterXSS({
  onTagAttr(tag, name, value, isWhiteAttr) {
    if (name === "style") {
      return `style="${value}"`;
    }
  },
});
app.use((req, res, next) => {
  for (const key in req.body) {
    const value = req.body[key];
    req.body[key] = myxss.process(value);
  }
  next();
});

const articles = [];
app.post("/api/article/add", (req, res) => {
  articles.push(req.body.content);
  console.log(articles);
  res.send({
    code: 0,
    msg: "",
    data: "ok",
  });
});

app.get("/articles", (req, res) => {
  res.render("articles.ejs", {
    articles,
    redirect: req.query.redirect,
  });
});

// 监听端口
server.listen(5008, () => {
  console.log("server listening on 5008");
});

