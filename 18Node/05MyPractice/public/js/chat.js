const socket = io.connect();

/**
 * 客户端发送消息给服务器
 */
// 进入聊天室
page.onLogin = function (username) {
  socket.emit("login", username);
};
// 监听发送消息
page.onSendMsg = function (me, msg, to) {
  socket.emit("msg", {
    to,
    content: msg,
  });
  page.addMsg(me, msg, to);
  page.clearInput();
};

/**
 * 客户端监听服务器消息
 */
socket.on("login", (result) => {
  if (result) {
    page.intoChatRoom();
    socket.emit("users", "");
  } else {
    alert("昵称不可用，请更换昵称");
  }
});
// 渲染成员列表
socket.on("users", (users) => {
  page.initChatRoom();
  for (const u of users) {
    page.addUser(u);
  }
});
// 用户进入
socket.on("userin", (username) => {
  page.addUser(username);
});
// 用户离开
socket.on("userout", (username) => {
  page.removeUser(username);
});
// 收到新消息
socket.on("new msg", (result) => {
  page.addMsg(result.from, result.content, result.to);
});
