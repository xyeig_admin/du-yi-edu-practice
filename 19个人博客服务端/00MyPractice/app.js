// 引包
var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const expressJWT = require("express-jwt");
const md5 = require("md5");
const {
  ForbiddenError,
  ServiceError,
  UnknownError,
} = require("./utils/errors");
const session = require("express-session");

// 默认读取项目根目录下的 .env 环境变量文件
require("dotenv").config();
// 捕获异步错误
require("express-async-errors");
// 引入数据库
// require("./dao/dbConnect");
require("./dao/db");

// 创建服务器实例
var app = express();

// 使用session
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
  })
);

// 使用中间件
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// 引入路由
const adminRouter = require("./routes/admin");
const captchaRouter = require("./routes/captcha");
const bannerRouter = require("./routes/banner");
const uploadRouter = require("./routes/upload");
const blogTypeRouter = require("./routes/blogType");
const blogRouter = require("./routes/blog");
const projectRouter = require("./routes/project");
const messageRouter = require("./routes/message");
const aboutRouter = require("./routes/about");
const settingRouter = require("./routes/setting");
// 配置验证token接口
app.use(
  expressJWT({
    secret: md5(process.env.JWT_SECRET),
    algorithms: ["HS256"], // 新版本express-jwt要求必须指定算法
  }).unless({
    // 需要排除token验证的路由
    path: [
      { url: "/api/admin/login", methods: ["POST"] },
      { url: "/res/captcha", methods: ["GET"] },
      { url: "/api/banner", methods: ["GET"] },
      { url: "/api/blogtype", methods: ["GET"] },
      { url: "/api/blog", methods: ["GET"] },
      { url: /\/api\/blog\/\d/, methods: ["GET"] },
      { url: "/api/project", methods: ["GET"] },
      { url: "/api/message", methods: ["GET", "POST"] },
      { url: "/api/comment", methods: ["GET", "POST"] },
      { url: "/api/about", methods: ["GET"] },
      { url: "/api/setting", methods: ["GET"] },
    ],
  })
);
// 使用路由中间件
app.use("/api/admin", adminRouter);
app.use("/res/captcha", captchaRouter);
app.use("/api/banner", bannerRouter);
app.use("/api/upload", uploadRouter);
app.use("/api/blogtype", blogTypeRouter);
app.use("/api/blog", blogRouter);
app.use("/api/project", projectRouter);
app.use("/api/message", messageRouter);
app.use("/api/comment", messageRouter);
app.use("/api/about", aboutRouter);
app.use("/api/setting", settingRouter);

// 错误处理
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // console.log({
  //   error: err.name,
  //   msg: err.message,
  // });
  if (err.name === "UnauthorizedError") {
    // token验证错误
    res.send(new ForbiddenError("未登录，或者登录凭证已过期").toResponseJSON());
  } else if (err instanceof ServiceError) {
    res.send(err.toResponseJSON());
  } else {
    res.send(new UnknownError().toResponseJSON());
  }
});

module.exports = app;
