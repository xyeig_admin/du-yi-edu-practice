const aboutModel = require("./model/aboutModel");

// 查询
module.exports.findAboutDao = async () => {
  return await aboutModel.findOne();
};

// 修改
module.exports.updateAboutDao = async (newUrl) => {
  const data = await aboutModel.findOne();
  data.url = newUrl;
  await data.save();
  return data.dataValues;
};
