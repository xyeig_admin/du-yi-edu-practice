/**
 * 负责和数据库打交道
 */
const adminModel = require("./model/adminModel");

// 登录
module.exports.loginDao = async (loginInfo) => {
  return await adminModel.findOne({
    where: {
      loginId: loginInfo.loginId,
      loginPwd: loginInfo.loginPwd,
    },
  });
};

// 更新
module.exports.updateAdminDao = async (newInfo) => {
  return await adminModel.update(newInfo, {
    where: {
      loginId: newInfo.loginId,
    },
  });
};
