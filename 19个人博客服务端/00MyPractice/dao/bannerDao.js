/**
 * 负责和数据库打交道
 */
const bannerModel = require("./model/bannerModel");

module.exports.getBannerDao = async () => {
  return await bannerModel.findAll();
};

// 更新
module.exports.updateBannerDao = async (bannerArray) => {
  // 删除表数据
  await bannerModel.destroy({
    truncate: true,
  });
  await bannerModel.bulkCreate(bannerArray);
  return await bannerModel.findAll();
};
