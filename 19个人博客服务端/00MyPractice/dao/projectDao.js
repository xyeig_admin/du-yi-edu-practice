const projectModel = require("./model/projectModel");

// 新增项目
module.exports.addProjectDao = async (newProjectInfo) => {
  const { dataValues } = await projectModel.create(newProjectInfo);
  return dataValues;
};

// 查询所有的项目
module.exports.findAllProjectDao = async () => {
  return await projectModel.findAll();
};

// 修改项目
module.exports.updateProjectDao = async (id, newProjectInfo) => {
  await projectModel.update(newProjectInfo, {
    where: {
      id,
    },
  });
  return await projectModel.findByPk(id);
};

// 删除项目
module.exports.deleteProjectDao = async (id) => {
  return await projectModel.destroy({
    where: {
      id,
    },
  });
};
