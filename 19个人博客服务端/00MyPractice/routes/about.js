const express = require("express");
const router = express.Router();

const {
  findAboutService,
  updateAboutService,
} = require("../service/aboutService");

router.get("/", async (req, res, next) => {
  res.send(await findAboutService());
});

router.post("/", async (req, res, next) => {
  res.send(await updateAboutService(req.body.url));
});

module.exports = router;
