const express = require("express");
const router = express.Router();

const { loginService, updateAdminService } = require("../service/adminService");
const { ValidationError } = require("../utils/errors");
const { formatResponse, analysisToken } = require("../utils/tool");

// 管理员登录
router.post("/login", async (req, res, next) => {
  // 请求时会拼接 app.js 中的一级路由 —— /api/admin/login

  // 校验验证码
  if (req.body.captcha.toLowerCase() !== req.session.captcha.toLowerCase()) {
    throw new ValidationError("验证码错误");
  }

  const result = await loginService(req.body);
  if (result.token) {
    res.setHeader("authentication", result.token);
  }
  res.send(formatResponse(0, "", result.data));
});

// 恢复登录
router.get("/whoami", async (req, res, next) => {
  // 解析token
  const token = analysisToken(req.get("Authorization"));
  res.send(
    formatResponse(0, "", {
      id: token.id,
      loginId: token.loginId,
      name: token.name,
    })
  );
});

// 更新登录信息
router.put("/", async (req, res, next) => {
  res.send(await updateAdminService(req.body));
});

module.exports = router;
