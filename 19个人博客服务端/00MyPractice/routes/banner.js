const express = require("express");
const router = express.Router();

const { getBannerService, updateBannerService } = require("../service/bannerService");

// 获取首页标语
router.get("/", async (req, res, next) => {
  res.send(await getBannerService());
});

// 设置首页标语
router.post("/", async (req, res, next) => {
  res.send(await updateBannerService(req.body))
});

module.exports = router;
