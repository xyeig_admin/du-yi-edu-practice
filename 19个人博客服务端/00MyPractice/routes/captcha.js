const express = require("express");
const router = express.Router();

const { getCaptchaService } = require("../service/captchaService");

// 获取验证码
router.get("/", async (req, res, next) => {
  // 生成验证码
  const captcha = await getCaptchaService();
  // 保存到session
  req.session.captcha = captcha.text;
  // 设置响应头
  res.setHeader("Content-Type", "images/svg+xml");
  res.send(captcha.data);
});

module.exports = router;
