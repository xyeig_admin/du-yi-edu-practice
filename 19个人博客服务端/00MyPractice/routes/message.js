const express = require("express");
const router = express.Router();

const {
  addMessageService,
  findMessageByPageService,
  deleteMessageService,
} = require("../service/messageService");

// 添加留言或者评论
router.post("/", async (req, res, next) => {
  res.send(await addMessageService(req.body));
});

// 获取留言或者评论
router.get("/", async (req, res, next) => {
  res.send(await findMessageByPageService(req.query));
});

// 删除留言或者评论
router.delete("/:id", async (req, res, next) => {
  res.send(await deleteMessageService(req.params.id));
});

module.exports = router;
