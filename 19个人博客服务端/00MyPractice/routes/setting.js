const express = require("express");
const router = express.Router();

const {
  findSettingService,
  updateSettingService,
} = require("../service/settingService");

// 获取全局设置
router.get("/", async (req, res, next) => {
  res.send(await findSettingService());
});

// 更新全局设置
router.put("/", async (req, res, next) => {
  res.send(await updateSettingService(req.body));
});

module.exports = router;
