const express = require("express");
const router = express.Router();

const multer = require("multer");
const { UploadError } = require("../utils/errors");
const { uploading, formatResponse } = require("../utils/tool");

router.post("/", async (req, res, next) => {
  // 参数是前端上传控件的name值
  const upload = uploading.single("file");
  upload(req, res, (err) => {
    if (err instanceof multer.MulterError) {
      next(new UploadError("上传文件失败，请检查文件大小，控制在2MB内"));
    }
    res.send(formatResponse(0, "", "/static/uploads/" + req.file.filename));
  });
});

module.exports = router;
