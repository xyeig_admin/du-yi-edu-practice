const { findAboutDao, updateAboutDao } = require("../dao/aboutDao");
const { formatResponse } = require("../utils/tool");

// 查询关于我
module.exports.findAboutService = async () => {
  const { url } = await findAboutDao();
  return formatResponse(0, "", url);
};

// 修改关于我
module.exports.updateAboutService = async (newUrl) => {
  const { url } = await updateAboutDao(newUrl);
  return formatResponse(0, "", url);
};
