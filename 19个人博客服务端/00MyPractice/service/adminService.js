/**
 * admin 模块业务逻辑
 */
const md5 = require("md5");
const jwt = require("jsonwebtoken");

const { loginDao, updateAdminDao } = require("../dao/adminDao");
const { ValidationError } = require("../utils/errors");
const { formatResponse } = require("../utils/tool");

// 登录
module.exports.loginService = async (loginInfo) => {
  loginInfo.loginPwd = md5(loginInfo.loginPwd);

  // 数据验证 —— 查询数据库
  let data = await loginDao(loginInfo);
  if (data && data.dataValues) {
    // 添加token
    data = {
      id: data.dataValues.id,
      loginId: data.dataValues.loginId,
      name: data.dataValues.name,
    };
    // 是否登录七天
    const loginPeriod = loginInfo.remember ? +loginInfo.remember : 1;
    // 生成token
    const token = jwt.sign(data, md5(process.env.JWT_SECRET), {
      expiresIn: 24 * 60 * 60 * loginPeriod,
    });
    return {
      token,
      data,
    };
  }

  return {
    data,
  };
};

// 更新
module.exports.updateAdminService = async (accountInfo) => {
  // 根据传入的账号信息查询管理员
  const adminInfo = await loginDao({
    loginId: accountInfo.loginId,
    loginPwd: md5(accountInfo.oldLoginPwd),
  });
  if (adminInfo && adminInfo.dataValues) {
    // 修改密码
    const result = await updateAdminDao({
      loginId: accountInfo.loginId,
      loginPwd: md5(accountInfo.loginPwd),
      name: accountInfo.name,
    });
    return formatResponse(0, "", {
      id: adminInfo.dataValues.id,
      loginId: accountInfo.loginId,
      name: accountInfo.name,
    });
  } else {
    // 密码不正确
    throw new ValidationError("旧密码不正确");
  }
};
