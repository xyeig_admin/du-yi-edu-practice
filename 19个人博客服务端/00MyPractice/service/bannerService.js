/**
 * 业务逻辑
 */

const { getBannerDao, updateBannerDao } = require("../dao/bannerDao");
const { handleResponseArray, formatResponse } = require("../utils/tool");

// 获取标语
module.exports.getBannerService = async () => {
  return formatResponse(0, "", handleResponseArray(await getBannerDao()));
};

// 更新
module.exports.updateBannerService = async (bannerArray) => {
  return formatResponse(0, '', handleResponseArray(await updateBannerDao(bannerArray)));
};
