const { validate } = require("validate.js");
const { blogCountByBlogType } = require("../dao/blogDao");
const {
  addBlogTypeDao,
  findAllBlogTypeDao,
  findOneBlogTypeDao,
  deleteBlogTypeDao,
  updateBlogTypeDao,
} = require("../dao/blogTypeDao");
const { ValidationError } = require("../utils/errors");
const { formatResponse, handleResponseArray } = require("../utils/tool");

// 新增博客分类
module.exports.addBlogTypeService = async (newBlogTypeInfo) => {
  // 数据验证规则
  const blogTypeRule = {
    name: {
      presence: {
        allowEmpty: false,
      },
      type: "string",
    },
    order: {
      presence: {
        allowEmpty: false,
      },
      type: "string",
    },
  };
  // 进行数据验证
  const validateResult = validate.validate(newBlogTypeInfo, blogTypeRule);
  if (!validateResult) {
    // 验证通过

    // 因为是新增的文章分类，所以一开始文章数量为 0
    newBlogTypeInfo.articleCount = 0;
    const data = await addBlogTypeDao(newBlogTypeInfo);
    return formatResponse(0, "", data);
  } else {
    // 数据验证失败
    throw new ValidationError("数据验证失败");
  }
};

// 查询所有博客分类
module.exports.findAllBlogTypeService = async () => {
  const data = await findAllBlogTypeDao();
  const obj = formatResponse(0, "", handleResponseArray(data));
  obj.data.sort((a, b) => a.order - b.order);
  return obj;
};

// 获取其中一个博客分类
module.exports.findOneBlogTypeService = async (id) => {
  const { dataValues } = await findOneBlogTypeDao(id);
  return formatResponse(0, "", dataValues);
};

// 修改其中一个博客分类
module.exports.updateBlogTypeService = async (id, blogInfo) => {
  const data = await updateBlogTypeDao(id, blogInfo);
  return formatResponse(0, "", data);
};

// 删除其中一个博客分类
module.exports.deleteBlogTypeService = async (id) => {
  const count = await blogCountByBlogType(id);
  await deleteBlogTypeDao(id);
  // 返回受影响的文章的数量
  return formatResponse(0, "", count);
};
