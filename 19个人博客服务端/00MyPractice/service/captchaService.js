/**
 * 业务逻辑
 */

const svgCaptcha = require("svg-captcha");

// 生成验证码
module.exports.getCaptchaService = async () => {
  return svgCaptcha.create({
    size: 4,
    ignoreChars: "iIl10Oo",
    noise: 6,
    color: true,
  });
};
