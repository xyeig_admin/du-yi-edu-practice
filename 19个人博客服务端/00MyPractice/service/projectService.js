const { validate } = require("validate.js");
const {
  findAllProjectDao,
  addProjectDao,
  updateProjectDao,
  deleteProjectDao,
} = require("../dao/projectDao");
const { ValidationError } = require("../utils/errors");
const { formatResponse, handleResponseArray } = require("../utils/tool");

// 新增项目
module.exports.addProjectService = async (newProjectInfo) => {
  // 将 description 项目描述转换为字符串
  newProjectInfo.description = JSON.stringify(newProjectInfo.description);

  // 定义验证规则
  const projectRule = {
    name: {
      presence: {
        allowEmpty: false,
      },
      type: "string",
    },
    url: {
      presence: {
        allowEmpty: false,
      },
      type: "string",
    },
    github: {
      presence: {
        allowEmpty: false,
      },
      type: "string",
    },
    description: {
      presence: {
        allowEmpty: false,
      },
      type: "string",
    },
    order: {
      presence: {
        allowEmpty: false,
      },
      type: "integer",
    },
    thumb: {
      type: "string",
    },
  };

  // 进行数据验证
  const validateResult = validate.validate(newProjectInfo, projectRule);
  if (!validateResult) {
    const data = await addProjectDao(newProjectInfo);
    return formatResponse(0, "", [data]);
  } else {
    throw new ValidationError("数据验证失败");
  }
};

// 查询所有项目
module.exports.findAllProjectService = async () => {
  const data = await findAllProjectDao();
  const obj = handleResponseArray(data);
  // 将项目描述还原成数组
  obj.forEach((item) => {
    item.description = JSON.parse(item.description);
  });
  return formatResponse(0, "", obj);
};

// 修改项目
module.exports.updateProjectService = async (id, newProjectInfo) => {
  if (newProjectInfo.description) {
    newProjectInfo.description = JSON.stringify(newProjectInfo.description);
  }
  const { dataValues } = await updateProjectDao(id, newProjectInfo);
  // 还原项目描述
  dataValues.description = JSON.parse(dataValues.description);
  return formatResponse(0, "", [dataValues]);
};

// 删除项目
module.exports.deleteProjectService = async (id) => {
  await deleteProjectDao(id);
  return formatResponse(0, "", true);
};
