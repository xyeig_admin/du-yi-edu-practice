/**
 * 自定义错误
 * 当错误发生时，捕获错误，抛出自定义错误
 */

const { formatResponse } = require("./tool");

/**
 * 业务处理错误基类
 */
class ServiceError extends Error {
  /**
   * 构造函数
   * @param {String} message 错误消息
   * @param {Number} code 错误消息码
   */
  constructor(message, code) {
    super(message);
    this.code = code;
  }

  /**
   * 以JSON格式将错误信息响应给客户端
   */
  toResponseJSON() {
    return formatResponse(this.code, this.message, null);
  }
}

/**
 * 文件上传错误
 */
exports.UploadError = class extends ServiceError {
  constructor(message) {
    super(message, 413);
  }
};

/**
 * 禁止访问错误
 */
exports.ForbiddenError = class extends ServiceError {
  constructor(message) {
    super(message, 401);
  }
};

/**
 * 验证错误
 */
exports.ValidationError = class extends ServiceError {
  constructor(message) {
    super(message, 406);
  }
};

/**
 * 无资源错误
 */
exports.NotfoundError = class extends ServiceError {
  constructor() {
    super("Not found", 406);
  }
};

/**
 * 未知错误
 */
exports.UnknownError = class extends ServiceError {
  constructor() {
    super("Server internal error", 500);
  }
};

module.exports.ServiceError = ServiceError;
