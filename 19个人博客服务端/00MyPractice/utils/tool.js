/**
 * 格式化响应信息
 * {
 *    "code": code,
 *    "msg": msg,
 *    "data": data
 * }
 */
module.exports.formatResponse = (code, msg, data) => {
  return {
    code,
    msg,
    data,
  };
};

// 解析token
const jwt = require("jsonwebtoken");
const md5 = require("md5");
module.exports.analysisToken = (token) => {
  return jwt.verify(token.split(" ")[1], md5(process.env.JWT_SECRET));
};

// 处理数组类型的响应数据
module.exports.handleResponseArray = (data) => {
  const arr = [];
  for (const i of data) {
    arr.push(i.dataValues);
  }
  return arr;
};

// 上传
const multer = require("multer");
const path = require("path");
const storage = multer.diskStorage({
  // 文件存储位置
  destination: function (req, file, cb) {
    cb(null, __dirname + "/../public/static/uploads");
  },
  // 处理上传文件的文件名
  filename: function (req, file, cb) {
    // 获取后缀名
    const extname = path.extname(file.originalname);
    // 获取文件名
    const basename = path.basename(file.originalname, extname);
    const newName =
      basename +
      new Date().getTime() +
      Math.floor(Math.random() * 9000 + 1000) +
      extname;
    cb(null, newName);
  },
});
module.exports.uploading = multer({
  storage,
  limits: {
    fileSize: 2 * 1024 * 1024,
    files: 1,
  },
});

// 处理TOC
const toc = require("markdown-toc");
module.exports.handleTOC = (info) => {
  let result = toc(info.markdownContent).json;

  // 经过上面 toc 方法的处理，就将整个 markdown 里面的标题全部提取出来了
  // 形成一个数组，数组里面是一个个对象，每个对象记录了标题的名称以及等级，如下：

  // [
  //     { content: '数值类型概述', slug: '数值类型概述', lvl: 2, i: 0, seen: 0 },
  //     { content: '整数和浮点数', slug: '整数和浮点数', lvl: 3, i: 1, seen: 0 },
  //     { content: '数值精度', slug: '数值精度', lvl: 4, i: 2, seen: 0 },
  //     { content: '数值范围', slug: '数值范围', lvl: 3, i: 3, seen: 0 },
  //     { content: '数值的表示方法', slug: '数值的表示方法', lvl: 2, i: 4, seen: 0 }
  // ]

  // 但是这不是我们想要的格式，我们想要转换为
  // [
  //     { "name": "章节1", "anchor": "title-1" },
  //     { "name": "章节2", "anchor": "title-2",
  //         "children": [
  //             { "name": "章节2-1", "anchor": "title-2-1" },
  //             { "name": "章节2-2", "anchor": "title-2-2" },
  //         ]
  //     }
  // ]

  // 转换格式
  const transfer = (flatArr) => {
    const stack = [],
      result = [];

    // 创建toc对象
    const createTOCItem = (item) => ({
      name: item.content,
      author: item.slug,
      level: item.lvl,
      children: [],
    });

    // 处理toc子级对象
    const handleTOCItem = (item) => {
      // stack为空返回undefined，stack不为空则返回最后一个元素【栈顶】
      const top = stack[stack.length - 1];
      if (!top) {
        stack.push(item);
      } else if (item.level > top.level) {
        // toc等级比栈顶（上一个toc对象）大，应该成为上一个toc对象的子级
        top.children.push(item);
        stack.push(item);
      } else {
        stack.pop();
        handleTOCItem(item);
      }
    };

    // 标题最小级别
    let min = 6;
    // 寻找最小级别的标题
    for (const i of flatArr) {
      if (i.lvl < min) min = i.lvl;
    }

    for (const item of flatArr) {
      const tocItem = createTOCItem(item);
      if (tocItem.level === min) {
        // 当前目录不会是children
        result.push(tocItem);
      }
      // 有可能是其他目录的子级
      handleTOCItem(tocItem);
    }

    return result;
  };

  info.toc = transfer(result);

  delete info.markdownContent;

  // 为各个级别的标题添加id
  for (const i of result) {
    let newStr = "";
    switch (i.lvl) {
      case 1:
        newStr = `<h1 id="${i.slug}">`;
        info.htmlContent = info.htmlContent.replace("<h1>", newStr);
        break;
      case 2:
        newStr = `<h2 id="${i.slug}">`;
        info.htmlContent = info.htmlContent.replace("<h2>", newStr);
        break;
      case 3:
        newStr = `<h3 id="${i.slug}">`;
        info.htmlContent = info.htmlContent.replace("<h3>", newStr);
        break;
      case 4:
        newStr = `<h4 id="${i.slug}">`;
        info.htmlContent = info.htmlContent.replace("<h4>", newStr);
        break;
      case 5:
        newStr = `<h5 id="${i.slug}">`;
        info.htmlContent = info.htmlContent.replace("<h5>", newStr);
        break;
      case 6:
        newStr = `<h6 id="${i.slug}">`;
        info.htmlContent = info.htmlContent.replace("<h6>", newStr);
        break;
    }
  }

  return info;
};
