/**
 * 定义基类方法【工厂方法模式】
 */
function SquareFactory() {}

// 基类方法统一创建子类
SquareFactory.create = function (type, x, y, color) {
  if (typeof SquareFactory.prototype[type] === undefined) {
    throw "No this type";
  }
  // 重新绑定流水线的原型继承关系
  const Child = SquareFactory.prototype[type];
  if (Child.prototype.__proto__ !== SquareFactory.prototype) {
    Child.prototype = new SquareFactory();
  }
  // 统一创建子类
  return new Child(x, y, color);
};

// 子类出厂前的初始化
SquareFactory.prototype.init = function (square, color, message) {
  square.viewContent.style.position = "absolute";
  square.viewContent.style.width = square.width + "px";
  square.viewContent.style.height = square.height + "px";
  square.viewContent.style.left = square.x * SQUARE_WIDTH + "px";
  square.viewContent.style.top = square.y * SQUARE_WIDTH + "px";
  square.viewContent.style.backgroundColor = color;
  square.touch = function () {
    return message;
  };
};

// 自定义子类类型
SquareFactory.prototype.Floor = function (x, y, color) {
  const floor = new Floor(x, y, SQUARE_WIDTH, SQUARE_WIDTH);
  this.init(floor, color, STRATEGIES_ENUM.move);
  return floor;
};

SquareFactory.prototype.Stone = function (x, y, color) {
  const stone = new Stone(x, y, SQUARE_WIDTH, SQUARE_WIDTH);
  this.init(stone, color, STRATEGIES_ENUM.die);
  return stone;
};

SquareFactory.prototype.Food = function (x, y, color) {
  const food = new Food(x, y, SQUARE_WIDTH, SQUARE_WIDTH);
  this.init(food, color, STRATEGIES_ENUM.eat);
  food.update(x, y);
  return food;
};

SquareFactory.prototype.SnakeHead = function (x, y, color) {
  const snakeHead = new SnakeHead(x, y, SQUARE_WIDTH, SQUARE_WIDTH);
  this.init(snakeHead, color, STRATEGIES_ENUM.die);
  snakeHead.update(x, y); // 更新单例属性
  return snakeHead;
};

SquareFactory.prototype.SnakeBody = function (x, y, color) {
  const snakeBody = new SnakeBody(x, y, SQUARE_WIDTH, SQUARE_WIDTH);
  this.init(snakeBody, color, STRATEGIES_ENUM.die);
  return snakeBody;
};
