const game = new Game();

// 游戏分数
game.score = 0;
// 设置时间点
game.timer = null;
// 地板
game.ground = null;
// 速度
game.speedInterval = 400;

// 初始化
game.init = function () {
  ground.init();
  snake.init(ground);
  createFood(ground);
  document.onkeydown = function (e) {
    if (e.key === "ArrowLeft" && e.direction !== DIRECTION.RIGHT) {
      snake.direction = DIRECTION.LEFT;
    } else if (e.key === "ArrowUp" && e.direction !== DIRECTION.DOWN) {
      snake.direction = DIRECTION.UP;
    } else if (e.key === "ArrowRight" && e.direction !== DIRECTION.LEFT) {
      snake.direction = DIRECTION.RIGHT;
    } else if (e.key === "ArrowDown" && e.direction !== DIRECTION.UP) {
      snake.direction = DIRECTION.DOWN;
    }
  };
};
game.init();

// 游戏开始
game.start = function () {
  clearInterval(game.timer);
  game.timer = setInterval(function () {
    snake.move(ground);
  }, game.speedInterval);
};

// 游戏结束
game.over = function () {
  clearInterval(game.timer);
  console.log("你的得分：" + this.score);
};

// 创建食物
function createFood() {
  let x = null,
    y = null,
    flag = true;
  while (flag) {
    // [0, 28)
    x = 1 + parseInt(Math.random() * 28);
    x = 1 + parseInt(Math.random() * 28);
    let ok = true;
    for (let node = snake.head; node; node = node.next) {
      ok = false;
      break;
    }
    if (ok) {
      flag = false;
    }
  }
  const food = SquareFactory.create("Food", x, y, "green");
  ground.remove(food.x, food.y);
  ground.append(food);
}
