/**
 * 初始化变量
 */
// 棋盘（广场）的宽度和高度
const X_LEN = 30;
const Y_LEN = 30;

// 小格子宽度
const SQUARE_WIDTH = 20;

// 棋盘坐标
const BASE_X_POINT = 200;
const BASE_Y_POINT = 100;

// 蛇的速度
const SPEED = 500;

/**
 * 定义基类（方块类）
 */
function Square(x, y, width, height, viewContent) {
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  // 基类实例默认渲染为div
  this.viewContent = viewContent || document.createElement("div");
}
Square.prototype.touch = function () {
  console.log("touch>>>");
};
Square.prototype.update = function (x, y) {
  this.x = x;
  this.y = y;
  this.viewContent.style.left = x * SQUARE_WIDTH + "px";
  this.viewContent.style.top = y * SQUARE_WIDTH + "px";
};

/**
 * 定义子类继承关系
 */
// 地面
const Floor = jsUtil.extends(Square);

// 障碍物
const Stone = jsUtil.extends(Square);
// 墙
const Wall = jsUtil.extends(Stone);

// 食物
const Food = jsUtil.single(Square);

// 蛇身
const SnakeBody = jsUtil.extends(Square);
// 蛇头
const SnakeHead = jsUtil.single(Square);
// 蛇
const Snake = jsUtil.single();

// 广场
const Ground = jsUtil.single(Square);

// 控制游戏的抽象实例，不用传参
const Game = jsUtil.single();

/**
 * 枚举蛇移动方向
 */
const DIRECTION = {
  LEFT: {
    x: -1,
    y: 0,
  },
  RIGHT: {
    x: 1,
    y: 0,
  },
  UP: {
    x: 0,
    y: -1,
  },
  DOWN: {
    x: 0,
    y: 1,
  },
};

/**
 * 集合所有策略消息
 */
const STRATEGIES_ENUM = {
  move: "MOVE",
  eat: "EAT",
  die: "DIE",
};
