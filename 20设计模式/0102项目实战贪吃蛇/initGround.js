/**
 * 初始化广场
 */
const ground = new Ground(
  BASE_X_POINT,
  BASE_Y_POINT,
  X_LEN * SQUARE_WIDTH,
  Y_LEN * SQUARE_WIDTH
);

ground.init = function () {
  this.viewContent.style.position = "absolute";
  this.viewContent.style.backgroundColor = "#8aaf21";
  this.viewContent.style.width = this.width + "px";
  this.viewContent.style.height = this.height + "px";
  this.viewContent.style.left = this.x + "px";
  this.viewContent.style.top = this.y + "px";
  document.body.appendChild(this.viewContent);

  // 二维数组，存储广场中的方块对象
  this.squareTable = [];
  // (x, y) => (j, i)
  for (let i = 0; i < Y_LEN; i++) {
    this.squareTable[i] = new Array(X_LEN);
    for (let j = 0; j < X_LEN; j++) {
      let newSquare;
      if (i === 0 || j === 0 || i === Y_LEN - 1 || j === X_LEN - 1) {
        // 创建石头组成墙
        newSquare = SquareFactory.create("Stone", j, i, "#0E2F18");
      } else {
        // 创建地板
        newSquare = SquareFactory.create("Floor", j, i, "#818D79");
      }
      this.squareTable[i][j] = newSquare;
      this.viewContent.appendChild(newSquare.viewContent);
    }
  }
};
// ground.init();

// 拆除地板（模拟蛇前进一格）
ground.remove = function (x, y) {
  // 定位方块
  const square = this.squareTable[y][x];
  this.viewContent.removeChild(square.viewContent);
  this.squareTable[y][x] = null;
};

// 安装地板
ground.append = function (square) {
  this.viewContent.appendChild(square.viewContent);
  this.squareTable[square.y][square.x] = square;
};
