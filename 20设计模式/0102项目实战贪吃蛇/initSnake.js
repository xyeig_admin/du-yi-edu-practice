/**
 * 初始化蛇【策略模式】
 */
const snake = new Snake();
snake.head = null;
snake.tail = null;

snake.init = function (ground) {
  // 创建蛇头和第一节蛇身
  const snakeHead = SquareFactory.create("SnakeHead", 3, 1, "green");
  const snakeBody1 = SquareFactory.create("SnakeBody", 2, 1, "lightgreen");
  const snakeBody2 = SquareFactory.create("SnakeBody", 1, 1, "lightgreen");
  // 双向链表结构
  snakeHead.last = null;
  snakeHead.next = snakeBody1;
  snakeBody1.last = snakeHead;
  snakeBody1.next = snakeBody2;
  snakeBody2.last = snakeBody1;
  snakeBody2.next = null;
  // 记录头尾指针
  this.head = snakeHead;
  this.tail = snakeBody2;
  // 渲染
  ground.remove(snakeHead.x, snakeHead.y);
  ground.append(snakeHead);
  ground.remove(snakeBody1.x, snakeBody1.y);
  ground.append(snakeBody1);
  ground.remove(snakeBody2.x, snakeBody2.y);
  ground.append(snakeBody2);
  // 默认向右移动
  snake.direction = DIRECTION.RIGHT;
};
// snake.init(ground);

// 引入策略处理蛇的移动
snake.strategies = {
  MOVE: function (snake, square, ground, fromEat) {
    // 创建蛇身
    const newBody = SquareFactory.create(
      "SnakeBody",
      snake.head.x,
      snake.head.y,
      "lightgreen"
    );
    newBody.next = snake.head.next;
    newBody.next.last = newBody;
    newBody.last = null;
    ground.remove(snake.head.x, snake.head.y);
    ground.append(newBody);
    // 创建蛇头
    const newHead = SquareFactory.create("SnackHead", 10, 20, "green");
    newHead.last = null;
    newHead.next = newBody;
    newBody.last = newHead;
    ground.remove(newHead.x, newHead.y);
    ground.append(newHead);
    snake.head = newHead;
    // 删除蛇尾
    if (!fromEat) {
      const floor = SquareFactory.create(
        "Floor",
        snake.tail.x,
        snake.tail.y,
        "#818D79"
      );
      ground.remove(snake.tail.x, snake.tail.y);
      ground.append(floor);
      snake.tail = snake.tail.next;
    }
  },
  EAT: function (snake, square, ground) {
    this.MOVE(snake, square, ground, true);
    game.score++;
    createFood(ground);
  },
  DIE: function () {
    game.over();
  },
};

// 移动蛇头
snake.move = function (ground) {
  const square =
    ground.squareTable[this.head.y + this.direction.y][
      this.head.x + this.direction.x
    ];
  // 获取对应的行动消息
  if (typeof square.touch === "function") {
    // this => snake
    this.strategies[square.touch()](this, square, ground);
  }
};
