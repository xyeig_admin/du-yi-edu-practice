const jsUtil = {
  // 原型的继承【圣杯模式】
  inherit(target, origin) {
    const temp = function () {};
    temp.prototype = origin.prototype;
    target.prototype = new temp();
    target.prototype.constructor = target;
  },
  // 通用继承方法
  extends(origin) {
    const result = function () {
      origin.apply(this, arguments);
    };
    // 遵循单一职责原则
    this.inherit(result, origin);
    return result;
  },
  // extends: function (parent) {
  //   var result = function () {
  //     parent.apply(this, arguments);
  //   };
  //   var Super = function () {};
  //   Super.prototype = parent.prototype;
  //   result.prototype = new Super();
  //   return result;
  // },
  // 单例模式
  single(origin) {
    let singleResult = (function () {
      let instance; // 不能初始化为null，否则会进下方的if判断，首次实例化失败
      return function () {
        // 闭包中已经有实例对象了，直接返回
        if (typeof instance === "object") {
          return instance;
        }
        // 保存初始实例对象到闭包中
        instance = this;
        // 如果传了参数，说明需要将该非单例模式构造函数转换为单例模式的构造函数
        origin && origin.apply(this, arguments);
        return instance;
      };
    })();
    // 将转换后的单例模式构造函数的原型继承回转换前的原型
    origin && this.inherit(singleResult, origin);
    return singleResult;
  },
  // single(fn) {
  //   let res = null;
  //   return (...args) => {
  //     if (!res) res = fn.apply(this, args);
  //     return res;
  //   };
  // },
  // // single: function () {
  // //   var result = function () {
  // //     if (typeof result.instance === "object") {
  // //       return result.instance;
  // //     }
  // //     result.instance = this;
  // //     return this;
  // //   };
  // //   return result;
  // // },
};
