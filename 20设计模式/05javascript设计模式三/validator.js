/**
 * ==============================
 * 自定义验证类
 * 常见于第三方库
 * ==============================
 */
function Validator() {
  // 缓存校验方法
  this.cache = [];
  // 保存所有显示错误提示的DOM元素
  this.warnDom = [];
}

/**
 * 定义校验规则策略
 */
Validator.prototype.strategies = {
  isNonEmpty: function (value, errorMsg) {
    if (value === "") return errorMsg;
    return true;
  },
  maxLength: function (value, length, errorMsg) {
    if (value !== "" && value.length > length) return errorMsg;
    return true;
  },
  minLength: function (value, length, errorMsg) {
    if (value !== "" && value.length < length) return errorMsg;
    return true;
  },
};

/**
 * 定义 add 方法，用于添加校验规则
 * @param {HTMLElement} dom 要校验内容的DOM元素
 * @param {HTMLElement} showDom 显示错误信息的DOM元素
 * @param {Array} rules 自定义的校验规则
 * {
 *    strategy: 'isNonEmpty',
 *    errorMsg: '用户名不为空'
 * }, {
 *    strategy: "maxLength:4",
 *    errorMsg: "用户名长度不能超过4"
 * }
 */
Validator.prototype.add = function (dom, showDom, rules) {
  var self = this;
  this.warnDom.push(showDom);
  rules.forEach(function (ele, index) {
    self.cache.push(function () {
      // arr => [['isNonEmpty'], ['maxLength', '4']]
      var arr = ele.strategy.split(":");

      // arr => [[], ['4']]
      // type => ['isNonEmpty', 'maxLength']
      var type = arr.shift();

      // arr => [[dom.value], [dom.value, '4']]
      arr.unshift(dom.value);
      // arr => [[dom.value, ele.errorMsg], [dom.value, '4', ele.errorMsg]]
      arr.push(ele.errorMsg);

      var msg = self.strategies[type].apply(self, arr);
      if (msg !== true) showDom.innerText = msg;
      return msg;
    });
  });
};

/**
 * 定义 start 方法，用于开始校验，返回真正的校验结果
 */
Validator.prototype.start = function () {
  // 标记最终校验结果是否通过
  var flag = true;

  // 清空错误提示文本
  this.warnDom.forEach(function (ele) {
    ele.innerText = "";
  });

  // cache中保存了add方法里添加的所有校验规则
  this.cache.forEach(function (validate) {
    if (validate() !== true) {
      flag = false;
    }
  });

  return flag;
};

/**
 * 定义 extend 方法，用于扩展校验规则算法
 * @param {Object} config 规则对象
 * {
 *    isMail: function () {}
 * }
 */
Validator.prototype.extend = function (config) {
  for (const prop in config) {
    Validator.prototype.strategies[prop] = config[prop];
  }
};
