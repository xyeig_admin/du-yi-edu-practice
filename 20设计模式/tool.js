/**
 * 常用设计模式模板
 */
export default {
  singleTon(fn) {
    let res = null;
    return (...args) => {
      if (!res) res = fn.apply(this, args);
      return res;
    };
  },
  observer() {
    const _message = {};
    return {
      // 订阅信息
      on(type, fn) {
        if (!_message[type]) {
          _message[type] = [fn];
        } else {
          _message[type].push(fn);
        }
      },
      // 发布信息
      emit(type, args) {
        // 如果消息没有被注册，则返回
        if (!_message[type]) {
          return;
        }
        // 定义消息信息
        const events = {
          type: type,
          args: args || {},
        };
        for (let i = 0; i < _message[type].length; i++) {
          _message[type][i].call(this, events);
        }
      },
      // 移除信息
      remove(type, fn) {
        if (_message[type] instanceof Array) {
          _message[type] = _message[type].filter((func) => func !== fn);
        }
      },
    };
  },
  addEvents(el, event, fn) {
    if (el.addEventListener) {
      el.addEventListener(event, fn, false);
    } else if (el.attachEvent) {
      // IE9及以下
      el.attachEvent("on" + event, fn);
    } else {
      el["on" + event] = fn;
    }
  },
  iterator() {
    const data = [
      // add something
    ];
    let length = data.length;
    let index = 0; // 计数器
    return {
      next () {
        if (!this.hasNext()) {
          return null;
        }
        return data[index++];
      },
      hasNext () {
        return index < length;
      },
      current () {
        return data[index];
      },
    };
  },
  adapter(obj) {
    const _obj = {
      // add something
    };
    for (let i in _obj) {
      _obj[i] = obj[i] || _obj[i];
    }
    return _obj;
  }
};
