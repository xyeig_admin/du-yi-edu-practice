let name;
name = "333";
function sum(a, b) {
    return a + b;
}
let num = sum(3, 4);
// 智能推导出返回值为 number 类型
function sum2(a, b) {
    return a + b;
}
let num2 = sum2(3, 4);
let age;
/**
 * =================
 */
function isOdd(n) {
    return n % 2 === 0;
}
let nums = [3, 4, 5]; // 语法糖
let nums2 = [3, 4, 5];
function printValues(obj) {
    const values = Object.values(obj);
    values.forEach((v) => console.log(v));
}
printValues({
    name: "afd",
    age: 33,
});
// let n:string = undefined;
// n.toUpperCase();
/**
 * =================
 */
let name1;
if (typeof name1 === "string") {
    // 类型保护
    console.log(name.length);
}
function printMenu() {
    console.log("1. 登录");
    console.log("2. 注册");
}
function throwError(msg) {
    throw new Error(msg);
}
function alwaysDoSomething() {
    while (true) {
        //...
    }
}
let a;
a = "A";
// a = 'B';
let gender;
gender = "女";
gender = "男";
let arr; // arr永远只能取值为一个空数组
let user;
user = {
    name: "34",
    age: 33,
};
let tu;
tu = ["3", 4];
let data = "sfdsdf";
let num3 = data;
let u;
u = {
    name: "sdfd",
    gender: "男",
    age: 34,
};
function getUsers(g) {
    return [];
}
getUsers("女");
function combine(a, b) {
    if (typeof a === "number" && typeof b === "number") {
        return a * b;
    }
    else if (typeof a === "string" && typeof b === "string") {
        return a + b;
    }
    throw new Error("a和b必须是相同的类型");
}
const result1 = combine("a", "b");
const result2 = combine(1, 2);
console.log(result1, result2);
// function sum3(a: number, b: number, c: number = 0) {
function sum3(a, b, c) {
    if (c) {
        return a + b + c;
    }
    else {
        return a + b;
    }
}
sum3(3, 4);
sum3(3, 4, 5);
