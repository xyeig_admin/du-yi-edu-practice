let name: string;
name = "333";

function sum(a: number, b: number): number {
  return a + b;
}
let num: number = sum(3, 4);

// 智能推导出返回值为 number 类型
function sum2(a: number, b: number) {
  return a + b;
}
let num2 = sum2(3, 4);

let age;

/**
 * =================
 */
function isOdd(n: number) {
  return n % 2 === 0;
}

let nums: number[] = [3, 4, 5]; // 语法糖
let nums2: Array<number> = [3, 4, 5];

function printValues(obj: object) {
  const values = Object.values(obj);
  values.forEach((v) => console.log(v));
}
printValues({
  name: "afd",
  age: 33,
});

// let n:string = undefined;
// n.toUpperCase();

/**
 * =================
 */
let name1: string | undefined;
if (typeof name1 === "string") {
  // 类型保护
  console.log(name.length);
}

function printMenu() {
  console.log("1. 登录");
  console.log("2. 注册");
}

function throwError(msg: string): never {
  throw new Error(msg);
}
function alwaysDoSomething(): never {
  while (true) {
    //...
  }
}

let a: "A";
a = "A";
// a = 'B';

let gender: "男" | "女";
gender = "女";
gender = "男";

let arr: []; // arr永远只能取值为一个空数组

let user: {
  name: string;
  age: number;
};
user = {
  name: "34",
  age: 33,
};

let tu: [string, number];
tu = ["3", 4];

let data: any = "sfdsdf";
let num3: number = data;

/**
 * =================
 */
type Gender = "男" | "女";
type User = {
  name: string;
  age: number;
  gender: Gender;
};

let u: User;
u = {
  name: "sdfd",
  gender: "男",
  age: 34,
};

function getUsers(g: Gender): User[] {
  return [];
}
getUsers("女");

/**
 * =================
 */
// 声明不同类型参数的返回值类型，确保result类型【函数重载】
function combine(a: number, b: number): number;
function combine(a: string, b: string): string;
function combine(a: number | string, b: number | string): number | string {
  if (typeof a === "number" && typeof b === "number") {
    return a * b;
  } else if (typeof a === "string" && typeof b === "string") {
    return a + b;
  }
  throw new Error("a和b必须是相同的类型");
}
const result1 = combine("a", "b");
const result2 = combine(1, 2);
console.log(result1, result2);

// function sum3(a: number, b: number, c: number = 0) {
function sum3(a: number, b: number, c?: number) {
  if (c) {
    return a + b + c;
  } else {
    return a + b;
  }
}
sum3(3, 4);
sum3(3, 4, 5);
