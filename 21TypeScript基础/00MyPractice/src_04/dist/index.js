// enum Permission {
//   Read,
//   Write,
//   Create,
//   Delete,
//   ReadAndWrite,
//   ReadAndWriteAndCreate,
//   // ...
// }
var Permission;
(function (Permission) {
    Permission[Permission["Read"] = 1] = "Read";
    Permission[Permission["Write"] = 2] = "Write";
    Permission[Permission["Create"] = 4] = "Create";
    Permission[Permission["Delete"] = 8] = "Delete";
})(Permission || (Permission = {}));
//1. 如何组合权限
//使用或运算
//0001
//或
//0010
//0011
let p = Permission.Read | Permission.Write;
//2. 如何判断是否拥有某个权限
//0011
//且
//0010
//0010
function hasPermission(target, per) {
    return (target & per) === per;
}
//判断变量p是否拥有可读权限
console.log(hasPermission(p, Permission.Read));
//3. 如何删除某个权限
//0011
//异或
//0010
//0001
p = p ^ Permission.Write;
console.log(hasPermission(p, Permission.Write));
