// import sayHello, { name, sum } from "./myModule";
// console.log(name);
// sayHello();

// import myModule from "./myModule";

// // 报错
// // import fs from "fs"; // module.exports = {}
// // fs.readFileSync("./");
// // 解决
// // import { readFileSync } from "fs";
// // readFileSync("./");
// // 或者
// import * as fs from "fs";
// fs.readFileSync("./");

// const myModuleCommonJS = require("./myModule");
// console.log(myModuleCommonJS);

import myModule = require("./myModule");
