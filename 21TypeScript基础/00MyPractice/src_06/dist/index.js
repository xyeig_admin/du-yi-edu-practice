let person = {
    name: "伪装成鸭子的人",
    age: 11,
    sound: "嘎嘎嘎",
    swim() {
        console.log(this.name + "正在游泳，并发出了" + this.sound + "的声音");
    },
};
let duck = person;
let duck2 = {
    sound: "嘎嘎嘎",
    swim() {
        console.log(this.name + "正在游泳，并发出了" + this.sound + "的声音");
    },
};
function sum(numbers, callBack) {
    let s = 0;
    numbers.forEach((n, i) => {
        if (callBack(n, i)) {
            s += n;
        }
    });
    return s;
}
const result = sum([3, 4, 5, 7, 11], (n) => n % 2 !== 0);
console.log(result);
[34, 4].forEach((it) => console.log(it));
