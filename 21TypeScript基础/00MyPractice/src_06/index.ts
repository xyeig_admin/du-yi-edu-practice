// interface User {
//   name: string;
//   age: number;
//   sayHello(): void;
// }

// // // 使用type约束
// // type User = {
// //   name: string;
// //   age: number;
// //   sayHello: () => void;
// // };

// let u: User = {
//   name: "sdfds",
//   age: 33,
//   sayHello() {
//     console.log("asfadasfaf");
//   },
// };

/**
 * =================
 */
// // type Condition = (n: number) => boolean
// // type Condition {
// //   (n: number): boolean;
// // }
// interface Condition {
//   (n: number): boolean;
// }

// // 对数组每一项作处理后求和
// function sum(numbers: number[], callBack: Condition) {
//   let s = 0;
//   numbers.forEach((n) => {
//     if (callBack(n)) {
//       s += n;
//     }
//   });
//   return s;
// }

// const result = sum([3, 4, 5, 7, 11], (n) => n % 2 !== 0);
// console.log(result);

/**
 * =================
 */
// interface A {
//   T1: string;
// }
// interface B {
//   T2: number;
// }
// interface C extends A, B {
//   // T1: number;
//   T3: boolean;
// }
// let u: C = {
//   T2: 33,
//   T1: "43",
//   T3: true,
// };

// // type A = {
// //   T1: string;
// // };
// // type B = {
// //   T2: number;
// // };
// // type C = {
// //   T1: number;
// //   T3: boolean;
// // } & A &
// //   B;
// // let u: C = {
// //   T2: 33,
// //   // T1: "43", // number & string，可以使用两种类型的方法，但是无法赋值
// //   T3: true,
// // };

/**
 * =================
 */
// // type User = {
// interface User {
//   readonly id: string;
//   name: string;
//   age: number;
//   // 第一个表示该属性不能重新赋值，第二个表示数组内容不可改变
//   readonly arr: readonly string[];
// }

// let u: User = {
//   id: "123",
//   name: "Asdf",
//   age: 33,
//   arr: ["Sdf", "dfgdfg"],
// };

// // u.id = '32323'; // 不合理操作，初始化赋值后又尝试修改

// // const 表示该数组不可重新赋值，readonly 表示该数组内容不可改变
// // const arr: ReadonlyArray<number> = [3, 4, 6];
// const arr: readonly number[] = [3, 4, 6];

// // 修改原数组的函数都无法调用，包括根据索引修改
// // arr.push();
// // arr[0] = 3;

/**
 * =================
 */
interface Duck {
  sound: "嘎嘎嘎"; // 字面量
  swim(): void;
}

let person = {
  name: "伪装成鸭子的人",
  age: 11,
  sound: "嘎嘎嘎" as "嘎嘎嘎", // 类型断言
  swim() {
    console.log(this.name + "正在游泳，并发出了" + this.sound + "的声音");
  },
};

/**
 * person中有满足Duck类型的结构，所以可以赋值
 *    第三方变量不清楚自定义接口的结构
 *    所以赋值时宽松判断，允许不相关字段存在
 */
let duck: Duck = person;

/**
 * 直接使用对象字面量赋值，会进行更严格的判断
 *    直接书写字面量时一定清楚自定义接口的结构
 *    此时赋值严格判断，不允许不相关字段存在
 */
let duck2: Duck = {
  // name: "伪装成鸭子的人", // 报错
  // age: 11, // 报错
  sound: "嘎嘎嘎" as "嘎嘎嘎",
  swim() {
    console.log(this.name + "正在游泳，并发出了" + this.sound + "的声音");
  },
};

// interface User {
//     name?: string
//     age: number
// }

/**
 * =================
 */
// 需求1：传递参数n，返回符合条件的数组
// 需求2：传递参数n和i，返回符合条件的数组中第i项
interface Condition {
  (n: number, i: number): boolean;
}

function sum(numbers: number[], callBack: Condition) {
  let s = 0;
  numbers.forEach((n, i) => {
    if (callBack(n, i)) {
      s += n;
    }
  });
  return s;
}

/**
 * 缺少参数i，但是不报错
 */
const result = sum([3, 4, 5, 7, 11], (n) => n % 2 !== 0);
// const result = sum([3, 4, 5, 7, 11], (n, i) => i % 2 !== 0);
console.log(result);

// (value: number, index: number, array: number[]) => void
[34, 4].forEach((it) => console.log(it));
