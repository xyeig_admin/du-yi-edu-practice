class User {
    constructor(name, _age) {
        this.name = name;
        this._age = _age;
        this.gender = "男";
        this._publishNumber = 3;
        this._curNumber = 0;
        this.id = Math.random();
    }
    publish(title) {
        if (this._curNumber < this._publishNumber) {
            console.log("发布一篇文章：" + title);
            this._curNumber++;
        }
        else {
            console.log("你今日发布的文章数量已达到上限");
        }
    }
    set age(value) {
        if (value < 0) {
            this._age = 0;
        }
        else if (value > 200) {
            this._age = 200;
        }
        else {
            this._age = value;
        }
    }
    get age() {
        return Math.floor(this._age);
    }
}
const u = new User("aa", 22);
u.age = 1.5;
console.log(u.age);
