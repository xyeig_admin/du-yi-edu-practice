function take(arr, n) {
    if (n >= arr.length)
        return arr;
    const newArr = [];
    for (let i = 0; i < n; i++) {
        newArr.push(arr[i]);
    }
    return newArr;
}
const newArr1 = take([1, 2, 3, 4], 2);
const newArr2 = take(["a", "b", "c", "d"], 2);
function mixinArray(arr1, arr2) {
    if (arr1.length != arr2.length) {
        throw new Error("两个数组长度不等");
    }
    let result = [];
    for (let i = 0; i < arr1.length; i++) {
        result.push(arr1[i]);
        result.push(arr2[i]);
    }
    return result;
}
const result = mixinArray([1, 3, 4], ["a", "b", "c"]);
result.forEach((r) => console.log(r));
