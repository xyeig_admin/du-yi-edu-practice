// function take(arr: any[], n: number): any[] {
//   if (n >= arr.length) return arr;
//   const newArr: any[] = []; // 类型推断为never，要显式标记any
//   for (let i = 0; i < n; i++) {
//     newArr.push(arr[i]);
//   }
//   return newArr;
// }
// const newArr1 = take([1, 2, 3, 4], 2);
// const newArr2 = take(["a", "b", "c", "d"], 2);

/**
 * =================
 */
function take<T = number>(arr: T[], n: number): T[] {
  if (n >= arr.length) return arr;
  const newArr: T[] = [];
  for (let i = 0; i < n; i++) {
    newArr.push(arr[i]);
  }
  return newArr;
}
// const newArr1 = take<number>([1, 2, 3, 4], 2); // number[]
const newArr1 = take([1, 2, 3, 4], 2); // number[]
const newArr2 = take<string>(["a", "b", "c", "d"], 2); // string[]

/**
 * =================
 */
// // 回调函数，判断数组中的某一项是否满足条件
// type callback<T> = (n: T, i: number) => boolean;
// function filter<T>(arr: T[], callback: callback<T>): T[] {
//   const newArr: T[] = [];
//   arr.forEach((n, i) => {
//     if (callback(n, i)) {
//       newArr.push(n);
//     }
//   });
//   return newArr;
// }
// const arr = [1, 2, 3, 444];
// console.log(filter(arr, (n) => n % 2 !== 0));

/**
 * =================
 */
// interface callback<T> {
//   (n: T, i: number): boolean;
// }

/**
 * =================
 */
// interface hasNameProperty {
//   name: string;
// }

// /**
//  * 将某个对象的name属性的每个单词的首字母大小，然后将该对象返回
//  */
// function nameToUpperCase<T extends hasNameProperty>(obj: T): T {
//   obj.name = obj.name
//     .split(" ")
//     .map((s) => s[0].toUpperCase() + s.substring(1))
//     .join(" ");
//   return obj;
// }

// const o = {
//   name: "kevin yuan",
//   age: 22,
//   gender: "男",
// };
// const newO = nameToUpperCase(o);
// console.log(newO.name); // Kevin Yuan

/**
 * =================
 */
// 将两个数组进行混合
// [1,3,4] + ["a","b","c"] = [1, "a", 3, "b", 4, "c"]
function mixinArray<T, K>(arr1: T[], arr2: K[]): (T | K)[] {
  if (arr1.length != arr2.length) {
    throw new Error("两个数组长度不等");
  }
  let result: (T | K)[] = [];
  for (let i = 0; i < arr1.length; i++) {
    result.push(arr1[i]);
    result.push(arr2[i]);
  }
  return result;
}

const result = mixinArray([1, 3, 4], ["a", "b", "c"]);
result.forEach((r) => console.log(r)); // 1 a 3 b 4 c
