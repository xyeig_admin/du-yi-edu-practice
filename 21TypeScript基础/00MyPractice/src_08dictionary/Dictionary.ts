interface Callback<K, V> {
  (k: K, v: V): void;
}

export class Dictionary<K, V> {
  private keys: K[] = [];
  private values: V[] = [];

  /**
   * 根据键，删除对应的键值对
   * @param key 待删除的键
   * @returns 是否删除成功
   */
  delete(key: K): boolean {
    const index = this.keys.indexOf(key);
    if (index === -1) return false;
    this.keys.splice(index, 1);
    this.values.splice(index, 1);
    return true;
  }

  /**
   * 循环键值对作处理
   * @param callback 遍历规则回调函数
   */
  each(callback: Callback<K, V>): void {
    this.keys.forEach((k, i) => {
      callback(k, this.values[i]);
    });
  }

  /**
   * 获取当前键值对的数量
   */
  get size() {
    return this.keys.length;
  }

  /**
   * 判断是否存在某个键
   * @param key 待查找的键
   */
  has(key: K): boolean {
    return this.keys.indexOf(key) !== -1;
  }

  /**
   * 设置键对应的值，不存在该键则添加
   * @param key 待设置的键
   * @param val 待设置的值
   */
  set(key: K, val: V): void {
    const index = this.keys.indexOf(key);
    if (index === -1) {
      this.keys.push(key);
      this.values.push(val);
    } else {
      this.values[index] = val;
    }
  }
}
