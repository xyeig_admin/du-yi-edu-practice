Object.defineProperty(exports, "__esModule", { value: true });
exports.Dictionary = void 0;
class Dictionary {
    constructor() {
        this.keys = [];
        this.values = [];
    }
    delete(key) {
        const index = this.keys.indexOf(key);
        if (index === -1)
            return false;
        this.keys.splice(index, 1);
        this.values.splice(index, 1);
        return true;
    }
    each(callback) {
        this.keys.forEach((k, i) => {
            callback(k, this.values[i]);
        });
    }
    get size() {
        return this.keys.length;
    }
    has(key) {
        return this.keys.indexOf(key) !== -1;
    }
    set(key, val) {
        const index = this.keys.indexOf(key);
        if (index === -1) {
            this.keys.push(key);
            this.values.push(val);
        }
        else {
            this.values[index] = val;
        }
    }
}
exports.Dictionary = Dictionary;
