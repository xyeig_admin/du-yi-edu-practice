import { Dictionary } from "./Dictionary";

const dictionary = new Dictionary<string, number>();

dictionary.set("a", 1);
dictionary.set("b", 2);
dictionary.set("c", 3);

dictionary.each((k, v) => console.log(`${k}: ${v}`));
console.log(`当前字典的键值对数量为 ${dictionary.size}`);

console.log("删除键b");
dictionary.delete("b");

dictionary.each((k, v) => console.log(`${k}: ${v}`));
console.log(`当前字典的键值对数量为 ${dictionary.size}`);

console.log(`当前字典是否含有键a：${dictionary.has("a")}`);
console.log(`当前字典是否含有键b：${dictionary.has("b")}`);
