Object.defineProperty(exports, "__esModule", { value: true });
exports.printDeck = exports.createDeck = void 0;
const enums_1 = require("./enums");
const createDeck = () => {
    const deck = [];
    const marks = Object.values(enums_1.Mark);
    const colors = Object.values(enums_1.Color);
    for (const m of marks) {
        for (const c of colors) {
            deck.push({
                color: c,
                mark: m,
                getString() {
                    return this.color + this.mark;
                },
            });
        }
    }
    let joker = {
        type: "small",
        getString() {
            return "jo";
        },
    };
    deck.push(joker);
    joker = {
        type: "big",
        getString() {
            return "JO";
        },
    };
    deck.push(joker);
    return deck;
};
exports.createDeck = createDeck;
const printDeck = (deck) => {
    let result = "\n";
    deck.forEach((card, i) => {
        result += card.getString() + "\t";
        if ((i + 1) % 6 === 0) {
            result += "\n";
        }
    });
    console.log(result);
};
exports.printDeck = printDeck;
