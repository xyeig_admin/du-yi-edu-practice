// 扑克牌花色的类型
export enum Color {
  heart = "♥",
  spade = "♠",
  club = "♣",
  diamond = "♦",
}

// 扑克牌数字的类型
export enum Mark {
  A = "A",
  two = "2",
  three = "3",
  four = "4",
  five = "5",
  six = "6",
  seven = "7",
  eight = "8",
  nine = "9",
  ten = "10",
  eleven = "J",
  twelve = "Q",
  king = "K",
}
