// import { createDeck, printDeck } from "./utils";

// const deck = createDeck();
// printDeck(deck);

import { Deck } from "./deck";

const deck = new Deck();
deck.shuffle();
console.log("======洗牌后======");
deck.print();
const res = deck.publish();
console.log("======发牌后======");

// console.log("======玩家1======");
// res[0].print();
// console.log("======玩家2======");
// res[1].print();
// console.log("======玩家3======");
// res[2].print();
// console.log("======剩余======");
// res[3].print();

console.log("===========玩家1========");
res.player1.print();
console.log("===========玩家2========");
res.player2.print();
console.log("===========玩家3========");
res.player3.print();
console.log("===========桌面========");
res.left.print();
