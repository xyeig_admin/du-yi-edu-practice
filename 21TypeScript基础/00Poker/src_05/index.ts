import { createDeck, printDeck } from "./utils";

const deck = createDeck();
printDeck(deck);
