import { Color, Mark } from "./enums";

// 一张扑克牌的类型
export type NormalCard = {
  color: Color;
  mark: Mark;
};

// 一副扑克牌的类型
export type Deck = NormalCard[];
