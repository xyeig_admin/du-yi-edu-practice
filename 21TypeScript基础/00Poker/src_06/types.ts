import { Color, Mark } from "./enums";

// // 一张扑克牌的接口
// export interface NormalCard {
//   color: Color;
//   mark: Mark;
// }
// // 大小王的接口
// export interface Joker {
//   type: "big" | "small";
// }
// // 一副扑克牌的类型
// export type Deck = (NormalCard | Joker)[];

/**
 * 或者
 */

// 一副扑克牌的类型
export interface Card {
  getString(): string;
}
// 一张扑克牌的接口
export interface NormalCard extends Card {
  color: Color;
  mark: Mark;
}
// 大小王的接口
export interface Joker extends Card {
  type: "big" | "small";
}
// 一副扑克牌的类型
export type Deck = Card[];
