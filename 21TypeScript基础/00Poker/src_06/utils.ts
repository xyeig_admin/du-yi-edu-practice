import { Color, Mark } from "./enums";
import { Card, Deck, Joker, NormalCard } from "./types";

/**
 * 创建一副扑克牌
 */
export const createDeck = (): Deck => {
  const deck: Deck = [];
  const marks = Object.values(Mark);
  const colors = Object.values(Color);
  for (const m of marks) {
    for (const c of colors) {
      // // 字面量赋值要求更严格的检查
      // deck.push({
      //   color: c,
      //   mark: m,
      // });

      // const card: NormalCard = {
      //   color: c,
      //   mark: m,
      //   getString() {
      //     return this.color + this.mark;
      //   },
      // };
      // deck.push(card);

      // 或者使用类型断言
      // // 写法一
      // deck.push({
      //   color: c,
      //   mark: m,
      //   getString() {
      //     return this.color + this.mark;
      //   },
      // } as Card);

      // 写法二
      // React 中不推荐，会和组件混淆
      deck.push(<Card>{
        color: c,
        mark: m,
        getString() {
          return this.color + this.mark;
        },
      });
    }
  }

  // 加入大小王
  let joker: Joker = {
    type: "small",
    getString() {
      return "jo";
    },
  };
  deck.push(joker);
  joker = {
    type: "big",
    getString() {
      return "JO";
    },
  };
  deck.push(joker);

  return deck;
};

/**
 * 打印扑克牌
 */
export const printDeck = (deck: Deck) => {
  let result = "\n";
  deck.forEach((card, i) => {
    // let str = card.color + card.mark;
    // result += str + "\t";

    result += card.getString() + "\t";

    if ((i + 1) % 6 === 0) {
      result += "\n";
    }
  });
  console.log(result);
};
