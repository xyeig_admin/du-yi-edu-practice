import { Mark, Color } from "./enums";
import { Card, Joker } from "./types";

interface PublishResult {
  player1: Deck;
  player2: Deck;
  player3: Deck;
  left: Deck;
}

// 一副扑克牌的类型
export class Deck {
  private cards: Card[] = [];

  constructor(cards?: Card[]) {
    if (cards) {
      this.cards = cards;
    } else {
      this.init();
    }
  }

  /**
   * 创建扑克牌
   */
  private init() {
    const marks = Object.values(Mark);
    const colors = Object.values(Color);
    for (const m of marks) {
      for (const c of colors) {
        this.cards.push({
          color: c,
          mark: m,
          getString() {
            return this.color + this.mark;
          },
        } as Card);
      }
    }

    // 加入大小王
    let joker: Joker = {
      type: "small",
      getString() {
        return "jo";
      },
    };
    this.cards.push(joker);
    joker = {
      type: "big",
      getString() {
        return "JO";
      },
    };
    this.cards.push(joker);
  }

  /**
   * 打印扑克牌
   */
  print() {
    let result = "\n";
    this.cards.forEach((card, i) => {
      result += card.getString() + "\t";
      if ((i + 1) % 6 === 0) {
        result += "\n";
      }
    });
    console.log(result);
  }

  /**
   * 洗牌
   */
  shuffle() {
    for (let i = 0; i < this.cards.length; i++) {
      const targetIndex = this.getRandom(0, this.cards.length);
      const temp = this.cards[i];
      this.cards[i] = this.cards[targetIndex];
      this.cards[targetIndex] = temp;
    }
  }

  /**
   * 洗牌辅助函数
   * 无法取到最大值
   */
  private getRandom(min: number, max: number) {
    const dec = max - min;
    return Math.floor(Math.random() * dec + min);
  }

  /**
   * 发牌
   */
  // // 结果有4个card[]（元组），分别是三个玩家的牌和剩余的牌
  // publish(): [Card[], Card[], Card[], Card[]] {
  //   const result:[Card[], Card[], Card[], Card[]] = [[],[],[],[]];
  // }

  // // 1个card[]就是一个Deck
  // publish(): [Deck, Deck, Deck, Deck] {
  //   let player1: Deck, player2: Deck, player3: Deck, left: Deck;
  //   player1 = this.takeCards(17);
  //   player2 = this.takeCards(17);
  //   player3 = this.takeCards(17);
  //   left = new Deck(this.cards);
  //   return [player1, player2, player3, left];
  // }

  publish(): PublishResult {
    let player1: Deck, player2: Deck, player3: Deck, left: Deck;
    player1 = this.takeCards(17);
    player2 = this.takeCards(17);
    player3 = this.takeCards(17);
    left = new Deck(this.cards);

    return {
      player1,
      player2,
      player3,
      left,
    };
  }

  /**
   * 发牌辅助函数
   * 从剩余牌堆中摸牌
   */
  private takeCards(n: number): Deck {
    const cards: Card[] = [];
    for (let i = 0; i < n; i++) {
      cards.push(this.cards.shift() as Card);
    }
    return new Deck(cards);
  }
}
