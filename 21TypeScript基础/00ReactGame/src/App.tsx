import React from "react";
// import { BoardComp } from "./components/BoardComp";
// import { ChessComp } from "./components/ChessComp";
// import { ChessType } from "./types/enums";
import { GameComp } from "./components/GameComp";

// const types: ChessType[] = [
//   ChessType.none,
//   ChessType.red,
//   ChessType.black,
//   ChessType.none,
//   ChessType.red,
//   ChessType.black,
//   ChessType.none,
//   ChessType.red,
//   ChessType.black,
// ];

export class App extends React.Component {
  render() {
    return (
      // <div>
      //   <ChessComp
      //     type={ChessType.none}
      //     onClick={() => console.log("被点击了")}
      //   />
      //   <ChessComp
      //     type={ChessType.red}
      //     onClick={() => console.log("被点击了")}
      //   />
      //   <ChessComp
      //     type={ChessType.black}
      //     onClick={() => console.log("被点击了")}
      //   />
      // </div>

      // <div>
      //   <BoardComp
      //     chesses={types}
      //     onClick={(i) => console.log(i)}
      //     isGameOver={true}
      //   />
      // </div>
      <div>
        <GameComp />
      </div>
    );
  }
}

// import { CountComp } from "./components/CountComp";
// interface IState {
//   num: number;
// }
// export class App extends React.Component<{}, IState> {
//   state = {
//     num: 0,
//   };
//   render() {
//     return (
//       <CountComp
//         num={this.state.num}
//         onChange={(n) => {
//           this.setState({
//             num: n,
//           });
//         }}
//       />
//     );
//   }
// }
