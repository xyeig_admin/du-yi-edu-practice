import React from "react";
import { ChessType } from "../types/enums";
import { ChessComp } from "./ChessComp";
import "./BoardComp.css";

interface IProps {
  chesses: ChessType[];
  onClick?: (index: number) => void;
  isGameOver?: boolean;
}

export const BoardComp: React.FC<IProps> = ({
  chesses,
  onClick,
  isGameOver,
}) => {
  /**
   * 设置了默认值的可选参数会判断为boolean | undefined
   * 实际上不可能为undefined，正常没有影响，涉及到计算时会有影响
   * 可以使用类型断言强制约束为boolean
   * 非空断言
   */
  // const isOver = isGameOver as boolean;
  const isOver = isGameOver!;

  const list = chesses.map((type, i) => (
    <ChessComp
      key={i}
      type={type}
      onClick={() => {
        onClick && !isOver && onClick(i);
      }}
    />
  ));
  return <div className="board">{list}</div>;
};

BoardComp.defaultProps = {
  isGameOver: false,
};
