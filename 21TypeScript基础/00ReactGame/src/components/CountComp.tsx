import React from "react";

interface IProps {
  num: number;
  onChange?: (n: number) => void;
}

// 写法一
//#region
// export function CountComp(props: IProps) {
//   return (
//     <div>
//       <button
//         onClick={() => {
//           if (props.onChange) {
//             props.onChange(props.num - 1);
//           }
//         }}
//       >
//         -
//       </button>
//       <span>{props.num}</span>
//       <button
//         onClick={() => {
//           if (props.onChange) {
//             props.onChange(props.num + 1);
//           }
//         }}
//       >
//         +
//       </button>
//     </div>
//   );
// }
//#endregion

// 写法二
// 比写法一多了一个属性 props.children
//#region
export const CountComp: React.FC<IProps> = (props) => {
  return (
    <div>
      <button
        onClick={() => {
          if (props.onChange) {
            props.onChange(props.num - 1);
          }
        }}
      >
        -
      </button>
      <span>{props.num}</span>
      <button
        onClick={() => {
          if (props.onChange) {
            props.onChange(props.num + 1);
          }
        }}
      >
        +
      </button>
    </div>
  );
};
//#endregion

// 类组件
// interface IState {
//   msg: string;
//   desc: string;
// }

// export class CountComp extends React.Component<IProps, IState> {
//   // 正常初始化会覆盖IState，需要显式约束state
//   // state = {
//   state: IState = {
//     msg: "",
//     desc: "",
//     // test: 2
//   };

//   render() {
//     // this.state.msg
//     return (
//       <div>
//         <button
//           onClick={() => {
//             if (this.props.onChange) {
//               this.props.onChange(this.props.num - 1);
//             }
//           }}
//         >
//           -
//         </button>
//         <span>{this.props.num}</span>
//         <button
//           onClick={() => {
//             if (this.props.onChange) {
//               this.props.onChange(this.props.num + 1);
//             }
//           }}
//         >
//           +
//         </button>
//       </div>
//     );
//   }
// }
