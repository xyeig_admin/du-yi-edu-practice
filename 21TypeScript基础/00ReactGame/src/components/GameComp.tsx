import React from "react";
import { ChessType, GameStatus } from "../types/enums";
// import { NextChess } from "../types/enums";
import { BoardComp } from "./BoardComp";
import { GameStatusComp } from "./GameStatusComp";

interface IState {
  chesses: ChessType[];
  gameStatus: GameStatus;
  nextChess: ChessType.red | ChessType.black;
  // nextChess: NextChess;
}

export class GameComp extends React.Component<{}, IState> {
  state: IState = {
    chesses: [],
    gameStatus: GameStatus.gaming,
    nextChess: ChessType.black,
  };

  /**
   * 初始化数据
   */
  init() {
    const arr: ChessType[] = [];
    for (let i = 0; i < 9; i++) {
      arr.push(ChessType.none);
    }
    this.setState({
      chesses: arr,
      gameStatus: GameStatus.gaming,
      nextChess: ChessType.black,
    });
  }
  componentDidMount() {
    this.init();
  }

  /**
   * 处理棋子的点击事件
   * 执行时说明游戏没有结束且点击的位置没有棋子
   * @param index 棋子下标
   */
  handleChessClick(index: number) {
    const chesses: ChessType[] = [...this.state.chesses];
    chesses[index] = this.state.nextChess;
    this.setState((prevState) => ({
      chesses,
      nextChess:
        prevState.nextChess === ChessType.red ? ChessType.black : ChessType.red,
      gameStatus: this.getGameStatus(chesses, index),
    }));
  }

  /**
   * 获取当前游戏状态
   * @param chesses 点击后最新的棋子数组
   * @param index 最新落子的位置
   */
  getGameStatus(chesses: ChessType[], index: number): GameStatus {
    // 1.判断是否有一方获得胜利
    const horMin = Math.floor(index / 3) * 3;
    const verMin = Math.floor(index % 3);
    if (
      // 横向三连
      (chesses[horMin] === chesses[horMin + 1] &&
        chesses[horMin] === chesses[horMin + 2]) ||
      // 纵向三连
      (chesses[verMin] === chesses[verMin + 3] &&
        chesses[verMin] === chesses[verMin + 6]) ||
      // 斜向三连
      (chesses[0] === chesses[4] &&
        chesses[0] === chesses[8] &&
        chesses[0] !== ChessType.none) ||
      (chesses[2] === chesses[4] &&
        chesses[2] === chesses[6] &&
        chesses[2] !== ChessType.none)
    )
      return chesses[index] === ChessType.red
        ? GameStatus.redWin
        : GameStatus.blackWin;

    // 2.判断是否平局
    if (!chesses.includes(ChessType.none)) return GameStatus.equal;

    // 3.游戏正在进行
    return GameStatus.gaming;
  }

  // 严格来说，容器组件不能有样式
  render() {
    return (
      <div
        style={{
          textAlign: "center",
        }}
      >
        <h1>三连棋游戏</h1>
        <GameStatusComp
          status={this.state.gameStatus}
          next={this.state.nextChess}
        />
        <BoardComp
          chesses={this.state.chesses}
          isGameOver={this.state.gameStatus !== GameStatus.gaming}
          onClick={this.handleChessClick.bind(this)}
        />
        <button
          onClick={() => {
            this.init();
          }}
        >
          重新开始
        </button>
      </div>
    );
  }
}
