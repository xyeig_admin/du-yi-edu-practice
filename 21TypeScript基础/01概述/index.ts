// // JS写法
// 有错误的代码
// function getUserName() {
//   if (Math.random() < 0.5) {
//     return "yuan jin";
//   }
//   return 404;
// }

// let myname = getUserName();
//   mynema = myname
//     .split(" ")
//     .filter((it) => it)
//     .map((it) => it[0].toUpperCase() + it.substr(1))
//     .join(" ");

// TS写法
function getUserName2(): string | number {
  if (Math.random() < 0.5) {
    return "yuan jin";
  }
  return 404;
}
let myName2 = getUserName2();
if (typeof myName2 === "string") {
  myName2 = myName2
    .split(" ")
    .filter((it) => it)
    .map((it) => it[0].toUpperCase() + it.substring(1))
    .join(" ");
}
// function getUserName(): string | number {
//   if (Math.random() < 0.5) {
//     return "yuan jin";
//   }
//   return 404;
// }

// let myname = getUserName();
// if (typeof myname === "string") {
//   myname = myname
//     .split(" ")
//     .filter((it) => it)
//     .map((it) => it[0].toUpperCase() + it.substr(1))
//     .join(" ");
// }
