// export class Tank {
//   // 出生坐标
//   x: number = 0;
//   y: number = 0;
// }

// export class PlayerTank extends Tank {}

// export class EnemyTank extends Tank {}

// const p = new PlayerTank();

/**
 * ==========
 */
// export class Tank {
//   // 出生坐标
//   x: number = 0;
//   y: number = 0;

//   shoot() {
//     console.log("发射子弹");
//   }

//   name: string = "坦克";
//   sayHello() {
//     console.log(`我是一个${this.name}`);
//   }
// }

// export class PlayerTank extends Tank {
//   x: number = 20;
//   y: number = 20;

//   shoot() {
//     console.log("玩家坦克发射子弹");
//   }

//   name: string = "玩家坦克";

//   mySayHello() {
//     super.sayHello();
//     this.sayHello();
//   }
// }

// export class EnemyTank extends Tank {
//   shoot() {
//     console.log("敌方坦克发射子弹");
//   }

//   name: string = "敌方坦克";
// }

// const t = new Tank();
// console.log(t.x, t.y); // 0 0

// const p = new PlayerTank();
// console.log(p.x, p.y); // 20 20
// p.shoot(); // 玩家坦克发射子弹
// p.sayHello(); // 我是一个玩家坦克
// p.mySayHello(); // 我是一个玩家坦克

/**
 * ==========
 */
// export class Tank {
//   name: string = "坦克";
//   sayHello() {
//     console.log(`我是一个${this.name}`);
//   }
// }

// export class PlayerTank extends Tank {
//   name: string = "玩家坦克";
//   life: number = 5;
// }

// export class EnemyTank extends Tank {
//   name: string = "敌方坦克";
// }

// const p: Tank = new PlayerTank();
// p.sayHello(); // 我是一个玩家坦克
// // console.log(p.life); // 报错，只能使用Tank和PlayerTank共有的属性

// // 类型保护
// if (p instanceof PlayerTank) {
//   console.log(p.life); // 5，可以确定当前行的p一定是PlayerTank
// }

/**
 * ==========
 */
// export class Tank {
//   protected name: string = "坦克";
//   sayHello() {
//     console.log(`我是一个${this.name}`);
//   }
// }

// export class PlayerTank extends Tank {
//   test() {
//     console.log(this.name);
//   }
// }

// export class EnemyTank extends Tank {
//   protected name: string = "敌方坦克";
// }

/**
 * ==========
 */
export class Tank {
  protected name: string = "坦克"
}

export class PlayerTank extends Tank {
}

export class EnemyTank extends Tank {
  health: number = 1;
}

export class BossTank extends EnemyTank {
  name: string = "敌方坦克"
  health: number = 3;
}

const b = new BossTank();
console.log(b.name, b.health); // 敌方坦克 3
