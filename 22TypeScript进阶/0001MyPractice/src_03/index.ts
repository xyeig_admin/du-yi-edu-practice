// // class Chess {}
// abstract class Chess {}

// class Horse extends Chess {}

// class Pao extends Chess {}

// class Soldier extends Chess {}

// const h = new Horse();
// const p = new Pao();
// const s = new Soldier();

// // const c = new Chess(); // 不应该创建这个对象 => 转换为抽象概念

/**
 * ==========
 */
// abstract class Chess {
//   x: number = 0;
//   y: number = 0;

//   // // 无法确定具体名字
//   // readonly name: string = "";
//   abstract readonly name: string;

//   abstract move(targetX: number, targetY: number): boolean;
// }

// class Horse extends Chess {
//   readonly name: string = "Horse";
//   move(targetX: number, targetY: number): boolean {
//     this.x = targetX;
//     this.y = targetY;
//     console.log("马走日");
//     return true;
//   }
// }

// class Pao extends Chess {
//   readonly name: string;
//   constructor() {
//     super();
//     this.name = "Pao";
//   }
//   move(targetX: number, targetY: number): boolean {
//     this.x = targetX;
//     this.y = targetY;
//     console.log("炮可以跳");
//     return true;
//   }
// }

// class Soldier extends Chess {
//   get name() {
//     return "Soldier";
//   }
//   move(targetX: number, targetY: number): boolean {
//     this.x = targetX;
//     this.y = targetY;
//     console.log("兵只能走一格");
//     return true;
//   }
// }

/**
 * ==========
 */
abstract class Chess {
  x: number = 0;
  y: number = 0;

  // 定义子类统一的流程作为模板
  move(targetX: number, targetY: number): boolean {
    console.log("1.边界判断");
    console.log("2.目前为止是否有己方棋子");
    // 3.棋子移动规则判断【子类实现不一致】
    if (this.rule(targetX, targetY)) {
      this.x = targetX;
      this.y = targetY;
      console.log("移动成功");
      return true;
    }
    return false;
  }

  protected abstract rule(targetX: number, targetY: number): boolean;
}

class Horse extends Chess {
  protected rule(targetX: number, targetY: number): boolean {
    return true;
  }
}

class Pao extends Chess {
  protected rule(targetX: number, targetY: number): boolean {
    return false;
  }
}

class Soldier extends Chess {
  protected rule(targetX: number, targetY: number): boolean {
    return true;
  }
}
