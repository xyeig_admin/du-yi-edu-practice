// class User {
//   constructor(
//     public loginId: string,
//     public loginPwd: string,
//     public name: string,
//     public age: number
//   ) {}

//   /**
//    * 该方法是实例方法，必须通过实例调用
//    * 但是该方法就是为了生成实例对象，冲突了
//    */
//   // login(loginId: string, loginPwd: string): User | undefined {
//   //   return undefined;
//   // }

//   /**
//    * 相当于JS写法
//    *    User.login = function (loginId, loginPwd) {}
//    * 该方法是静态方法，必须通过类名调用
//    */
//   static login(loginId: string, loginPwd: string): User | undefined {
//     return undefined;
//   }
// }

// User.login("admin", "123");

/**
 * ==========
 */
// class User {
//   // 不同实例对象应该获得同一个用户列表，所以应该是静态成员
//   static users: User[] = [];

//   constructor(
//     public loginId: string,
//     public loginPwd: string,
//     public name: string,
//     public age: number
//   ) {
//     /**
//      * 将新创建的用户加入数组中
//      */
//     // this.users.push(this); // 报错，第一个this本意是指向类，但在构造函数（实例方法）中指向当前对象
//     User.users.push(this);
//   }

//   sayHello() {
//     console.log(
//       `大家好，我叫${this.name}，今年${this.age}岁，账号是${this.loginId}`
//     );
//   }

//   static login(loginId: string, loginPwd: string): User | undefined {
//     // return User.users.find(
//     //   (u: User) => u.loginId === loginId && u.loginPwd === loginPwd
//     // );
//     // 等同于
//     return this.users.find(
//       (u: User) => u.loginId === loginId && u.loginPwd === loginPwd
//     );
//   }
// }

// const u1 = new User("u1", "123", "王富贵", 11);
// const u2 = new User("u2", "123", "坤坤", 18);
// const u3 = new User("u3", "123", "旺财", 22);

// u1.sayHello();
// u2.sayHello();
// u3.sayHello();

// console.log(User.users);

// const res = User.login("u1", "123");
// console.log(res);

/**
 * ==========
 */
class Board {
  width: number = 500;
  height: number = 700;

  init() {
    console.log("初始化棋盘");
  }

  // 1.构造函数私有化，禁止使用new创建对象
  private constructor() {}

  /**
   * 写法一
   * 推荐
   */
  // 2.定义私有的静态成员，表示该类在系统中唯一的对象
  private static _board?: Board;
  // 3.定义公有的静态方法，只能调用该方法创建对象，内部返回系统中唯一的对象
  static createBoard(): Board {
    if (this._board) {
      return this._board;
    }
    this._board = new Board();
    return this._board;
  }

  /**
   * 写法二
   * 无法在需要时再创建对象
   * 无法实现在创建对象时完成其他操作
   */
  // static readonly singleBoard = new Board();
}

const b1 = Board.createBoard();
const b2 = Board.createBoard();
console.log(b1 === b2); // true

// const b1 = Board.singleBoard;
// const b2 = Board.singleBoard;
// console.log(b1 === b2); // true
