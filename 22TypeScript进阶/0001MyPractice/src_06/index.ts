// class User {
//   // 针对所有成员
//   // [prop: string]: any;
//   // 或者
//   [prop: string]: string | number | { (): void };

//   constructor(public name: string, public age: number) {}

//   sayHello() {}
// }

// const u = new User("aaa", 111);
// console.log(u["pid"]);
// u["sayHello"]();

/**
 * ==========
 */
// class MyArray {
//   [index: number]: string;

//   0 = "1";
//   1 = "ass";
//   2 = "sfdg";
// }
// /*
//   编译结果：
//   class MyArray {
//     constructor() {
//       this[0] = "1"
//     }
//   }
// */

// const my = new MyArray();
// console.log(my[5]);
// my[4] = "222";

/**
 * ==========
 */
class B {}

class A {
  [prop: number]: B;
  [prop: string]: object;
}
