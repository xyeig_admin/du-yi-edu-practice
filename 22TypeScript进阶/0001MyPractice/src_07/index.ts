// const u = {
//   name: "ssf",
//   age: 33,
//   sayHello() {
//     // this 为 any
//     console.log(this.name, this.age);
//   },
// };

// class User {
//   constructor(public name: string, public age: number) {}

//   sayHello() {
//     // this 为 类的实例对象
//     console.log(this, this.name, this.age);
//   }
// }

/**
 * ==========
 */
interface IUser {
  name: string;
  age: number;
  sayHello(this: IUser): void;
}

// 字面量约束this
const u: IUser = {
  name: "ssf",
  age: 33,
  sayHello() {
    console.log(this.name, this.age);
  },
};
const say = u.sayHello;
// say(); // 报错，这样调用this指向不明

// 类约束this
class User {
  constructor(public name: string, public age: number) {}

  sayHello(this: IUser) {
    // this 为 类的实例对象
    console.log(this, this.name, this.age);
  }
}
