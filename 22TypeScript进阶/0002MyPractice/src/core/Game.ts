import GameConfig from "./GameConfig";
import { Square } from "./Square";
import { SquareGroup } from "./SquareGroup";
import { createTeris } from "./Teris";
import { TerisRule } from "./TerisRule";
import { GameStatus, GameViewer, MoveDirection } from "./types";

export class Game {
  // 游戏状态
  private _gameStatus: GameStatus = GameStatus.init;
  // 当前操作的方块组
  private _currentTeris?: SquareGroup;
  // 下一个方块组
  private _nextTeris: SquareGroup;
  // 下落计时器
  private _timer?: number;
  // 下落间隔时间
  private _duration: number;
  // 已下落的方块数组
  private _exits: Square[] = [];
  // 积分
  private _score: number = 0;

  public get score(): number {
    return this._score;
  }
  public set score(val: number) {
    this._score = val;
    this._viewer.showScore(val);
    // 获得最后一个分数小于当前分数的级别
    // “!”表示去掉undefined的情况
    const level = GameConfig.levels
      .filter((level) => level.score <= val)
      .pop()!;
    if (level.duration === this._duration) return;
    this._duration = level.duration;
    if (this._timer) {
      clearInterval(this._timer);
      this._timer = undefined;
      this.autoDrop();
    }
  }

  public get gameStatus(): GameStatus {
    return this._gameStatus;
  }

  constructor(private _viewer: GameViewer) {
    this._duration = GameConfig.levels[0].duration;

    // 无实际含义，TS无法识别函数中的初始化，会报错
    this._nextTeris = createTeris({
      x: 0,
      y: 0,
    });

    this.createNextTeris();
    this._viewer.initGame(this);
    this._viewer.showScore(this.score);
  }

  /**
   * 创建下一个方块组，显示在右侧区域
   */
  private createNextTeris() {
    this._nextTeris = createTeris({
      x: 0,
      y: 0,
    });
    this.resetCenterPoint(GameConfig.nextSize.width, this._nextTeris);
    this._viewer.showNext(this._nextTeris);
    this.score = 0;
  }

  /**
   * 初始化操作
   */
  private init() {
    this._exits.forEach((sq) => sq.viewer && sq.viewer.remove());
    this._exits = [];
    this.createNextTeris();
    this._currentTeris = undefined;
  }

  /**
   * 游戏开始
   */
  start() {
    if (this._gameStatus === GameStatus.playing) return;
    if (this._gameStatus === GameStatus.over) {
      // 从游戏结束到重新开始
      this.init();
    }
    this._gameStatus = GameStatus.playing;
    // 给当前玩家操作的方块组赋值
    if (!this._currentTeris) {
      this.switchTeris();
    }
    this.autoDrop();
    this._viewer.onGameStart();
  }

  /**
   * 游戏暂停
   */
  pause() {
    if (this._gameStatus === GameStatus.playing) {
      this._gameStatus = GameStatus.pause;
      clearInterval(this._timer);
      this._timer = undefined;
      this._viewer.onGamePause();
    }
  }

  /**
   * 操作向左
   */
  controlLeft() {
    if (this._currentTeris && this._gameStatus === GameStatus.playing) {
      TerisRule.move(this._currentTeris, MoveDirection.left, this._exits);
    }
  }

  /**
   * 操作向右
   */
  controlRight() {
    if (this._currentTeris && this._gameStatus === GameStatus.playing) {
      TerisRule.move(this._currentTeris, MoveDirection.right, this._exits);
    }
  }

  /**
   * 操作向下
   */
  controlDown() {
    if (this._currentTeris && this._gameStatus === GameStatus.playing) {
      TerisRule.moveDirectly(
        this._currentTeris,
        MoveDirection.down,
        this._exits
      );
      this.hitBottom();
    }
  }

  /**
   * 操作旋转
   */
  controlRotate() {
    if (this._currentTeris && this._gameStatus === GameStatus.playing) {
      TerisRule.rotate(this._currentTeris, this._exits);
    }
  }

  /**
   * 更新当前方块组
   */
  private switchTeris() {
    this._currentTeris = this._nextTeris;
    // 下落前移除右侧区域中的新方块组
    this._currentTeris.squares.forEach((sq) => sq.viewer && sq.viewer.remove());
    // 新方块组在面板中居中下落
    this.resetCenterPoint(GameConfig.panelSize.width, this._currentTeris);
    // 新方块组一切换就可能和已下落方块重叠
    if (
      !TerisRule.canIMove(
        this._currentTeris.shape,
        this._currentTeris.centerPoint,
        this._exits
      )
    ) {
      // 游戏结束
      this._gameStatus = GameStatus.over;
      clearInterval(this._timer);
      this._timer = undefined;
      this._viewer.onGameOver();
      return;
    }
    // 下一个方块组在右侧区域中居中
    this.createNextTeris();
    this._viewer.switchNext(this._currentTeris);
  }

  /**
   * 方块组自由下落
   */
  private autoDrop() {
    if (this._timer || this._gameStatus !== GameStatus.playing) return;
    this._timer = setInterval(() => {
      if (this._currentTeris) {
        if (
          !TerisRule.move(this._currentTeris, MoveDirection.down, this._exits)
        )
          this.hitBottom();
      }
    }, this._duration);
  }

  /**
   * 重新计算中心点坐标，使该方块出现在特定区域的中上方
   * @param width 逻辑宽度（一个格子宽度）
   * @param teris 要计算中心点坐标的方块组
   */
  private resetCenterPoint(width: number, teris: SquareGroup) {
    // 1.先更新中心点坐标
    const x = Math.ceil(width / 2) - 1,
      y = 0;
    teris.centerPoint = { x, y };
    // 2.判断纵轴方向是否超出区域上方
    while (teris.squares.some((sq) => sq.point.y < 0)) {
      // // 如果区域高度只有一格（游戏设置问题），程序会死循环
      // TerisRule.move(teris, MoveDirection.down);
      // 强制下移一格
      teris.centerPoint = {
        x: teris.centerPoint.x,
        y: teris.centerPoint.y + 1,
      };
    }
  }

  /**
   * 触底处理
   */
  private hitBottom() {
    // 保存已下落的方块
    this._exits.push(...this._currentTeris!.squares);
    // 处理移除
    const lines = TerisRule.deleteSquares(this._exits);
    this.addScore(lines);
    // 切换当前方块
    this.switchTeris();
  }

  /**
   * 计算积分
   * @param lineNumber 消除行数
   */
  private addScore(lineNumber: number): void {
    if (lineNumber === 0) return;
    else if (lineNumber === 1) {
      this.score += 10;
    } else if (lineNumber === 2) {
      this.score += 25;
    } else if (lineNumber === 3) {
      this.score += 50;
    } else {
      this.score += 100;
    }
  }
}
