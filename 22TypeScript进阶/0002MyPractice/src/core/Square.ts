/**
 * 小方块
 */

import { IViewer, Point } from "./types";

export class Square {
  private _point: Point = {
    x: 0,
    y: 0,
  };
  public get point() {
    return this._point;
  }
  public set point(val) {
    this._point = val;
    // 完成显示
    this._viewer && this._viewer.show();
  }

  private _color: string = "red";
  public get color() {
    return this._color;
  }
  public set color(val) {
    this._color = val;
  }

  // 属性：显示者
  private _viewer?: IViewer;
  public get viewer() {
    return this._viewer;
  }
  public set viewer(val) {
    this._viewer = val;
    this._viewer && this._viewer.show();
  }
}
