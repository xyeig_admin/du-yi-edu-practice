import { Square } from "./Square";
import { Point, Shape } from "./types";

export class SquareGroup {
  // 只读数组
  // private _squares: ReadonlyArray<Square>;
  private _squares: readonly Square[];

  constructor(
    private _shape: Shape, // 各个方块相对中心点的坐标
    private _centerPoint: Point, // 中心点的坐标
    private _color: string
  ) {
    // 设置小方块的数组
    const arr: Square[] = [];
    this._shape.forEach((p) => {
      const sq = new Square();
      sq.color = this._color;
      arr.push(sq);
    });
    this._squares = arr;
    this.setSquarePoints();
  }

  public get squares() {
    return this._squares;
  }

  public get shape(): Shape {
    return this._shape;
  }

  /**
   * 设置访问器
   * 修改中心点坐标后同步修改方块组合的坐标
   */
  public get centerPoint(): Point {
    return this._centerPoint;
  }
  public set centerPoint(v: Point) {
    this._centerPoint = v;
    this.setSquarePoints();
  }

  /**
   * 根据中心点坐标和形状，设置小方块的坐标
   */
  private setSquarePoints() {
    this._shape.forEach((p, i) => {
      this._squares[i].point = {
        x: this._centerPoint.x + p.x,
        y: this._centerPoint.y + p.y,
      };
    });
  }

  // 旋转方向是否为顺时针
  protected isClock: boolean = true;

  /**
   * 根据当前形状，生成新的形状
   */
  afterRotateShape(): Shape {
    if (this.isClock) {
      return this._shape.map((p) => ({
        x: -p.y,
        y: p.x,
      }));
    }
    return this._shape.map((p) => ({
      x: p.y,
      y: -p.x,
    }));
  }

  rotate() {
    const newShape = this.afterRotateShape();
    this._shape = newShape;
    this.setSquarePoints();
  }
}
