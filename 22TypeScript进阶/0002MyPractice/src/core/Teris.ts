import { SquareGroup } from "./SquareGroup";
import { Point, Shape } from "./types";
import { getRandom } from "./utils";

/**
 * 预定义一些方块组合的形状
 */
export class TShape extends SquareGroup {
  constructor(_centerPoint: Point, _color: string) {
    super(
      [
        {
          x: -1,
          y: 0,
        },
        {
          x: 0,
          y: 0,
        },
        {
          x: 1,
          y: 0,
        },
        {
          x: 0,
          y: -1,
        },
      ],
      _centerPoint,
      _color
    );
  }
}
// export const TShape: Shape = [
//   {
//     x: -1,
//     y: 0,
//   },
//   {
//     x: 0,
//     y: 0,
//   },
//   {
//     x: 1,
//     y: 0,
//   },
//   {
//     x: 0,
//     y: -1,
//   },
// ];

export class LShape extends SquareGroup {
  constructor(_centerPoint: Point, _color: string) {
    super(
      [
        {
          x: -2,
          y: 0,
        },
        {
          x: -1,
          y: 0,
        },
        {
          x: 0,
          y: 0,
        },
        {
          x: 0,
          y: -1,
        },
      ],
      _centerPoint,
      _color
    );
  }
}
// export const LShape: Shape = [
//   {
//     x: -2,
//     y: 0,
//   },
//   {
//     x: -1,
//     y: 0,
//   },
//   {
//     x: 0,
//     y: 0,
//   },
//   {
//     x: 0,
//     y: -1,
//   },
// ];

export class LMirrorShape extends SquareGroup {
  constructor(_centerPoint: Point, _color: string) {
    super(
      [
        {
          x: 2,
          y: 0,
        },
        {
          x: 1,
          y: 0,
        },
        {
          x: 0,
          y: 0,
        },
        {
          x: 0,
          y: -1,
        },
      ],
      _centerPoint,
      _color
    );
  }
}
// export const LMirrorShape: Shape = [
//   {
//     x: 2,
//     y: 0,
//   },
//   {
//     x: 1,
//     y: 0,
//   },
//   {
//     x: 0,
//     y: 0,
//   },
//   {
//     x: 0,
//     y: -1,
//   },
// ];

export class SShape extends SquareGroup {
  constructor(_centerPoint: Point, _color: string) {
    super(
      [
        {
          x: 0,
          y: 0,
        },
        {
          x: 1,
          y: 0,
        },
        {
          x: 0,
          y: 1,
        },
        {
          x: -1,
          y: 1,
        },
      ],
      _centerPoint,
      _color
    );
  }
  rotate() {
    super.rotate();
    this.isClock = !this.isClock;
  }
}
// export const SShape: Shape = [
//   {
//     x: 0,
//     y: 0,
//   },
//   {
//     x: 1,
//     y: 0,
//   },
//   {
//     x: 0,
//     y: 1,
//   },
//   {
//     x: -1,
//     y: 1,
//   },
// ];

export class SMirrorShape extends SquareGroup {
  constructor(_centerPoint: Point, _color: string) {
    super(
      [
        {
          x: 0,
          y: 0,
        },
        {
          x: -1,
          y: 0,
        },
        {
          x: 0,
          y: 1,
        },
        {
          x: 1,
          y: 1,
        },
      ],
      _centerPoint,
      _color
    );
  }
  rotate() {
    super.rotate();
    this.isClock = !this.isClock;
  }
}
// export const SMirrorShape: Shape = [
//   {
//     x: 0,
//     y: 0,
//   },
//   {
//     x: -1,
//     y: 0,
//   },
//   {
//     x: 0,
//     y: 1,
//   },
//   {
//     x: 1,
//     y: 1,
//   },
// ];

export class SquareShape extends SquareGroup {
  constructor(_centerPoint: Point, _color: string) {
    super(
      [
        {
          x: 0,
          y: 0,
        },
        {
          x: 1,
          y: 0,
        },
        {
          x: 0,
          y: 1,
        },
        {
          x: 1,
          y: 1,
        },
      ],
      _centerPoint,
      _color
    );
  }
  // 返回当前形状 => 不旋转
  afterRotateShape(): Shape {
    return this.shape;
  }
}
// export const SquareShape: Shape = [
//   {
//     x: 0,
//     y: 0,
//   },
//   {
//     x: 1,
//     y: 0,
//   },
//   {
//     x: 0,
//     y: 1,
//   },
//   {
//     x: 1,
//     y: 1,
//   },
// ];

export class LineShape extends SquareGroup {
  constructor(_centerPoint: Point, _color: string) {
    super(
      [
        {
          x: -1,
          y: 0,
        },
        {
          x: 0,
          y: 0,
        },
        {
          x: 1,
          y: 0,
        },
        {
          x: 2,
          y: 0,
        },
      ],
      _centerPoint,
      _color
    );
  }
  rotate() {
    super.rotate();
    this.isClock = !this.isClock;
  }
}
// export const LineShape: Shape = [
//   {
//     x: -1,
//     y: 0,
//   },
//   {
//     x: 0,
//     y: 0,
//   },
//   {
//     x: 1,
//     y: 0,
//   },
//   {
//     x: 2,
//     y: 0,
//   },
// ];

/**
 * 所有预定义形状的数组
 */
export const shapes = [
  TShape,
  LShape,
  LMirrorShape,
  SShape,
  SMirrorShape,
  SquareShape,
  LineShape,
];

/**
 * 所有预定义的颜色
 */
export const colors = ["red", "white", "green", "blue", "orange"];

/**
 * 随机产生一个俄罗斯方块
 * @param centerPoint 中心点坐标
 * @description 颜色随机，形状（其他方块坐标）随机
 */
export const createTeris = (centerPoint: Point): SquareGroup => {
  // let index = getRandom(0, shapes.length);
  // const shape = shapes[index];
  // // index = getRandom(0, colors.length);
  // // const color = colors[index];
  // const color = `rgb(${getRandom(0, 256)}, ${getRandom(0, 256)}, ${getRandom(
  //   0,
  //   256
  // )})`;
  // return new SquareGroup(shape, centerPoint, color);

  let index = getRandom(0, shapes.length);
  const shape = shapes[index];
  const color = `rgb(${getRandom(0, 256)}, ${getRandom(0, 256)}, ${getRandom(
    0,
    256
  )})`;
  return new shape(centerPoint, color);
};
