/**
 * 该类提供一系列函数，根据游戏规则判断各种情况
 */

import GameConfig from "./GameConfig";
import { Square } from "./Square";
import { SquareGroup } from "./SquareGroup";
import { MoveDirection, Point, Shape } from "./types";

/**
 * 类型保护函数
 * @param obj 判断对象
 * @returns 是否是 Point 类型
 */
const isPoint = (obj: any): obj is Point => {
  if (typeof obj.x === "undefined") {
    return false;
  }
  return true;
};

export class TerisRule {
  /**
   * 判断某个形状的方块，是否能移动到目标位置
   * @param shape 方块形状
   * @param targetPoint 目标位置
   * @returns 是否能移动
   */
  static canIMove(shape: Shape, targetPoint: Point, exits: Square[]): boolean {
    // 1.假设中心点已经移动到了目标位置，算出每个方块坐标
    const targetSquarePoints: Point[] = shape.map((s) => ({
      x: s.x + targetPoint.x,
      y: s.y + targetPoint.y,
    }));
    // 2.边界判断：至少有一个方块坐标不在面板内
    if (
      targetSquarePoints.some(
        (p) =>
          p.x < 0 ||
          p.x > GameConfig.panelSize.width - 1 ||
          p.y < 0 ||
          p.y > GameConfig.panelSize.height - 1
      )
    )
      return false;
    // 3.判断目标位置是否有已下落方块
    if (
      targetSquarePoints.some((p) =>
        exits.some((sq) => sq.point.x === p.x && sq.point.y === p.y)
      )
    )
      return false;
    return true;
  }

  // /**
  //  * 将方块移动到目标位置
  //  * @param teris 待移动的方块
  //  * @param targetPoint 目标位置
  //  * @returns 是否移动成功
  //  */
  // static move(teris: SquareGroup, targetPoint: Point): boolean {}

  // /**
  //  * 将方块每次向目标方向移动1格
  //  * @param teris 待移动的方块
  //  * @param direction 目标方向
  //  * @returns 是否移动成功
  //  */
  // static move(teris: SquareGroup, direction: MoveDirection): boolean {}

  /**
   * 函数重载
   */
  static move(teris: SquareGroup, targetPoint: Point, exits: Square[]): boolean;
  static move(
    teris: SquareGroup,
    direction: MoveDirection,
    exits: Square[]
  ): boolean;
  static move(
    teris: SquareGroup,
    targetPointOrDirection: Point | MoveDirection,
    exits: Square[]
  ): boolean {
    if (isPoint(targetPointOrDirection)) {
      if (this.canIMove(teris.shape, targetPointOrDirection, exits)) {
        teris.centerPoint = targetPointOrDirection;
        return true;
      }
      return false;
    } else {
      const direction = targetPointOrDirection;
      let targetPoint: Point;
      if (direction === MoveDirection.down) {
        targetPoint = {
          x: teris.centerPoint.x,
          y: teris.centerPoint.y + 1,
        };
      } else if (direction === MoveDirection.left) {
        targetPoint = {
          x: teris.centerPoint.x - 1,
          y: teris.centerPoint.y,
        };
      } else {
        targetPoint = {
          x: teris.centerPoint.x + 1,
          y: teris.centerPoint.y,
        };
      }
      return this.move(teris, targetPoint, exits);
    }
  }

  /**
   * 将当前的方块，移动到目标方向的终点
   * @param teris 待移动的方块
   * @param direction 目标方向
   */
  static moveDirectly(
    teris: SquareGroup,
    direction: MoveDirection,
    exits: Square[]
  ) {
    while (this.move(teris, direction, exits)) {}
  }

  /**
   * 旋转
   * @param teris 待旋转的方块
   * @returns 是否可以旋转（旋转后是否超出边界）
   */
  static rotate(teris: SquareGroup, exits: Square[]): boolean {
    const newShape = teris.afterRotateShape();
    if (this.canIMove(newShape, teris.centerPoint, exits)) {
      teris.rotate();
      return true;
    }
    return false;
  }

  /**
   * 从已存在的方块数组中删除满行的方块
   * @param exits 已下落的方块数组
   * @returns 返回删除的行数
   */
  static deleteSquares(exits: Square[]): number {
    // 1.获取所有行
    const lines = exits.map((sq) => sq.point.y);
    // 2.获取最大最小值
    const minY = Math.min(...lines);
    const maxY = Math.max(...lines);
    let count = 0;
    // 3.判断每一行是否可以删除
    for (let y = minY; y <= maxY; y++) {
      this.deleteLine(exits, y);
      count++;
    }
    return count;
  }

  /**
   * 删除一行方块
   * @param exits 已下落的方块数组
   * @param y 待删除的行号
   * @returns 是否删除成功
   */
  private static deleteLine(exits: Square[], y: number): boolean {
    const squares = exits.filter((sq) => sq.point.y === y);
    if (squares.length === GameConfig.panelSize.width) {
      // 1.从界面上移除
      squares.forEach((sq) => {
        // 移除当前行
        sq.viewer && sq.viewer.remove();
        // 从已下落数组中移除当前方块
        const index = exits.indexOf(sq);
        exits.splice(index, 1);
      });
      // 2.当前行上方的方块下移一格
      exits
        .filter((e) => e.point.y < y)
        .forEach(
          (sq) =>
            (sq.point = {
              x: sq.point.x,
              y: sq.point.y + 1,
            })
        );
      return true;
    }
    return false;
  }
}
