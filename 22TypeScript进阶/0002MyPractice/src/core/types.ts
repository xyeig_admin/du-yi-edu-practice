import { Game } from "./Game";
import { SquareGroup } from "./SquareGroup";

/**
 * 逻辑坐标
 */
export interface Point {
  readonly x: number;
  readonly y: number;
}

/**
 * 类的显示者
 */
export interface IViewer {
  // 显示
  show(): void;

  // 移除
  remove(): void;
}

/**
 * 方块组合的形状
 */
export type Shape = Point[];

/**
 * 移动方向
 */
export enum MoveDirection {
  left,
  right,
  down,
}

/**
 * 游戏状态
 */
export enum GameStatus {
  init, // 开始
  playing, // 进行中
  pause, // 暂停
  over, // 结束
}

/**
 * 游戏的显示者
 */
export interface GameViewer {
  // 显示下一个方块组
  showNext(teris: SquareGroup): void;
  // 切换到下一个方块组
  switchNext(teris: SquareGroup): void;
  // 初始化游戏界面
  initGame(game: Game): void;
  // 显示分数
  showScore(score: number): void;
  // 游戏状态改变
  onGamePause(): void;
  onGameStart(): void;
  onGameOver(): void;
}
