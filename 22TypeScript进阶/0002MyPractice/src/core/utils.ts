/**
 * 产生一个随机数
 * @param min 最小值
 * @param max 最大值
 * @description 取不到最大值
 */
export const getRandom = (min: number, max: number): number =>
  Math.floor(Math.random() * (max - min) + min);
