import { Game } from "../Game";
import GameConfig from "../GameConfig";
import { SquareGroup } from "../SquareGroup";
import { GameStatus, GameViewer } from "../types";
import PageConfig from "./PageConfig";
import { SquarePageViewer } from "./SquarePageViewer";
import $ from "jquery";

export class GamePageViewer implements GameViewer {
  private _nextDom = $("#next");
  private _panelDom = $("#panel");
  private _scoreDom = $("#score");
  private _msgDom = $("#msg");

  showNext(teris: SquareGroup): void {
    teris.squares.forEach((sq) => {
      sq.viewer = new SquarePageViewer(sq, this._nextDom);
    });
  }

  switchNext(teris: SquareGroup): void {
    teris.squares.forEach((sq) => {
      sq.viewer && sq.viewer.remove();
      sq.viewer = new SquarePageViewer(sq, this._panelDom);
    });
  }

  initGame(game: Game): void {
    // 1.设置区域宽高
    this._panelDom.css({
      width: GameConfig.panelSize.width * PageConfig.SquareSize.width,
      height: GameConfig.panelSize.height * PageConfig.SquareSize.height,
    });
    this._nextDom.css({
      width: GameConfig.nextSize.width * PageConfig.SquareSize.width,
      height: GameConfig.nextSize.height * PageConfig.SquareSize.height,
    });
    // 2.注册键盘事件
    $(document).keydown((e) => {
      if (e.code === "ArrowLeft") {
        game.controlLeft();
      } else if (e.code === "ArrowUp") {
        game.controlRotate();
      } else if (e.code === "ArrowRight") {
        game.controlRight();
      } else if (e.code === "ArrowDown") {
        game.controlDown();
      } else if (e.code === "Space") {
        if (game.gameStatus === GameStatus.playing) game.pause();
        else game.start();
      }
    });
  }

  showScore(score: number): void {
    this._scoreDom.html(score.toString());
  }

  onGamePause(): void {
    this._msgDom.css({
      display: "flex",
    });
    this._msgDom.find("p").html("游戏暂停");
  }

  onGameStart(): void {
    this._msgDom.hide();
  }

  onGameOver(): void {
    this._msgDom.css({
      display: "flex",
    });
    this._msgDom.find("p").html("游戏结束");
  }
}
