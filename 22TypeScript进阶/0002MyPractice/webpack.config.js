const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

module.exports = {
  entry: "./src/index.ts", // 启动文件/入口文件
  output: {
    // 出口文件
    path: path.resolve("./dist"), // 根目录
    filename: "script/bundle.js", // 文件位置
  },
  plugins: [
    // 生成页面的模板
    new HtmlWebpackPlugin({
      template: "./public/index.html",
    }),
    // 清空上次打包输出的文件
    new CleanWebpackPlugin(),
  ],
  module: {
    // 加载规则
    rules: [
      {
        test: /.ts$/,
        use: {
          loader: "ts-loader",
          options: {
            transpileOnly: true,
          },
        },
      },
    ],
  },
  resolve: {
    // 解析模块时读取的扩展名
    extensions: [".ts", ".js"],
  },
};
