// function test(target: new () => object) {
//   console.log(target); // [class A]
// }

// @test
// class A {}

/**
 * ==========
 */
// function test(target: new () => object) {
//   console.log(target);
//   return class B extends target {};
// }

// @test
// class A {}

// const a = new A();
// console.log(a); // B {}

/**
 * ==========
 */
// function test(target: new (...args: any[]) => object) {
//   console.log(target);
// }

// @test
// class A {}

// @test
// class C {
//   constructor(public name: string) {}
// }

// @test
// class D {
//   constructor(public name: string, public age: number) {}
// }

/**
 * ==========
 */
// function test(str: string) {
//   return function (target: new (...args: any[]) => object) {
//     console.log(target);
//   };
// }

// @test("这是一个类")
// class A {}

/**
 * ==========
 */
// type Constructor = new (...args: any[]) => object;
// function d1(target: Constructor) {
//   console.log("d1");
// }
// function d2(target: Constructor) {
//   console.log("d2");
// }

// @d1
// @d2
// class A {}
// // 从下到上，先输出d2再输出d1

/**
 * ==========
 */
// type Constructor = new (...args: any[]) => object;
// // 普通函数
// function d1() {
//   console.log("d1");
//   // 返回值才是装饰器
//   return function (target: Constructor) {
//     console.log("d1 decorator");
//   };
// }
// function d2() {
//   console.log("d2");
//   return function (target: Constructor) {
//     console.log("d2 decorator");
//   };
// }

// @d1()
// @d2()
// class A {}
// /**
// d1
// d2
// d2 decorator
// d1 decorator
// */

/**
 * ==========
 */
// type Constructor = new (...args: any[]) => object;
// function d(target: any, key: string) {
//   console.log(target, key);
//   /**
//    * target === A.prototype
//    * {} prop1
//    * {} prop2
//    */
// }

// class A {
//   @d
//   prop1: string = "";

//   @d
//   prop2: string = "";
// }

/**
 * ==========
 */
// type Constructor = new (...args: any[]) => object;
// function d(target: any, key: string) {
//   console.log(target, key);
//   /**
//    * {} prop1
//    * [class A] { prop2: '' } prop2
//    */
// }

// class A {
//   @d
//   prop1: string = "";

//   @d
//   static prop2: string = "";
// }

/**
 * ==========
 */
// type Constructor = new (...args: any[]) => object;
// function d() {
//   return function (target: any, key: string, descriptor: PropertyDescriptor) {
//     console.log(target, key, descriptor);
//   };
//   /**
//   {} method1 {
//   value: [Function: method1],
//     writable: true,
//     enumerable: false,
//     configurable: true
//   }
//   */
// }

// class A {
//   @d()
//   method1() {}
// }

/**
 * ==========
 */
function enumerable(target: any, key: string, descriptor: PropertyDescriptor) {
  // console.log(target, key, descriptor);
  descriptor.enumerable = true;
}

function useless(target: any, key: string, descriptor: PropertyDescriptor) {
  descriptor.value = function () {
    console.warn(key + "方法已过期");
  };
}

class A {
  @enumerable
  @useless
  method1() {
    console.log("method1");
  }

  @enumerable
  method2() {}
}

const a = new A();
a.method1();
