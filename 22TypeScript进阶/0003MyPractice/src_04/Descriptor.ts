type Constructor = new (...args: any[]) => object;

/**
 * 将类的装饰信息保存到该类的原型中
 * @param description 类的描述信息
 * @returns 类装饰器
 */
export const classDescriptor = (description: string) => {
  return (targetClass: Constructor) => {
    targetClass.prototype.$classDescription = description;
  };
};

/**
 * 将所有属性的信息保存到该类的原型中
 * @param description 属性的描述信息
 * @returns 属性装饰器
 * @description 该装饰器可能调用多次
 */
export const propDescriptor = (description: string) => {
  return (targetPrototype: any, propName: string) => {
    if (!targetPrototype.$propDescriptions) {
      targetPrototype.$propDescriptions = [];
    }
    targetPrototype.$propDescriptions.push({
      propName,
      description,
    });
  };
};

/**
 * 输出实例对象的相关描述信息
 * @param obj 实例对象
 */
export const printObject = (obj: any): void => {
  // 1.输出类的描述信息，没有则输出原型上的类名
  if (obj.$classDescription) {
    console.log(obj.$classDescription);
  } else {
    // console.log(obj.__proto__.constructor.name);
    console.log(Object.getPrototypeOf(obj).constructor.name);
  }
  // 2.输出所有属性名描述信息和属性值，没有则输出属性名
  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      const prop = obj.$propDescriptions.find((p: any) => p.propName === key);
      if (prop) {
        console.log(`\t${prop.description}: ${obj[key]}`);
      } else {
        console.log(`\t${key}: ${obj[key]}`);
      }
    }
  }
};

// export function classDescriptor(description: string) {
//   return function (target: Function) {
//     // 保存到该类的原型中
//     target.prototype.$classDescription = description;
//   };
// }

// export function propDescriptor(description: string) {
//   return function (target: any, propName: string) {
//     // 把所有的属性信息保存到该类的原型中
//     if (!target.$propDescriptions) {
//       target.$propDescriptions = [];
//     }
//     target.$propDescriptions.push({
//       propName,
//       description,
//     });
//   };
// }

// export function printObj(obj: any) {
//   // 输出类的名字
//   if (obj.$classDescription) {
//     console.log(obj.$classDescription);
//   } else {
//     console.log(Object.getPrototypeOf(obj).constructor.name);
//   }
//   if (!obj.$propDescriptions) {
//     obj.$propDescriptions = [];
//   }
//   // 输出所有的属性描述和属性值
//   for (const key in obj) {
//     if (obj.hasOwnProperty(key)) {
//       const prop = obj.$propDescriptions.find((p: any) => p.propName === key);
//       if (prop) {
//         console.log(`\t${prop.description}:${obj[key]}`);
//       } else {
//         console.log(`\t${key}:${obj[key]}`);
//       }
//     }
//   }
// }
