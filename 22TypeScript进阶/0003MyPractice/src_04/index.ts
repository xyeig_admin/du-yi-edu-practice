import { classDescriptor, printObject, propDescriptor } from "./Descriptor";

@classDescriptor("用户类")
class User {
  @propDescriptor("用户名")
  loginId: string;

  loginPwd: string;

  constructor(loginId: string, loginPwd: string) {
    this.loginId = loginId;
    this.loginPwd = loginPwd;
  }
}

const user = new User("admin", "123");
printObject(user);
/**
用户类
    用户名: admin
    loginPwd: 123
*/

// import { classDescriptor, propDescriptor, printObj } from "./Descriptor";

// @classDescriptor("文章")
// class Article {
//   @propDescriptor("标题")
//   title: string = "title";

//   @propDescriptor("内容")
//   content: string = "content";

//   @propDescriptor("日期")
//   date: Date = new Date();
// }

// const ar = new Article();
// ar.title = "xxxx";
// ar.content = "asdfasdfasdfasdfasdf";
// ar.date = new Date();

// printObj(ar);
// /**
// 文章
//     标题:xxxx
//     内容:asdfasdfasdfasdfasdf
//     日期:Sat Nov 04 2023 17:00:22 GMT+0800 (China Standard Time)
//  */
