import "reflect-metadata";

type Constructor = new (...args: any[]) => object;

// KEY值一定不能冲突，最好使用Symbol类型
// const KEY = "descriptor";
const KEY = Symbol.for("descriptor");

export const descriptor = (description: string) => {
  return Reflect.metadata(KEY, description);
};

/**
 * 输出实例对象的相关描述信息
 * @param obj 实例对象
 */
export const printObject = (obj: any): void => {
  const classConstructor = Object.getPrototypeOf(obj).constructor;

  // 1.输出类的描述信息，没有则输出原型上的类名
  if (Reflect.hasMetadata(KEY, classConstructor)) {
    console.log(Reflect.getMetadata(KEY, classConstructor));
  } else {
    console.log(classConstructor.name);
  }
  // 2.输出所有属性名描述信息和属性值，没有则输出属性名
  for (const key in obj) {
    if (Reflect.hasMetadata(KEY, obj, key)) {
      console.log(`\t${Reflect.getMetadata(KEY, obj, key)}: ${obj[key]}`);
    } else {
      console.log(`\t${key}: ${obj[key]}`);
    }
  }
};
