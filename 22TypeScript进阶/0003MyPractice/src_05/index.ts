// import "reflect-metadata"; // 提供了一些全局对象

// @Reflect.metadata("a", "一个类") // 传入 key-value，后续根据key值区分多个装饰器
// class A {
//   @Reflect.metadata("prop", "一个属性")
//   prop1: string = "prop1";
// }

// const obj = new A();

// // 获取附着在类A上的键名为a的元数据
// console.log(Reflect.getMetadata("a", A)); // 一个类
// console.log(Reflect.getMetadata("a", Object.getPrototypeOf(obj).constructor)); // 一个类

// // 获取附着在obj对象上的某个成员的元数据
// console.log(Reflect.getMetadata("prop", obj, "prop1")); // 一个属性

import { descriptor, printObject } from "./Descriptor";

@descriptor("用户类")
class User {
  @descriptor("用户名")
  loginId: string;

  loginPwd: string;

  constructor(loginId: string, loginPwd: string) {
    this.loginId = loginId;
    this.loginPwd = loginPwd;
  }
}

const user = new User("admin", "123");
printObject(user);
/**
用户类
    用户名: admin
    loginPwd: 123
*/
