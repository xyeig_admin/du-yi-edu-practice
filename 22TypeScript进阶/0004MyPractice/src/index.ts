interface User {
  name: string;
  age: number;
}
let u1: Partial<User> = {
  age: 24,
};

// let u2: Exclude<"a" | "b" | "c" | "d", "b" | "c">; // "a" | "d"
type T = "男" | "女" | null | undefined;
type NEWT = Exclude<T, null | undefined>; // "男" | "女"

let u3: Extract<"a" | "b" | "c" | "d", "b" | "c" | "e" | "f">; // "b" | "c"

type Func = () => number;
type returnType = ReturnType<Func>; // number

function sum(a: number, b: number) {
  return a + b;
}
let a: ReturnType<typeof sum>; // number

class User {}
let u4: InstanceType<typeof User>; // User

// 约束构造函数
type twoParamsConstructor = new (arg1: any, arg2: any) => User;
// 鸭子辩型法 => 参数少时不报错，丢失类型检查
// const A:twoParamsConstructor = class Test {}
type Inst = InstanceType<twoParamsConstructor>; // User
