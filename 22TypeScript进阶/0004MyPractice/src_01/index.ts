// const a = "a"; // 字面量类型
// console.log(typeof a); // string 【JS本身有的关键字】

// let b: typeof a = "a"; // 让b和a的类型保持一致

/**
 * ==========
 */
// class User {
//   loginId: string = "admin";
//   loginPwd: string = "123";
// }

// // 需求：传入一个类，调用该类的构造函数，创建用户对象
// // function createUser(cls: ???): User {

// // function createUser(cls: new () => User): User {
// //   return new cls();
// // }
// // 或者
// function createUser(cls: typeof User): User {
//   return new cls();
// }

// const u = createUser(User);

/**
 * ==========
 */
// interface User {
//   loginId: string;
//   loginPwd: string;
//   age: number;
// }

// // // 无法百分百确定 obj[prop] 属于接口定义的属性
// // function printUserProperty(obj:User, prop: string) {
// //   console.log(obj[prop]);
// // }

// // 接口属性变化时无法自动更新 prop 的限制类型
// // function printUserProperty(obj:User, prop: "loginId" | "loginPwd" | "age") {
// //   console.log(obj[prop]);
// // }

// function printUserProperty(obj: User, prop: keyof User) {
//   console.log(obj[prop]);
// }

/**
 * ==========
 */
// interface User {
//   loginId: string;
//   loginPwd: string;
//   age: number;
// }

// // 使用索引器定义类型别名，只能添加定义好的属性
// type UserString = {
//   // loginId: string;
//   // loginPwd: string;
//   // age: number;

//   // =>
//   // [p in "loginId" | "loginPwd" | "age"]: string;

//   // =>
//   // 将User的所有属性值类型变成字符串，得到一个新类型
//   [p in keyof User]: string;
// };
// // const u: UserString = {};
// // u.abc = "123"; // 报错

// // 保持User中所有属性值的类型不变，得到一个新类型
// type UserObject = {
//   [p in keyof User]: User[p];
// };
// // 保持User中所有属性值的类型不变，得到一个只读的新类型
// type UserReadonly = {
//   readonly [p in keyof User]: User[p];
// };
// // 保持User中所有属性值的类型不变，得到一个可选的新类型
// type UserPartial = {
//   [p in keyof User]?: User[p];
// };

/**
 * ==========
 */
interface User {
  loginId: string;
  loginPwd: string;
  age: number;
}
interface Article {
  title: string;
  publishDate: Date;
}

type NewString<T> = {
  [p in keyof T]: string;
};

type NewReadonly<T> = {
  readonly [p in keyof T]: T[p];
};

type NewPartial<T> = {
  [p in keyof T]?: T[p];
};

const u: NewString<Article> = {
  title: "Sfsdf",
  publishDate: "sdf",
};
