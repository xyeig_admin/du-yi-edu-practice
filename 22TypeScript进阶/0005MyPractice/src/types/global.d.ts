// /**
//  * 只是告诉TS编译时如何报错
//  * 与代码运行时无关
//  * 该文件不参与运行
//  */

// // 写法一
// // declare var console: object;

// // 写法二
// // declare var console: {
// //   log(message: any): void;
// // };

// // 写法三
// // interface Console {
// //   log(message: any): void;
// // }
// // declare var console: Console;

// // 写法四
// declare namespace console {
//   function log(message?: any): void;
//   function error(message?: any): void;
// }

// /**
//  * ==========
//  */
// type TimeHandler = () => void;
// declare function setTimeout(handler: TimeHandler, miliseconds: number): void;
// declare function setInterval(handler: TimeHandler, miliseconds: number): void;

/// <reference path="../types/lodash/index.d.ts" />
