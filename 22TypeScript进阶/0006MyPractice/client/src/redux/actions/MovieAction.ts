/**
 * action的创建函数
 */

import { ISearchCondition, SwitchType } from "../../services/CommonTypes";
import { IMovie, MovieService } from "../../services/MovieService";
import { IRootState } from "../reducers/RootReducer";
import { IAction } from "./ActionType";
import { ThunkAction } from "redux-thunk";

export type SaveMoviesAction = IAction<
  "movie_save",
  {
    movies: IMovie[];
    total: number;
  }
>;
export type SetLoadingAction = IAction<"movie_set_loading", boolean>;
export type SetConditionAction = IAction<
  "movie_set_condition",
  ISearchCondition
>;
export type DeleteAction = IAction<"movie_delete", string>;
export type MovieChangeSwitchAction = IAction<
  "movie_switch",
  {
    type: SwitchType;
    newVal: boolean;
    id: string;
  }
>;
export type MovieActions =
  | SaveMoviesAction
  | SetLoadingAction
  | SetConditionAction
  | DeleteAction
  | MovieChangeSwitchAction;

const saveMoviesAction = (
  movies: IMovie[],
  total: number
): SaveMoviesAction => {
  return {
    type: "movie_save",
    // 负载/载荷
    payload: {
      movies,
      total,
    },
  };
};
const setLoadingAction = (isLoading: boolean): SetLoadingAction => {
  return {
    type: "movie_set_loading",
    payload: isLoading,
  };
};
const setCondition = (condition: ISearchCondition): SetConditionAction => {
  return {
    type: "movie_set_condition",
    payload: condition,
  };
};
const deleteAction = (id: string): DeleteAction => {
  return {
    type: "movie_delete",
    payload: id,
  };
};
const changeSwitchAction = (
  type: SwitchType,
  newVal: boolean,
  id: string
): MovieChangeSwitchAction => ({
  type: "movie_switch",
  payload: {
    type,
    newVal,
    id,
  },
});

/**
 * 副作用操作
 * 根据条件从服务器获取电影数据
 */
const fetchMovies = (
  condition: ISearchCondition
): ThunkAction<Promise<void>, IRootState, any, MovieActions> => {
  return async (dispatch, getState) => {
    // 1.设置加载状态
    dispatch(setLoadingAction(true));
    // 2.设置条件
    dispatch(setCondition(condition));
    // 3.获取服务器数据
    const curCondition = getState().movie.condition;
    const resp = await MovieService.getMovies(curCondition);
    // 4.更改仓库数据
    dispatch(saveMoviesAction(resp.data, resp.total));
    // 关闭加载状态
    dispatch(setLoadingAction(false));
  };
};
const deleteMovie = (
  id: string
): ThunkAction<Promise<void>, IRootState, any, MovieActions> => {
  return async (dispatch) => {
    dispatch(setLoadingAction(true));
    // 删除服务器中的数据
    await MovieService.delete(id);
    // 删除本地仓库中的数据
    dispatch(deleteAction(id));
    dispatch(setLoadingAction(false));
  };
};
const changeSwitch = (
  type: SwitchType,
  newVal: boolean,
  id: string
): ThunkAction<Promise<void>, IRootState, any, MovieActions> => {
  return async (dispatch) => {
    dispatch(changeSwitchAction(type, newVal, id));
    await MovieService.edit(id, {
      [type]: newVal,
    });
  };
};

export default {
  saveMoviesAction,
  setLoadingAction,
  setCondition,
  deleteAction,
  fetchMovies,
  deleteMovie,
  changeSwitchAction,
  changeSwitch,
};
