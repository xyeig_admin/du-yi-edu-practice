import { combineReducers } from "redux";
import MovieReducer, { IMovieState } from "./MovieReducer";

export const rootReducer = combineReducers({
  movie: MovieReducer,
});

/**
 * 整个网站的根状态
 */
export interface IRootState {
  movie: IMovieState;
}
