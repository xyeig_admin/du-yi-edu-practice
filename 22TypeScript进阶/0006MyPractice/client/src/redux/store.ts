import { applyMiddleware, createStore } from "redux";
import { IRootState, rootReducer } from "./reducers/RootReducer";
import logger from "redux-logger";
import thunk, { ThunkMiddleware } from "redux-thunk";

/**
 * ThunkMiddleware<IRootState> 修改dispatch的类型判断
 * 不是用redux定义的action类型，使用thunk定义的函数类型
 */
export const store = createStore(
  rootReducer,
  applyMiddleware(thunk as ThunkMiddleware<IRootState>, logger)
);
