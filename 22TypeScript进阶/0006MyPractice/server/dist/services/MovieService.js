"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("../db");
const Movie_1 = require("../entities/Movie");
const SearchCondition_1 = require("../entities/SearchCondition");
class MovieService {
    static add(movie) {
        return __awaiter(this, void 0, void 0, function* () {
            movie = Movie_1.Movie.transformToClass(movie);
            const errors = yield movie.validateMovie();
            if (errors.length > 0) {
                return errors;
            }
            return yield db_1.MovieModel.create(movie);
        });
    }
    static edit(id, movie) {
        return __awaiter(this, void 0, void 0, function* () {
            const movieObj = Movie_1.Movie.transformToClass(movie);
            const errors = yield movieObj.validateMovie(true);
            if (errors.length > 0) {
                return errors;
            }
            yield db_1.MovieModel.updateOne({
                _id: id,
            }, movie);
            return errors;
        });
    }
    static remove(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield db_1.MovieModel.deleteOne({
                _id: id,
            });
        });
    }
    static findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield db_1.MovieModel.findById(id);
        });
    }
    static find(condition) {
        return __awaiter(this, void 0, void 0, function* () {
            const searchObj = SearchCondition_1.SearchCondition.transformToClass(condition);
            const errors = yield searchObj.validateMovie(true);
            if (errors.length > 0) {
                return {
                    total: 0,
                    data: [],
                    errors,
                };
            }
            const movies = yield db_1.MovieModel.find({
                name: {
                    $regex: new RegExp(searchObj.key),
                },
            })
                .skip((searchObj.page - 1) * searchObj.limit)
                .limit(searchObj.limit);
            const total = yield db_1.MovieModel.find({
                name: {
                    $regex: new RegExp(searchObj.key),
                },
            }).countDocuments();
            return {
                total,
                data: movies,
                errors: [],
            };
        });
    }
}
exports.MovieService = MovieService;
