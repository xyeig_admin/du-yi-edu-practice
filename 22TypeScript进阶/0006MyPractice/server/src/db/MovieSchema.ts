import Mongoose from "mongoose";
import { Movie } from "../entities/Movie";

// 开发期间的类型检查（TS）
// Mongoose.Document封装了id、save等常用属性和方法
export interface IMovie extends Mongoose.Document, Movie {}

const movieSchema = new Mongoose.Schema<IMovie>(
  {
    // 运行期间的类型（JS）
    name: String,
    types: [String],
    areas: [String],
    timeLong: Number,
    isHot: Boolean,
    isClassic: Boolean,
    isComing: Boolean,
    description: String,
    poster: String,
  },
  {
    versionKey: false,
  }
);

export default Mongoose.model<IMovie>("Movie", movieSchema);
