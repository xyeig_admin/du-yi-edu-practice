import Mongoose from "mongoose";
import MovieSchema from "./MovieSchema";

// 写localhost会连接失败
Mongoose.connect("mongodb://127.0.0.1:27017/movieDB", {
  useNewUrlParser: true
}).then(() => console.log("连接数据库成功"));

// 必须导出一个声明 var/function，所以要加 {}
export { MovieSchema as MovieModel };
