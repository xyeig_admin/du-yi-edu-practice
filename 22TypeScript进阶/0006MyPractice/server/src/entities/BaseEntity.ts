import { ClassType } from "class-transformer/ClassTransformer";
import { plainToClass } from "class-transformer";
import { validate } from "class-validator";

export abstract class BaseEntity {
  /**
   * 验证当前调用该方法的Movie实例对象
   * @param skipMissing 是否跳过未提供属性的验证
   */
  /*
    errors: [
      {
        constraints: {
          IsNotEmpty: "电影名称不可为空"
        }
      },
      {
        constraints: {
          IsNotEmpty: "时长不可为空"
          IsInt: "时长必须是证书"
        }
      },
      ...
    ]
    需要得到下面的数组：
    ["电影名称不可为空", "时长不可为空", "时长必须是证书"]
  */
  public async validateMovie(skipMissing = false): Promise<string[]> {
    const errors = await validate(this, {
      skipMissingProperties: skipMissing,
    });
    return errors.map((e) => Object.values(e.constraints)).flat();
  }

  /**
   * 根据平面对象生成实体对象
   * @param plainObject 平面对象
   * @returns 实体对象
   */
  // // public static transformMovie<T extends new (...args: any) => T>(plainObject: object): T {
  // // public static transformMovie<T extends ClassType<T>>(plainObject: object): T {
  // public static transformMovie<T>(cls: ClassType<T>, plainObject: object): T {
  //   if (plainObject instanceof cls) {
  //     return plainObject;
  //   }
  //   return plainToClass(cls, plainObject);
  // }
  protected static transformPlainToClass<T>(
    cls: ClassType<T>,
    plainObject: object
  ): T {
    if (plainObject instanceof cls) {
      return plainObject;
    }
    return plainToClass(cls, plainObject);
  }
}
