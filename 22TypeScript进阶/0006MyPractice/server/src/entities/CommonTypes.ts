export interface ISearchResult<T> {
  /**
   * 数据总数
   */
  total: number;
  /**
   * 查询的数据数组
   */
  data: T[];
  /**
   * 查询的错误
   */
  errors: string[];
}
