import { Type } from "class-transformer";
import { IsInt, Min } from "class-validator";
import { BaseEntity } from "./BaseEntity";

export class SearchCondition extends BaseEntity {
  /**
   * 页码
   */
  @IsInt({ message: "页码必须是整数" })
  @Min(1, { message: "页码最小值为1" })
  @Type(() => Number)
  public page: number = 1;

  /**
   * 页容量
   */
  @IsInt({ message: "页容量必须是整数" })
  @Min(1, { message: "页容量最小值为1" })
  @Type(() => Number)
  public limit: number = 10;

  /**
   * 查询关键字
   */
  @Type(() => String)
  public key: string = "";

  /**
   * 将平面对象转换为实体对象
   * @param plainObject 平面对象
   * @returns 实体对象
   */
  public static transformToClass(plainObject: object): SearchCondition {
    return super.transformPlainToClass(SearchCondition, plainObject);
  }
}
