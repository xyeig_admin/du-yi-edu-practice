import "reflect-metadata";
import Express from "express";
import MovieRouter from "./routes/MovieRoute";
import UploadRouter from "./routes/UploadRoute";
import history from "connect-history-api-fallback";

const app = Express();

// 配置中间件，用于解析请求消息体中的json格式数据
app.use(Express.json());

app.use("/api/movie", MovieRouter);
app.use("/api/upload", UploadRouter);

// 静态资源
app.use(history());
app.use("/", Express.static("public/build"));
app.use("/upload", Express.static("public/upload"));

app.listen(3001);

// import { Movie } from "./entities/Movie";
// import { MovieService } from "./services/MovieService";

// const getRandom = (min: number, max: number): number =>
//   Math.floor(Math.random() * (max - min) + min);

// for (let i = 0; i < 100; i++) {
//   const m = new Movie();
//   m.name = "Movie" + (i + 1);
//   m.areas = ["中国大陆", "美国"];
//   m.types = ["喜剧", "动作"];
//   m.isClassic = true;
//   m.timeLong = getRandom(70, 240);
//   MovieService.add(m);
// }

// const condition: any = {
//   page: 3,
//   limit: 5,
//   key: "2",
// };
// MovieService.find(condition).then((r) => {
//   if (r.errors.length > 0) {
//     console.log(r.errors);
//   } else {
//     r.data.forEach((data) => console.log(data.name));
//   }
// });
