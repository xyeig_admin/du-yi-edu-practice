import { MovieModel } from "../db";
import { IMovie } from "../db/MovieSchema";
import { ISearchResult } from "../entities/CommonTypes";
import { Movie } from "../entities/Movie";
import { SearchCondition } from "../entities/SearchCondition";

export class MovieService {
  public static async add(movie: Movie): Promise<IMovie | string[]> {
    // 1.转换类型
    // movie = Movie.transformToClass(Movie, movie);
    movie = Movie.transformToClass(movie);
    // 2.数据验证
    const errors = await movie.validateMovie();
    if (errors.length > 0) {
      return errors;
    }
    // 3.添加到数据库
    return await MovieModel.create(movie);
  }

  public static async edit(id: string, movie: Movie): Promise<string[]> {
    /**
     * 如果覆盖掉原来的平面对象，修改某些字段时
     * 未提供的字段有可能使用实体类的默认值覆盖现有值
     * 应该保持原有平面对象不被破坏
     */
    // movie = Movie.transformToClass(movie);

    const movieObj = Movie.transformToClass(movie);
    const errors = await movieObj.validateMovie(true);
    if (errors.length > 0) {
      return errors;
    }
    await MovieModel.updateOne(
      {
        _id: id,
      },
      movie
    );
    return errors;
  }

  public static async remove(id: string): Promise<void> {
    await MovieModel.deleteOne({
      _id: id,
    });
  }

  public static async findById(id: string): Promise<IMovie | null> {
    return await MovieModel.findById(id);
  }

  /**
   * 条件查询
   * @param condition 查询条件 page、limit、key
   */
  public static async find(
    condition: SearchCondition
  ): Promise<ISearchResult<Movie>> {
    const searchObj = SearchCondition.transformToClass(condition);
    const errors = await searchObj.validateMovie(true);
    if (errors.length > 0) {
      return {
        total: 0,
        data: [],
        errors,
      };
    }
    const movies = await MovieModel.find({
      name: {
        $regex: new RegExp(searchObj.key),
      },
    })
      .skip((searchObj.page - 1) * searchObj.limit)
      .limit(searchObj.limit);
    const total = await MovieModel.find({
      name: {
        $regex: new RegExp(searchObj.key),
      },
    }).countDocuments();
    return {
      total,
      data: movies,
      errors: [],
    };
  }
}
